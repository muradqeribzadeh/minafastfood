﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebService1
{
    public class DBConnection
    {
        private SqlDataAdapter myAdapter;
        private SqlConnection conn;

        /// <constructor>
        /// Initialise Connection
        /// </constructor>
        public DBConnection()
        {
            myAdapter = new SqlDataAdapter();
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FastFoodConnectionString"].ConnectionString);
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        private SqlConnection openConnection()
        {
            if (conn.State == ConnectionState.Closed || conn.State ==
                        ConnectionState.Broken)
            {
                conn.Open();
            }
            return conn;
        }

        /// <method>
        /// Select Query
        /// </method>
        public DataTable executeSelectQuery(String _query, SqlParameter[] sqlParameters)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameters);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                conn.Close();
            }
            return dataTable;
        }

        public DataTable executeSelectQuery(String _query)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;

                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                conn.Close();
            }
            return dataTable;
        }

        /// <method>
        /// Insert Query
        /// </method>
        //public bool executeInsertQuery(String _query, OracleParameter[] sqlParameter)
        //{
        //    OracleCommand myCommand = new OracleCommand();
        //    try
        //    {
        //        myCommand.Connection = openConnection();
        //        myCommand.CommandText = _query;
        //        myCommand.Parameters.AddRange(sqlParameter);
        //        myAdapter.InsertCommand = myCommand;
        //        myCommand.ExecuteNonQuery();
        //    }
        //    catch (OracleException e)
        //    {
        //        Console.Write("Error - Connection.executeInsertQuery - Query: " + _query + " \nException: \n" + e.StackTrace.ToString());
        //        //return false;
        //        throw e;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //    return true;
        //}

        ///// <method>
        ///// Update Query
        ///// </method>
        //public bool executeUpdateQuery(String _query, OracleParameter[] sqlParameter)
        //{
        //    OracleCommand myCommand = new OracleCommand();
        //    OracleTransaction mytransaction;
        //    try
        //    {
        //        myCommand.Connection = openConnection();
        //        myCommand.CommandText = _query;
        //        myCommand.Parameters.AddRange(sqlParameter);
        //        myAdapter.UpdateCommand = myCommand;
        //        mytransaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);
        //        myCommand.Transaction = mytransaction;
        //        myCommand.ExecuteNonQuery();
        //        mytransaction.Commit();
        //    }
        //    catch (OracleException e)
        //    {
        //        Console.Write("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
        //        //return false;
        //        throw e;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //    return true;
        //}

        ////<method>
        /////Stored Procedure
        ////</method>
        //public int executeStoredProcedure(String _query, SqlParameter[] sqlParameter, string returnParam)
        //{
        //    SqlCommand myCommand = new SqlCommand();
        //    try
        //    {
        //        myCommand.Connection = openConnection();
        //        myCommand.CommandType = CommandType.StoredProcedure;
        //        myCommand.CommandText = _query;
        //        myCommand.Parameters.AddRange(sqlParameter);
        //        myAdapter.SelectCommand = myCommand;
        //        myCommand.ExecuteScalar();
        //        return int.Parse(myCommand.Parameters[returnParam].Value.ToString());
        //    }
        //    catch (OracleException e)
        //    {
        //        Console.Write("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
        //        //return 0;
        //        throw e;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}

        //public void executeStoredProcedure(String _query, OracleParameter[] sqlParameter)
        //{
        //    OracleCommand myCommand = new OracleCommand();
        //    try
        //    {
        //        myCommand.Connection = openConnection();
        //        myCommand.CommandType = CommandType.StoredProcedure;
        //        myCommand.CommandText = _query;
        //        myCommand.Parameters.AddRange(sqlParameter);
        //        myAdapter.SelectCommand = myCommand;
        //        myCommand.ExecuteNonQuery();
        //    }
        //    catch (OracleException e)
        //    {
        //        Console.Write("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());

        //        throw e;
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    }
        //}
    }
}