﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService1
{
    public class ItemOptionObject
    {
        public string ItemId { get; set; }
        public string ItemNumber { get; set; }
        public string OptionId { get; set; }
        public string OptionsCost { get; set; }
    }
}