﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService1
{
    public class OrderItemObject
    {
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemPrice { get; set; }
        public string Quantity { get; set; }
        public string OptionsDescription { get; set; }
        public string AnItemPrice { get; set; }
        public string OptionsPrice { get; set; }
        public string ItemNumber { get; set; }
        public string IsStockItem { get; set; }
    }
}