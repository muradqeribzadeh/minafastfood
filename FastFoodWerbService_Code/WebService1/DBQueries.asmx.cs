﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Services;

namespace WebService1
{
    /// <summary>
    /// Summary description for DBQueries
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class DBQueries : System.Web.Services.WebService
    {
        [WebMethod]
        public string getUser(string username, string password)
        {
            DBConnection conn = new DBConnection();
            string query = @"SELECT [UserEmployee],[StoredPassword] FROM [EmpUser] where [UserName]=@username";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = username;
            DataTable curUserTbl = conn.executeSelectQuery(query, sqlParameters);
            if (curUserTbl.Rows.Count == 0)
            {
                return "0";
            }
            else
            {
                if (curUserTbl.Rows[0][0] != null && (new DevExpress.Persistent.Base.PasswordCryptographer().AreEqual(curUserTbl.Rows[0][1].ToString(), password)))
                    return curUserTbl.Rows[0][0].ToString();
                else
                    return "0";
            }
        }

        [WebMethod]
        public DataTable getMenuList()
        {
            DBConnection conn = new DBConnection();
            string query = @"Select Oid,GroupName,Picture from [Group] where visible=1 order by displayorder";
            DataTable menuTbl = conn.executeSelectQuery(query);
            return menuTbl;
        }

        [WebMethod]
        public DataTable getUserList()
        {
            //Select Oid,GroupName,Picture from [MinAFastFood].[dbo].[Group] where visible=1 order by displayorder
            DBConnection conn = new DBConnection();
            string query = @"Select Oid,UserName  FROM [EmpUser] where [GCRecord] is null ";
            DataTable userTbl = conn.executeSelectQuery(query);
            return userTbl;
        }

        [WebMethod]
        public DataTable getItems(string groupId)
        {
            if (groupId.Equals("0"))
                return null;
            else
            {
                DBConnection conn = new DBConnection();
                string query = @"SELECT  Oid,ItemName,DefaultItemPrice,IsStockItem FROM [Item] where [Item].[Group]=@groupId and visible=1 order by displayindex";
                SqlParameter[] sqlParameters = new SqlParameter[1];
                sqlParameters[0] = new SqlParameter("@groupId", SqlDbType.VarChar);
                sqlParameters[0].Value = groupId;

                DataTable itemsTbl = conn.executeSelectQuery(query, sqlParameters);
                return itemsTbl;
            }
        }

        [WebMethod]
        public DataTable getOptions(string itemId)
        {
            if (itemId.Equals("0"))
                return null;
            else
            {
                DBConnection conn = new DBConnection();
                string query = @"SELECT  Oid,ModifierName,ModifierPrice FROM [Modifiers] where [Modifiers].[Item]=@itemId and visible=1";
                SqlParameter[] sqlParameters = new SqlParameter[1];
                sqlParameters[0] = new SqlParameter("@itemId", SqlDbType.VarChar);
                sqlParameters[0].Value = itemId;

                DataTable itemsTbl = conn.executeSelectQuery(query, sqlParameters);
                return itemsTbl;
            }
        }

        [WebMethod]

        public DataTable getTableGroups()
        {
            //Select Oid,GroupName,Picture from [MinAFastFood].[dbo].[Group] where visible=1 order by displayorder
            DBConnection conn = new DBConnection();
            string query = @"Select Oid,GroupName from [TableGroups] where visible=1";
            DataTable groupsTbl = conn.executeSelectQuery(query);
            return groupsTbl;
        }

        [WebMethod]
        public DataTable getTables(string groupId)
        {
            if (groupId.Equals("0"))
                return null;
            else
            {
                DBConnection conn = new DBConnection();
                string query = @"SELECT  Oid,TableName FROM [Tables] where TableGroups=@groupId and visible=1 order by TableName";
                SqlParameter[] sqlParameters = new SqlParameter[1];
                sqlParameters[0] = new SqlParameter("@groupId", SqlDbType.VarChar);
                sqlParameters[0].Value = groupId;

                DataTable tablesTbl = conn.executeSelectQuery(query, sqlParameters);
                return tablesTbl;
            }
        }

        [WebMethod]
        public DataTable orderSave(OrderObject order, List<OrderItemObject> orderItems, List<ItemOptionObject> itemOptions)
        {
            String result = "true";

            string orderId = "";
            int ticketNumber = 0;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FastFoodConnectionString"].ConnectionString);
            try
            {

                using (TransactionScope scope = new TransactionScope())
                {
                    //Inserting Order------------------------------------
                    SqlCommand cmd = new SqlCommand("dbo.insertOrUpdateOrder", conn);

                    cmd.CommandType = CommandType.StoredProcedure;


                    cmd.Parameters.Add(new SqlParameter("@OrderDate", DateTime.Parse(order.OrderDate)));

                    if (order.UserEmployee.Equals("0"))
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", System.Data.SqlTypes.SqlGuid.Null));
                    else
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", new Guid(order.UserEmployee)));

                    cmd.Parameters.Add(new SqlParameter("@DiscountId", System.Data.SqlTypes.SqlGuid.Null));

                    cmd.Parameters.Add(new SqlParameter("@DiscountAmount", System.Data.SqlTypes.SqlMoney.Null));

                    cmd.Parameters.Add(new SqlParameter("@DiscountType", System.Data.SqlTypes.SqlInt32.Null));

                    cmd.Parameters.Add(new SqlParameter("@Discount", System.Data.SqlTypes.SqlMoney.Null));

                    cmd.Parameters.Add(new SqlParameter("@Amount", double.Parse(order.Amount)));
                    cmd.Parameters.Add(new SqlParameter("@TotalAmount", double.Parse(order.Amount)));
                    cmd.Parameters.Add("@Notes", "");

                    cmd.Parameters.Add(new SqlParameter("@OrderStatus", int.Parse("0")));
                    cmd.Parameters.Add(new SqlParameter("@Printed", false));
                    if (string.IsNullOrEmpty(order.TableId) || order.TableId.Equals("0"))
                    {
                        cmd.Parameters.Add(new SqlParameter("@TableId", System.Data.SqlTypes.SqlGuid.Null));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@TableId", new Guid(order.TableId)));
                    }

                    //@OldOid
                    cmd.Parameters.Add(new SqlParameter("@OldOid", System.Data.SqlTypes.SqlGuid.Null));
                    //@IoU
                    cmd.Parameters.Add(new SqlParameter("@IoU", int.Parse("0")));
                    cmd.Parameters.Add("@Oid", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@TicketNumber", SqlDbType.Int).Direction = ParameterDirection.Output;


                    conn.Open();
                    cmd.ExecuteNonQuery();
                    orderId = cmd.Parameters["@Oid"].Value.ToString();
                    ticketNumber = Convert.ToInt32(cmd.Parameters["@TicketNumber"].Value);

                    //Inserting OrderItems--------------------------------------------

                    foreach (OrderItemObject orderItem in orderItems)
                    {


                        SqlCommand cmdOrderItems = new SqlCommand("dbo.insertOrderDetails", conn);

                        cmdOrderItems.CommandType = CommandType.StoredProcedure;

                        cmdOrderItems.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier)).Value = new Guid(orderId);
                        cmdOrderItems.Parameters.Add(new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier)).Value = new Guid(orderItem.ItemId);
                        cmdOrderItems.Parameters.Add(new SqlParameter("@Quantity", double.Parse(orderItem.Quantity)));

                        cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountId", System.Data.SqlTypes.SqlGuid.Null));

                        cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountAmount", System.Data.SqlTypes.SqlMoney.Null));

                        cmdOrderItems.Parameters.Add(new SqlParameter("@ItemPrice", double.Parse(orderItem.ItemPrice)));
                        cmdOrderItems.Parameters.Add(new SqlParameter("@ItemName", orderItem.ItemName));
                        cmdOrderItems.Parameters.Add(new SqlParameter("@OptionsDescription", orderItem.OptionsDescription));
                        cmdOrderItems.Parameters.Add(new SqlParameter("@PrintedTo", int.Parse("0")));
                        cmdOrderItems.Parameters.Add(new SqlParameter("@AlreadyPrinted", int.Parse("0")));

                        cmdOrderItems.Parameters.Add(new SqlParameter("@AnItemsPrice", double.Parse(orderItem.AnItemPrice)));
                        cmdOrderItems.Parameters.Add(new SqlParameter("@ItemNumber", int.Parse(orderItem.ItemNumber)));
                        cmdOrderItems.Parameters.Add(new SqlParameter("@OptionsPrice", double.Parse(orderItem.OptionsPrice)));

                        cmdOrderItems.ExecuteNonQuery();


                        if (bool.Parse(orderItem.IsStockItem))
                            decreaseItemQuantity(orderItem.ItemId, double.Parse(orderItem.Quantity));


                    }

                    //Inserting item options---------------------------------------------

                    foreach (ItemOptionObject itemOption in itemOptions)
                    {

                        SqlCommand cmdOrderOptions = new SqlCommand("dbo.insertOrderOptions", conn);
                        cmdOrderOptions.CommandType = CommandType.StoredProcedure;
                        cmdOrderOptions.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier)).Value = new Guid(orderId);
                        cmdOrderOptions.Parameters.Add(new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier)).Value = new Guid(itemOption.ItemId);
                        cmdOrderOptions.Parameters.Add(new SqlParameter("@ItemNumber", int.Parse(itemOption.ItemNumber)));

                        cmdOrderOptions.Parameters.Add(new SqlParameter("@OptionId", SqlDbType.UniqueIdentifier)).Value = new Guid(itemOption.OptionId);
                        cmdOrderOptions.Parameters.Add(new SqlParameter("@OptionCost", double.Parse(itemOption.OptionsCost)));
                        cmdOrderOptions.ExecuteNonQuery();
                    }


                    scope.Complete();
                }



            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {

                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            DataSet ds = new DataSet();
            DataTable table = new DataTable("ResultTable");
            table.Columns.Add("Result", typeof(string));
            table.Columns.Add("TicketNumber", typeof(string));
            table.Rows.Add(result, ticketNumber);

            ds.Tables.Add(table);
            return ds.Tables[0];






        }

        private void decreaseItemQuantity(string oid, double qty)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["FastFoodConnectionString"].ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update [Item] Set [QtyOnHand]=[QtyOnHand]-" + qty + " where  [Oid]=N'" + oid + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                // Message.Show(string.Format("An error occurred: {0}", ex.Message));
                Console.WriteLine(ex.Message);
            }
        }

        //[WebMethod]
        //public void saveOrderItem(string orderId, string itemId, string itemName, string itemPrice, string quantity, string optionsDescription, string anItemPrice, string optionsPrice, string itemNumber, string isStockItem)
        //{
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FastFoodConnectionString"].ConnectionString);

        //    SqlCommand cmdOrderItems = new SqlCommand("dbo.insertOrderDetails", conn);

        //    cmdOrderItems.CommandType = CommandType.StoredProcedure;

        //    cmdOrderItems.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier)).Value = new Guid(orderId);
        //    cmdOrderItems.Parameters.Add(new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier)).Value = new Guid(itemId);
        //    cmdOrderItems.Parameters.Add(new SqlParameter("@Quantity", double.Parse(quantity)));

        //    cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountId", System.Data.SqlTypes.SqlGuid.Null));

        //    cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountAmount", System.Data.SqlTypes.SqlMoney.Null));

        //    cmdOrderItems.Parameters.Add(new SqlParameter("@ItemPrice", double.Parse(itemPrice)));
        //    cmdOrderItems.Parameters.Add(new SqlParameter("@ItemName", itemName));
        //    cmdOrderItems.Parameters.Add(new SqlParameter("@OptionsDescription", optionsDescription));
        //    cmdOrderItems.Parameters.Add(new SqlParameter("@PrintedTo", int.Parse("0")));
        //    cmdOrderItems.Parameters.Add(new SqlParameter("@AlreadyPrinted", int.Parse("0")));

        //    cmdOrderItems.Parameters.Add(new SqlParameter("@AnItemsPrice", double.Parse(anItemPrice)));
        //    cmdOrderItems.Parameters.Add(new SqlParameter("@ItemNumber", int.Parse(itemNumber)));
        //    cmdOrderItems.Parameters.Add(new SqlParameter("@OptionsPrice", double.Parse(optionsPrice)));

        //    conn.Open();
        //    cmdOrderItems.ExecuteNonQuery();
        //    conn.Close();

        //    if (bool.Parse(isStockItem))
        //        decreaseItemQuantity(itemId, double.Parse(quantity));
        //}

        //[WebMethod]
        //public void saveOrderOptions(string orderId, string itemId, string itemNumber, string optionId, string optionsCost)
        //{
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FastFoodConnectionString"].ConnectionString);

        //    SqlCommand cmdOrderOptions = new SqlCommand("dbo.insertOrderOptions", conn);

        //    cmdOrderOptions.CommandType = CommandType.StoredProcedure;
        //    cmdOrderOptions.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier)).Value = new Guid(orderId);
        //    cmdOrderOptions.Parameters.Add(new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier)).Value = new Guid(itemId);
        //    //ItemNumber
        //    cmdOrderOptions.Parameters.Add(new SqlParameter("@ItemNumber", int.Parse(itemNumber)));

        //    cmdOrderOptions.Parameters.Add(new SqlParameter("@OptionId", SqlDbType.UniqueIdentifier)).Value = new Guid(optionId);
        //    cmdOrderOptions.Parameters.Add(new SqlParameter("@OptionCost", double.Parse(optionsCost)));
        //    conn.Open();
        //    cmdOrderOptions.ExecuteNonQuery();
        //    conn.Close();
        //}

        [WebMethod]
        public int getItemQuantity(string itemId)
        {
            if (itemId.Equals("0"))
                return 0;
            else
            {

                DBConnection conn = new DBConnection();
                string query = @"Select QtyOnHand from Item where Oid=@itemId and  Visible=1";
                SqlParameter[] sqlParameters = new SqlParameter[1];
                sqlParameters[0] = new SqlParameter("@itemId", SqlDbType.VarChar);
                sqlParameters[0].Value = itemId;

                DataTable itemsTbl = conn.executeSelectQuery(query, sqlParameters);
                //    return itemsTbl;
                try
                {
                    return Int32.Parse(itemsTbl.Rows[0][0].ToString());
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
    }
}