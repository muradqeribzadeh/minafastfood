﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebService1
{
    public class OrderObject
    {
        public string OrderDate { get; set; }
        public string UserEmployee { get; set; }
        public string Amount { get; set; }
        public string TableId { get; set; }

    }
}