﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

namespace WebService1
{
    /// <summary>
    /// Summary description for DBQueries
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class DBQueries : System.Web.Services.WebService
    {
        [WebMethod]
        public string getUser(string username, string password)
        {
            //
            DBConnection conn = new DBConnection();
            string query = @"SELECT [UserEmployee] FROM [EmpUser] where [UserName]=@username and [StoredPassword]=@password";
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = username;
            sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            sqlParameters[1].Value = password;
            DataTable curUserTbl = conn.executeSelectQuery(query, sqlParameters);
            if (curUserTbl.Rows.Count == 0)
            {
                return "0";
            }
            else
            {
                if (curUserTbl.Rows[0][0] != null)
                    return curUserTbl.Rows[0][0].ToString();
                else
                    return "0";
            }
        }

        [WebMethod]
        public DataTable getMenuList()
        {
            //Select Oid,GroupName,Picture from [MinAFastFood].[dbo].[Group] where visible=1 order by displayorder
            DBConnection conn = new DBConnection();
            string query = @"Select Oid,GroupName,Picture from [Group] where visible=1 order by displayorder";
            DataTable menuTbl = conn.executeSelectQuery(query);
            return menuTbl;
        }

        [WebMethod]
        public DataTable getUserList()
        {
            //Select Oid,GroupName,Picture from [MinAFastFood].[dbo].[Group] where visible=1 order by displayorder
            DBConnection conn = new DBConnection();
            string query = @"Select Oid,UserName  FROM [EmpUser] where [GCRecord] is null ";
            DataTable userTbl = conn.executeSelectQuery(query);
            return userTbl;
        }

        [WebMethod]
        public DataTable getItems(string groupId)
        {
            DBConnection conn = new DBConnection();
            string query = @"SELECT  Oid,ItemName,DefaultItemPrice,IsStockItem FROM [Item] where [Item].[Group]=@groudId and visible=1 order by displayindex";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@groudId", SqlDbType.VarChar);
            sqlParameters[0].Value = groupId;

            DataTable itemsTbl = conn.executeSelectQuery(query, sqlParameters);
            return itemsTbl;
        }

        [WebMethod]
        public DataTable getOptions(string itemId)
        {
            DBConnection conn = new DBConnection();
            string query = @"SELECT  Oid,ModifierName,ModifierPrice FROM [Modifiers] where [Modifiers].[Item]=@itemId and visible=1";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@itemId", SqlDbType.VarChar);
            sqlParameters[0].Value = itemId;

            DataTable itemsTbl = conn.executeSelectQuery(query, sqlParameters);
            return itemsTbl;
        }

        [WebMethod]

        public DataTable getTableGroups()
        {
            //Select Oid,GroupName,Picture from [MinAFastFood].[dbo].[Group] where visible=1 order by displayorder
            DBConnection conn = new DBConnection();
            string query = @"Select Oid,GroupName from [TableGroups] where visible=1";
            DataTable groupsTbl = conn.executeSelectQuery(query);
            return groupsTbl;
        }

        [WebMethod]
        public DataTable getTables(string groupId)
        {
            DBConnection conn = new DBConnection();
            string query = @"SELECT  Oid,TableName FROM [Tables] where TableGroups=@groupId and visible=1 order by TableName";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@groupId", SqlDbType.VarChar);
            sqlParameters[0].Value = groupId;

            DataTable tablesTbl = conn.executeSelectQuery(query, sqlParameters);
            return tablesTbl;
        }

        [WebMethod]
        public DataTable orderSave(string date, string userEmployee, string amount, string tableId)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FastFoodConnectionString"].ConnectionString);

            SqlCommand cmd = new SqlCommand("dbo.insertOrUpdateOrder", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            string orderId1 = "";
            int ticketNumber = 0;

            cmd.Parameters.Add(new SqlParameter("@OrderDate", DateTime.Parse(date)));

            if (userEmployee.Equals("0"))
                cmd.Parameters.Add(new SqlParameter("@EmployeeId", System.Data.SqlTypes.SqlGuid.Null));
            else
                cmd.Parameters.Add(new SqlParameter("@EmployeeId", new Guid(userEmployee)));

            cmd.Parameters.Add(new SqlParameter("@DiscountId", System.Data.SqlTypes.SqlGuid.Null));

            cmd.Parameters.Add(new SqlParameter("@DiscountAmount", System.Data.SqlTypes.SqlMoney.Null));

            cmd.Parameters.Add(new SqlParameter("@DiscountType", System.Data.SqlTypes.SqlInt32.Null));

            cmd.Parameters.Add(new SqlParameter("@Discount", System.Data.SqlTypes.SqlMoney.Null));

            cmd.Parameters.Add(new SqlParameter("@Amount", double.Parse(amount)));
            cmd.Parameters.Add(new SqlParameter("@TotalAmount", double.Parse(amount)));
            cmd.Parameters.Add("@Notes", "");

            cmd.Parameters.Add(new SqlParameter("@OrderStatus", int.Parse("0")));
            cmd.Parameters.Add(new SqlParameter("@Printed", false));
            if (string.IsNullOrEmpty(tableId) || tableId.Equals("0"))
            {
                cmd.Parameters.Add(new SqlParameter("@TableId", System.Data.SqlTypes.SqlGuid.Null));
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@TableId", new Guid(tableId)));
            }

            //@OldOid
            cmd.Parameters.Add(new SqlParameter("@OldOid", System.Data.SqlTypes.SqlGuid.Null));
            //@IoU
            cmd.Parameters.Add(new SqlParameter("@IoU", int.Parse("0")));
            cmd.Parameters.Add("@Oid", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@TicketNumber", SqlDbType.Int).Direction = ParameterDirection.Output;
            // set parameter values
            //     cmd.Parameters["@ContractNumber"].Value = contractNumber;

            // open connection and execute stored procedure

            conn.Open();
            cmd.ExecuteNonQuery();
            orderId1 = cmd.Parameters["@Oid"].Value.ToString();
            ticketNumber = Convert.ToInt32(cmd.Parameters["@TicketNumber"].Value);
            conn.Close();
            DataSet ds = new DataSet();

            DataTable table = new DataTable("OrderResult");

            table.Columns.Add("orderId", typeof(string));
            table.Columns.Add("ticketNumber", typeof(int));

            //
            // Here we add five DataRows.
            //
            table.Rows.Add(orderId1, ticketNumber);
            ds.Tables.Add(table);
            return ds.Tables[0];
        }

        private void decreaseItemQuantity(string oid, double qty)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["FastFoodConnectionString"].ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update [Item] Set [QtyOnHand]=[QtyOnHand]-" + qty + " where  [Oid]=N'" + oid + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                // Message.Show(string.Format("An error occurred: {0}", ex.Message));
                Console.WriteLine(ex.Message);
            }
        }

        [WebMethod]

        public void saveOrderItem(string orderId, string itemId, string itemName, string itemPrice, string quantity, string optionsDescription, string anItemPrice, string optionsPrice, string itemNumber, string isStockItem)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FastFoodConnectionString"].ConnectionString);

            SqlCommand cmdOrderItems = new SqlCommand("dbo.insertOrderDetails", conn);

            cmdOrderItems.CommandType = CommandType.StoredProcedure;

            cmdOrderItems.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier)).Value = new Guid(orderId);
            cmdOrderItems.Parameters.Add(new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier)).Value = new Guid(itemId);
            cmdOrderItems.Parameters.Add(new SqlParameter("@Quantity", double.Parse(quantity)));

            cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountId", System.Data.SqlTypes.SqlGuid.Null));

            cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountAmount", System.Data.SqlTypes.SqlMoney.Null));

            cmdOrderItems.Parameters.Add(new SqlParameter("@ItemPrice", double.Parse(itemPrice)));
            cmdOrderItems.Parameters.Add(new SqlParameter("@ItemName", itemName));
            cmdOrderItems.Parameters.Add(new SqlParameter("@OptionsDescription", optionsDescription));
            cmdOrderItems.Parameters.Add(new SqlParameter("@PrintedTo", int.Parse("0")));
            cmdOrderItems.Parameters.Add(new SqlParameter("@AlreadyPrinted", int.Parse("0")));

            cmdOrderItems.Parameters.Add(new SqlParameter("@AnItemsPrice", double.Parse(anItemPrice)));
            cmdOrderItems.Parameters.Add(new SqlParameter("@ItemNumber", int.Parse(itemNumber)));
            cmdOrderItems.Parameters.Add(new SqlParameter("@OptionsPrice", double.Parse(optionsPrice)));

            conn.Open();
            cmdOrderItems.ExecuteNonQuery();
            conn.Close();

            if (bool.Parse(isStockItem))
                decreaseItemQuantity(itemId, double.Parse(quantity));
        }

        [WebMethod]
        public void saveOrderOptions(string orderId, string itemId, string itemNumber, string optionId, string optionsCost)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["FastFoodConnectionString"].ConnectionString);

            SqlCommand cmdOrderOptions = new SqlCommand("dbo.insertOrderOptions", conn);

            cmdOrderOptions.CommandType = CommandType.StoredProcedure;
            cmdOrderOptions.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier)).Value = new Guid(orderId);
            cmdOrderOptions.Parameters.Add(new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier)).Value = new Guid(itemId);
            //ItemNumber
            cmdOrderOptions.Parameters.Add(new SqlParameter("@ItemNumber", int.Parse(itemNumber)));

            cmdOrderOptions.Parameters.Add(new SqlParameter("@OptionId", SqlDbType.UniqueIdentifier)).Value = new Guid(optionId);
            cmdOrderOptions.Parameters.Add(new SqlParameter("@OptionCost", double.Parse(optionsCost)));
            conn.Open();
            cmdOrderOptions.ExecuteNonQuery();
            conn.Close();
        }

        [WebMethod]
        public int getItemQuantity(string itemId)
        {
            //  DataSet ds = PDataset("Select QtyOnHand from Item where Oid=N'" + oid + "' and  Visible=1");
            DBConnection conn = new DBConnection();
            string query = @"Select QtyOnHand from Item where Oid=@itemId and  Visible=1";
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@itemId", SqlDbType.VarChar);
            sqlParameters[0].Value = itemId;

            DataTable itemsTbl = conn.executeSelectQuery(query, sqlParameters);
            //    return itemsTbl;
            try
            {
                return Int32.Parse(itemsTbl.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}