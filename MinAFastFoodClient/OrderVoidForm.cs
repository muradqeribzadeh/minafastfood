﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace MinAFastFoodClient
{
    public partial class OrderVoidForm : DevExpress.XtraEditors.XtraForm
    {
        private string orderId;

        public OrderVoidForm(string orderId, int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.orderId = orderId;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update [Orders] Set [OrderStatus]=3,[VoidReason]='" + txtVoidReason.Text + "' where  [Oid]=N'" + orderId + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                    getStockItems();
                    Close();
                }
            }

            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        protected DataSet PDataset(string select_statement)
        {
            SqlConnection conn = new SqlConnection(Utility.DBHelper.ConnectionString);
            SqlCommand cmd = new SqlCommand(select_statement, conn);
            conn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            ad.Fill(ds);

            conn.Close();
            return ds;
        }

        private void getStockItems()
        {
            DataSet itemDataSet = PDataset("SELECT   dbo.OrderDetail.ItemId,dbo.OrderDetail.Quantity FROM    dbo.OrderDetail INNER JOIN  dbo.Item ON dbo.OrderDetail.ItemId = dbo.Item.Oid WHERE     (dbo.Item.IsStockItem = 1)  and (dbo.OrderDetail.OrderId=N'" + orderId + "')");
            DataTable itemTable = itemDataSet.Tables[0];

            foreach (DataRow r in itemTable.Rows)
            {
                decreaseItemQuantity(r[0].ToString(), double.Parse(r[1].ToString()));
            }
        }

        private void decreaseItemQuantity(string oid, double qty)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update [Item] Set [QtyOnHand]=[QtyOnHand]+" + qty + " where  [Oid]=N'" + oid + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                XtraMessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        private void OrderVoidForm_Load(object sender, EventArgs e)
        {
            txtVoidReason.Focus();
        }
    }
}