﻿namespace MinAFastFoodClient
{
    partial class ItemForDiscount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemForDiscount));
            this.btnDone = new DevExpress.XtraEditors.SimpleButton();
            this.itemsControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.SuspendLayout();
            // 
            // btnDone
            // 
            resources.ApplyResources(this.btnDone, "btnDone");
            this.btnDone.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDone.Appearance.Font")));
            this.btnDone.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnDone.Appearance.GradientMode")));
            this.btnDone.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnDone.Appearance.Image")));
            this.btnDone.Appearance.Options.UseFont = true;
            this.btnDone.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnDone.Name = "btnDone";
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // itemsControl
            // 
            resources.ApplyResources(this.itemsControl, "itemsControl");
            this.itemsControl.Name = "itemsControl";
            // 
            // ItemForDiscount
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.itemsControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItemForDiscount";
            this.Load += new System.EventHandler(this.ItemForDiscount_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnDone;
        private DevExpress.XtraEditors.XtraScrollableControl itemsControl;
    }
}