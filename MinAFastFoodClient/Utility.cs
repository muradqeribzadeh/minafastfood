﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace MinAFastFoodClient
{
    class Utility
    {
        public class DBHelper
        {
            public static string ConnectionString
            {
                get
                {
                    return ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

                }

            }
        }
    }
}
