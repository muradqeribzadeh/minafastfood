﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using MinAFastFoodClient.Classes;

namespace MinAFastFoodClient
{
    public partial class MainForm : Form
    {
        private User user;
        private int language;

        public MainForm(User user, int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.user = new User();
            this.user = user;
            this.language = language;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            LoginForm l = new LoginForm();
            this.Hide();
            l.ShowDialog();
            l.Close();
        }

        private void btnOrders_Click(object sender, EventArgs e)
        {
            OrdersListView ordersListView = new OrdersListView(user, this.language);

            ordersListView.ShowDialog();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu(user, this.language);
            menu.ShowDialog();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNewOrder_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu(this.user, 1, this.language);
            menu.ShowDialog();
        }

        private void btnTimecard_Click(object sender, EventArgs e)
        {
            EmployeeTimeCard empTImeCard = new EmployeeTimeCard(this.user, this.language);
            empTImeCard.ShowDialog();
        }

        private void btnBackOffice_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(@"..\AdminFiles\MinAFastFoodAdmin.Win.exe");
            }
            catch (Exception ex)
            {
            }
        }

        private void btnFoodsQuantity_Click(object sender, EventArgs e)
        {
            FoodsQuantity foodqty = new FoodsQuantity(this.language);
            foodqty.ShowDialog();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm(this.language);
            aboutForm.ShowDialog();
        }
    }
}