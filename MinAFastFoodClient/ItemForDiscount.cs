﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MinAFastFoodClient.Classes;
using System.Threading;
using System.Globalization;
using System.Resources;

namespace MinAFastFoodClient
{
    public partial class ItemForDiscount : DevExpress.XtraEditors.XtraForm
    {
     private   ResourceManager LocRM = new ResourceManager("MinAFastFoodClient.FastFoodLanguage", typeof(ItemForDiscount).Assembly);
     private int language;
        public ItemForDiscount(int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.language = language;
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ItemForDiscount_Load(object sender, EventArgs e)
        {
            fillOrderItems();
        }
        int XIndent = 10;
        int YIndent = 10;
        int count = 0;
        public void fillOrderItems()
         {
 
         //   DataSet discountDataset = PDataset("Select [Oid],[DiscountDesc],[DiscountAmount],[DiscountType] FROM [MinAFastFood].[dbo].[Discounts] where [Visible]=1");
           // DataTable discountTable = discountDataset.Tables[0];

            foreach (OrderItems o in MinAFastFoodClient.Menu.orderItems)
            {
                SimpleButton discountBtn = new SimpleButton();
                discountBtn.Name =o.ItemNumber.ToString();
                discountBtn.Width = 162;
              //  discountBtn.ImageLocation = ImageLocation.TopCenter;
                discountBtn.Height = 71;
                discountBtn.Text =o.ItemName;
                discountBtn.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;

                if (o.DiscountId.Equals("0"))
                    discountBtn.Appearance.BackColor = Color.LimeGreen;
                else
                    discountBtn.Appearance.BackColor = Color.Yellow;
                discountBtn.Left = XIndent;
                discountBtn.Top = YIndent;

                itemsControl.Controls.Add(discountBtn);
                XIndent += discountBtn.Width + 10;

                discountBtn.Click += new EventHandler(itemButton_Click);
                count++;

                if (count % 3 == 0)
                {
                    XIndent = 10;
                    YIndent += discountBtn.Height + 10;
                }
            }
        }
        private int getOrderItemById(int id)
        {

            int k = MinAFastFoodClient.Menu.orderItems.FindIndex(x => x.ItemNumber.Equals(id));
            return k;

        }
             private void itemButton_Click(object sender, EventArgs e){
                 

                 SimpleButton btn=(SimpleButton)sender;

                 if (btn.Appearance.BackColor == Color.Yellow) {

                     if (MessageBox.Show(this.language == 0 ? LocRM.GetString("AzCancelItemDiscount") : LocRM.GetString("EngCancelItemDiscount"), "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                     {
                         btn.Appearance.BackColor = Color.LimeGreen;
                         int index = getOrderItemById(int.Parse(btn.Name.ToString()));
                       
                             MinAFastFoodClient.Menu.orderItems[index].ItemPrice = MinAFastFoodClient.Menu.orderItems[index].Quantity *MinAFastFoodClient.Menu.orderItems[index].Oneprice;
                             MinAFastFoodClient.Menu.orderItems[index].DiscountId = "0";
                             MinAFastFoodClient.Menu.orderItems[index].DiscountAmount = 0;
                             
                            // Close();
                    
                     }
                 }
                 else
                 {            Discounts discounts = new Discounts("I", int.Parse(btn.Name),this.language);
                     this.Hide();
                     discounts.ShowDialog();
                     Close();
                 }
             }
    }
}