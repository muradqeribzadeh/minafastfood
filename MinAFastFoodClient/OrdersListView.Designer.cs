﻿namespace MinAFastFoodClient
{
    partial class OrdersListView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrdersListView));
            this.dataRepeater1 = new Microsoft.VisualBasic.PowerPacks.DataRepeater();
            this.lblOrderStatus = new DevExpress.XtraEditors.LabelControl();
            this.lblOid = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.txtYekun = new DevExpress.XtraEditors.TextEdit();
            this.txtDiscount = new DevExpress.XtraEditors.TextEdit();
            this.txtOrderDate = new DevExpress.XtraEditors.TextEdit();
            this.txtUmMebleg = new DevExpress.XtraEditors.TextEdit();
            this.txtTicket = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtTicketNumber = new DevExpress.XtraEditors.TextEdit();
            this.btn7 = new DevExpress.XtraEditors.SimpleButton();
            this.btn8 = new DevExpress.XtraEditors.SimpleButton();
            this.btn9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn5 = new DevExpress.XtraEditors.SimpleButton();
            this.btn6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn0 = new DevExpress.XtraEditors.SimpleButton();
            this.txtDate = new DevExpress.XtraEditors.DateEdit();
            this.btnAllOrders = new DevExpress.XtraEditors.SimpleButton();
            this.btnBackToMain = new DevExpress.XtraEditors.SimpleButton();
            this.btnVoidOrder = new DevExpress.XtraEditors.SimpleButton();
            this.btnClosedOrders = new DevExpress.XtraEditors.SimpleButton();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.btnBack = new DevExpress.XtraEditors.SimpleButton();
            this.btnOpenOrders = new DevExpress.XtraEditors.SimpleButton();
            this.btnMyOrders = new DevExpress.XtraEditors.SimpleButton();
            this.btnModifyOrder = new DevExpress.XtraEditors.SimpleButton();
            this.btnNewOrder = new DevExpress.XtraEditors.SimpleButton();
            this.dataRepeater1.ItemTemplate.SuspendLayout();
            this.dataRepeater1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYekun.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUmMebleg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTicket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTicketNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // dataRepeater1
            // 
            resources.ApplyResources(this.dataRepeater1, "dataRepeater1");
            this.dataRepeater1.BackColor = System.Drawing.Color.White;
            // 
            // dataRepeater1.ItemTemplate
            // 
            resources.ApplyResources(this.dataRepeater1.ItemTemplate, "dataRepeater1.ItemTemplate");
            this.dataRepeater1.ItemTemplate.Controls.Add(this.lblOrderStatus);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.lblOid);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.simpleButton1);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtYekun);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtDiscount);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtOrderDate);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtUmMebleg);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtTicket);
            this.dataRepeater1.ItemTemplate.ForeColor = System.Drawing.Color.White;
            this.dataRepeater1.Name = "dataRepeater1";
            // 
            // lblOrderStatus
            // 
            resources.ApplyResources(this.lblOrderStatus, "lblOrderStatus");
            this.lblOrderStatus.Name = "lblOrderStatus";
            // 
            // lblOid
            // 
            resources.ApplyResources(this.lblOid, "lblOid");
            this.lblOid.Name = "lblOid";
            // 
            // simpleButton1
            // 
            resources.ApplyResources(this.simpleButton1, "simpleButton1");
            this.simpleButton1.Image = global::MinAFastFoodClient.Properties.Resources.print_icon__1_;
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // txtYekun
            // 
            resources.ApplyResources(this.txtYekun, "txtYekun");
            this.txtYekun.Name = "txtYekun";
            this.txtYekun.Properties.AccessibleDescription = resources.GetString("txtYekun.Properties.AccessibleDescription");
            this.txtYekun.Properties.AccessibleName = resources.GetString("txtYekun.Properties.AccessibleName");
            this.txtYekun.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtYekun.Properties.Appearance.BackColor")));
            this.txtYekun.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtYekun.Properties.Appearance.Font")));
            this.txtYekun.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtYekun.Properties.Appearance.ForeColor")));
            this.txtYekun.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtYekun.Properties.Appearance.GradientMode")));
            this.txtYekun.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtYekun.Properties.Appearance.Image")));
            this.txtYekun.Properties.Appearance.Options.UseBackColor = true;
            this.txtYekun.Properties.Appearance.Options.UseFont = true;
            this.txtYekun.Properties.Appearance.Options.UseForeColor = true;
            this.txtYekun.Properties.AutoHeight = ((bool)(resources.GetObject("txtYekun.Properties.AutoHeight")));
            this.txtYekun.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtYekun.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtYekun.Properties.Mask.AutoComplete")));
            this.txtYekun.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtYekun.Properties.Mask.BeepOnError")));
            this.txtYekun.Properties.Mask.EditMask = resources.GetString("txtYekun.Properties.Mask.EditMask");
            this.txtYekun.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtYekun.Properties.Mask.IgnoreMaskBlank")));
            this.txtYekun.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtYekun.Properties.Mask.MaskType")));
            this.txtYekun.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtYekun.Properties.Mask.PlaceHolder")));
            this.txtYekun.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtYekun.Properties.Mask.SaveLiteral")));
            this.txtYekun.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtYekun.Properties.Mask.ShowPlaceHolders")));
            this.txtYekun.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtYekun.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtYekun.Properties.NullValuePrompt = resources.GetString("txtYekun.Properties.NullValuePrompt");
            this.txtYekun.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtYekun.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // txtDiscount
            // 
            resources.ApplyResources(this.txtDiscount, "txtDiscount");
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Properties.AccessibleDescription = resources.GetString("txtDiscount.Properties.AccessibleDescription");
            this.txtDiscount.Properties.AccessibleName = resources.GetString("txtDiscount.Properties.AccessibleName");
            this.txtDiscount.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtDiscount.Properties.Appearance.BackColor")));
            this.txtDiscount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtDiscount.Properties.Appearance.Font")));
            this.txtDiscount.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtDiscount.Properties.Appearance.ForeColor")));
            this.txtDiscount.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtDiscount.Properties.Appearance.GradientMode")));
            this.txtDiscount.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtDiscount.Properties.Appearance.Image")));
            this.txtDiscount.Properties.Appearance.Options.UseBackColor = true;
            this.txtDiscount.Properties.Appearance.Options.UseFont = true;
            this.txtDiscount.Properties.Appearance.Options.UseForeColor = true;
            this.txtDiscount.Properties.AutoHeight = ((bool)(resources.GetObject("txtDiscount.Properties.AutoHeight")));
            this.txtDiscount.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtDiscount.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtDiscount.Properties.Mask.AutoComplete")));
            this.txtDiscount.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtDiscount.Properties.Mask.BeepOnError")));
            this.txtDiscount.Properties.Mask.EditMask = resources.GetString("txtDiscount.Properties.Mask.EditMask");
            this.txtDiscount.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtDiscount.Properties.Mask.IgnoreMaskBlank")));
            this.txtDiscount.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtDiscount.Properties.Mask.MaskType")));
            this.txtDiscount.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtDiscount.Properties.Mask.PlaceHolder")));
            this.txtDiscount.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtDiscount.Properties.Mask.SaveLiteral")));
            this.txtDiscount.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtDiscount.Properties.Mask.ShowPlaceHolders")));
            this.txtDiscount.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtDiscount.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtDiscount.Properties.NullValuePrompt = resources.GetString("txtDiscount.Properties.NullValuePrompt");
            this.txtDiscount.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtDiscount.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // txtOrderDate
            // 
            resources.ApplyResources(this.txtOrderDate, "txtOrderDate");
            this.txtOrderDate.Name = "txtOrderDate";
            this.txtOrderDate.Properties.AccessibleDescription = resources.GetString("txtOrderDate.Properties.AccessibleDescription");
            this.txtOrderDate.Properties.AccessibleName = resources.GetString("txtOrderDate.Properties.AccessibleName");
            this.txtOrderDate.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtOrderDate.Properties.Appearance.BackColor")));
            this.txtOrderDate.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtOrderDate.Properties.Appearance.Font")));
            this.txtOrderDate.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtOrderDate.Properties.Appearance.ForeColor")));
            this.txtOrderDate.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtOrderDate.Properties.Appearance.GradientMode")));
            this.txtOrderDate.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtOrderDate.Properties.Appearance.Image")));
            this.txtOrderDate.Properties.Appearance.Options.UseBackColor = true;
            this.txtOrderDate.Properties.Appearance.Options.UseFont = true;
            this.txtOrderDate.Properties.Appearance.Options.UseForeColor = true;
            this.txtOrderDate.Properties.AutoHeight = ((bool)(resources.GetObject("txtOrderDate.Properties.AutoHeight")));
            this.txtOrderDate.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtOrderDate.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtOrderDate.Properties.Mask.AutoComplete")));
            this.txtOrderDate.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtOrderDate.Properties.Mask.BeepOnError")));
            this.txtOrderDate.Properties.Mask.EditMask = resources.GetString("txtOrderDate.Properties.Mask.EditMask");
            this.txtOrderDate.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtOrderDate.Properties.Mask.IgnoreMaskBlank")));
            this.txtOrderDate.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtOrderDate.Properties.Mask.MaskType")));
            this.txtOrderDate.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtOrderDate.Properties.Mask.PlaceHolder")));
            this.txtOrderDate.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtOrderDate.Properties.Mask.SaveLiteral")));
            this.txtOrderDate.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtOrderDate.Properties.Mask.ShowPlaceHolders")));
            this.txtOrderDate.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtOrderDate.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtOrderDate.Properties.NullValuePrompt = resources.GetString("txtOrderDate.Properties.NullValuePrompt");
            this.txtOrderDate.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtOrderDate.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // txtUmMebleg
            // 
            resources.ApplyResources(this.txtUmMebleg, "txtUmMebleg");
            this.txtUmMebleg.Name = "txtUmMebleg";
            this.txtUmMebleg.Properties.AccessibleDescription = resources.GetString("txtUmMebleg.Properties.AccessibleDescription");
            this.txtUmMebleg.Properties.AccessibleName = resources.GetString("txtUmMebleg.Properties.AccessibleName");
            this.txtUmMebleg.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtUmMebleg.Properties.Appearance.BackColor")));
            this.txtUmMebleg.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtUmMebleg.Properties.Appearance.Font")));
            this.txtUmMebleg.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtUmMebleg.Properties.Appearance.ForeColor")));
            this.txtUmMebleg.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtUmMebleg.Properties.Appearance.GradientMode")));
            this.txtUmMebleg.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtUmMebleg.Properties.Appearance.Image")));
            this.txtUmMebleg.Properties.Appearance.Options.UseBackColor = true;
            this.txtUmMebleg.Properties.Appearance.Options.UseFont = true;
            this.txtUmMebleg.Properties.Appearance.Options.UseForeColor = true;
            this.txtUmMebleg.Properties.AutoHeight = ((bool)(resources.GetObject("txtUmMebleg.Properties.AutoHeight")));
            this.txtUmMebleg.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtUmMebleg.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtUmMebleg.Properties.Mask.AutoComplete")));
            this.txtUmMebleg.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.BeepOnError")));
            this.txtUmMebleg.Properties.Mask.EditMask = resources.GetString("txtUmMebleg.Properties.Mask.EditMask");
            this.txtUmMebleg.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.IgnoreMaskBlank")));
            this.txtUmMebleg.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtUmMebleg.Properties.Mask.MaskType")));
            this.txtUmMebleg.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtUmMebleg.Properties.Mask.PlaceHolder")));
            this.txtUmMebleg.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.SaveLiteral")));
            this.txtUmMebleg.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.ShowPlaceHolders")));
            this.txtUmMebleg.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtUmMebleg.Properties.NullValuePrompt = resources.GetString("txtUmMebleg.Properties.NullValuePrompt");
            this.txtUmMebleg.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtUmMebleg.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // txtTicket
            // 
            resources.ApplyResources(this.txtTicket, "txtTicket");
            this.txtTicket.Name = "txtTicket";
            this.txtTicket.Properties.AccessibleDescription = resources.GetString("txtTicket.Properties.AccessibleDescription");
            this.txtTicket.Properties.AccessibleName = resources.GetString("txtTicket.Properties.AccessibleName");
            this.txtTicket.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtTicket.Properties.Appearance.BackColor")));
            this.txtTicket.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtTicket.Properties.Appearance.Font")));
            this.txtTicket.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtTicket.Properties.Appearance.ForeColor")));
            this.txtTicket.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtTicket.Properties.Appearance.GradientMode")));
            this.txtTicket.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtTicket.Properties.Appearance.Image")));
            this.txtTicket.Properties.Appearance.Options.UseBackColor = true;
            this.txtTicket.Properties.Appearance.Options.UseFont = true;
            this.txtTicket.Properties.Appearance.Options.UseForeColor = true;
            this.txtTicket.Properties.AutoHeight = ((bool)(resources.GetObject("txtTicket.Properties.AutoHeight")));
            this.txtTicket.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txtTicket.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtTicket.Properties.Mask.AutoComplete")));
            this.txtTicket.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtTicket.Properties.Mask.BeepOnError")));
            this.txtTicket.Properties.Mask.EditMask = resources.GetString("txtTicket.Properties.Mask.EditMask");
            this.txtTicket.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtTicket.Properties.Mask.IgnoreMaskBlank")));
            this.txtTicket.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtTicket.Properties.Mask.MaskType")));
            this.txtTicket.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtTicket.Properties.Mask.PlaceHolder")));
            this.txtTicket.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtTicket.Properties.Mask.SaveLiteral")));
            this.txtTicket.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtTicket.Properties.Mask.ShowPlaceHolders")));
            this.txtTicket.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtTicket.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtTicket.Properties.NullValuePrompt = resources.GetString("txtTicket.Properties.NullValuePrompt");
            this.txtTicket.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtTicket.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // labelControl1
            // 
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.DisabledImage")));
            this.labelControl1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl1.Appearance.Font")));
            this.labelControl1.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl1.Appearance.ForeColor")));
            this.labelControl1.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl1.Appearance.GradientMode")));
            this.labelControl1.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.HoverImage")));
            this.labelControl1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.Image")));
            this.labelControl1.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.PressedImage")));
            this.labelControl1.Name = "labelControl1";
            // 
            // labelControl2
            // 
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.DisabledImage")));
            this.labelControl2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl2.Appearance.Font")));
            this.labelControl2.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl2.Appearance.ForeColor")));
            this.labelControl2.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl2.Appearance.GradientMode")));
            this.labelControl2.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.HoverImage")));
            this.labelControl2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.Image")));
            this.labelControl2.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.PressedImage")));
            this.labelControl2.Name = "labelControl2";
            // 
            // labelControl3
            // 
            resources.ApplyResources(this.labelControl3, "labelControl3");
            this.labelControl3.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.DisabledImage")));
            this.labelControl3.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl3.Appearance.Font")));
            this.labelControl3.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl3.Appearance.ForeColor")));
            this.labelControl3.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl3.Appearance.GradientMode")));
            this.labelControl3.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.HoverImage")));
            this.labelControl3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.Image")));
            this.labelControl3.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.PressedImage")));
            this.labelControl3.Name = "labelControl3";
            // 
            // labelControl4
            // 
            resources.ApplyResources(this.labelControl4, "labelControl4");
            this.labelControl4.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.DisabledImage")));
            this.labelControl4.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl4.Appearance.Font")));
            this.labelControl4.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl4.Appearance.ForeColor")));
            this.labelControl4.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl4.Appearance.GradientMode")));
            this.labelControl4.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.HoverImage")));
            this.labelControl4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.Image")));
            this.labelControl4.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.PressedImage")));
            this.labelControl4.Name = "labelControl4";
            // 
            // labelControl5
            // 
            resources.ApplyResources(this.labelControl5, "labelControl5");
            this.labelControl5.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.DisabledImage")));
            this.labelControl5.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl5.Appearance.Font")));
            this.labelControl5.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl5.Appearance.ForeColor")));
            this.labelControl5.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl5.Appearance.GradientMode")));
            this.labelControl5.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.HoverImage")));
            this.labelControl5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.Image")));
            this.labelControl5.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.PressedImage")));
            this.labelControl5.Name = "labelControl5";
            // 
            // labelControl6
            // 
            resources.ApplyResources(this.labelControl6, "labelControl6");
            this.labelControl6.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.DisabledImage")));
            this.labelControl6.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl6.Appearance.Font")));
            this.labelControl6.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl6.Appearance.ForeColor")));
            this.labelControl6.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl6.Appearance.GradientMode")));
            this.labelControl6.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.HoverImage")));
            this.labelControl6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.Image")));
            this.labelControl6.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.PressedImage")));
            this.labelControl6.Name = "labelControl6";
            // 
            // labelControl7
            // 
            resources.ApplyResources(this.labelControl7, "labelControl7");
            this.labelControl7.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.DisabledImage")));
            this.labelControl7.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl7.Appearance.Font")));
            this.labelControl7.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl7.Appearance.ForeColor")));
            this.labelControl7.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl7.Appearance.GradientMode")));
            this.labelControl7.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.HoverImage")));
            this.labelControl7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.Image")));
            this.labelControl7.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.PressedImage")));
            this.labelControl7.Name = "labelControl7";
            // 
            // labelControl8
            // 
            resources.ApplyResources(this.labelControl8, "labelControl8");
            this.labelControl8.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.DisabledImage")));
            this.labelControl8.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl8.Appearance.Font")));
            this.labelControl8.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl8.Appearance.ForeColor")));
            this.labelControl8.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl8.Appearance.GradientMode")));
            this.labelControl8.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.HoverImage")));
            this.labelControl8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.Image")));
            this.labelControl8.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.PressedImage")));
            this.labelControl8.Name = "labelControl8";
            // 
            // txtTicketNumber
            // 
            resources.ApplyResources(this.txtTicketNumber, "txtTicketNumber");
            this.txtTicketNumber.Name = "txtTicketNumber";
            this.txtTicketNumber.Properties.AccessibleDescription = resources.GetString("txtTicketNumber.Properties.AccessibleDescription");
            this.txtTicketNumber.Properties.AccessibleName = resources.GetString("txtTicketNumber.Properties.AccessibleName");
            this.txtTicketNumber.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtTicketNumber.Properties.Appearance.Font")));
            this.txtTicketNumber.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtTicketNumber.Properties.Appearance.GradientMode")));
            this.txtTicketNumber.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtTicketNumber.Properties.Appearance.Image")));
            this.txtTicketNumber.Properties.Appearance.Options.UseFont = true;
            this.txtTicketNumber.Properties.AutoHeight = ((bool)(resources.GetObject("txtTicketNumber.Properties.AutoHeight")));
            this.txtTicketNumber.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtTicketNumber.Properties.Mask.AutoComplete")));
            this.txtTicketNumber.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtTicketNumber.Properties.Mask.BeepOnError")));
            this.txtTicketNumber.Properties.Mask.EditMask = resources.GetString("txtTicketNumber.Properties.Mask.EditMask");
            this.txtTicketNumber.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtTicketNumber.Properties.Mask.IgnoreMaskBlank")));
            this.txtTicketNumber.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtTicketNumber.Properties.Mask.MaskType")));
            this.txtTicketNumber.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtTicketNumber.Properties.Mask.PlaceHolder")));
            this.txtTicketNumber.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtTicketNumber.Properties.Mask.SaveLiteral")));
            this.txtTicketNumber.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtTicketNumber.Properties.Mask.ShowPlaceHolders")));
            this.txtTicketNumber.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtTicketNumber.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtTicketNumber.Properties.NullValuePrompt = resources.GetString("txtTicketNumber.Properties.NullValuePrompt");
            this.txtTicketNumber.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtTicketNumber.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // btn7
            // 
            resources.ApplyResources(this.btn7, "btn7");
            this.btn7.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn7.Appearance.Font")));
            this.btn7.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn7.Appearance.GradientMode")));
            this.btn7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn7.Appearance.Image")));
            this.btn7.Appearance.Options.UseFont = true;
            this.btn7.Name = "btn7";
            this.btn7.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn8
            // 
            resources.ApplyResources(this.btn8, "btn8");
            this.btn8.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn8.Appearance.Font")));
            this.btn8.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn8.Appearance.GradientMode")));
            this.btn8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn8.Appearance.Image")));
            this.btn8.Appearance.Options.UseFont = true;
            this.btn8.Name = "btn8";
            this.btn8.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn9
            // 
            resources.ApplyResources(this.btn9, "btn9");
            this.btn9.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn9.Appearance.Font")));
            this.btn9.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn9.Appearance.GradientMode")));
            this.btn9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn9.Appearance.Image")));
            this.btn9.Appearance.Options.UseFont = true;
            this.btn9.Name = "btn9";
            this.btn9.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn4
            // 
            resources.ApplyResources(this.btn4, "btn4");
            this.btn4.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn4.Appearance.Font")));
            this.btn4.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn4.Appearance.GradientMode")));
            this.btn4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn4.Appearance.Image")));
            this.btn4.Appearance.Options.UseFont = true;
            this.btn4.Name = "btn4";
            this.btn4.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn1
            // 
            resources.ApplyResources(this.btn1, "btn1");
            this.btn1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn1.Appearance.Font")));
            this.btn1.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn1.Appearance.GradientMode")));
            this.btn1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn1.Appearance.Image")));
            this.btn1.Appearance.Options.UseFont = true;
            this.btn1.Name = "btn1";
            this.btn1.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn5
            // 
            resources.ApplyResources(this.btn5, "btn5");
            this.btn5.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn5.Appearance.Font")));
            this.btn5.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn5.Appearance.GradientMode")));
            this.btn5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn5.Appearance.Image")));
            this.btn5.Appearance.Options.UseFont = true;
            this.btn5.Name = "btn5";
            this.btn5.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn6
            // 
            resources.ApplyResources(this.btn6, "btn6");
            this.btn6.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn6.Appearance.Font")));
            this.btn6.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn6.Appearance.GradientMode")));
            this.btn6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn6.Appearance.Image")));
            this.btn6.Appearance.Options.UseFont = true;
            this.btn6.Name = "btn6";
            this.btn6.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn2
            // 
            resources.ApplyResources(this.btn2, "btn2");
            this.btn2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn2.Appearance.Font")));
            this.btn2.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn2.Appearance.GradientMode")));
            this.btn2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn2.Appearance.Image")));
            this.btn2.Appearance.Options.UseFont = true;
            this.btn2.Name = "btn2";
            this.btn2.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn3
            // 
            resources.ApplyResources(this.btn3, "btn3");
            this.btn3.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn3.Appearance.Font")));
            this.btn3.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn3.Appearance.GradientMode")));
            this.btn3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn3.Appearance.Image")));
            this.btn3.Appearance.Options.UseFont = true;
            this.btn3.Name = "btn3";
            this.btn3.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btn0
            // 
            resources.ApplyResources(this.btn0, "btn0");
            this.btn0.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btn0.Appearance.Font")));
            this.btn0.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btn0.Appearance.GradientMode")));
            this.btn0.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btn0.Appearance.Image")));
            this.btn0.Appearance.Options.UseFont = true;
            this.btn0.Name = "btn0";
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // txtDate
            // 
            resources.ApplyResources(this.txtDate, "txtDate");
            this.txtDate.Name = "txtDate";
            this.txtDate.Properties.AccessibleDescription = resources.GetString("txtDate.Properties.AccessibleDescription");
            this.txtDate.Properties.AccessibleName = resources.GetString("txtDate.Properties.AccessibleName");
            this.txtDate.Properties.AutoHeight = ((bool)(resources.GetObject("txtDate.Properties.AutoHeight")));
            this.txtDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("txtDate.Properties.Buttons"))))});
            this.txtDate.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtDate.Properties.Mask.AutoComplete")));
            this.txtDate.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtDate.Properties.Mask.BeepOnError")));
            this.txtDate.Properties.Mask.EditMask = resources.GetString("txtDate.Properties.Mask.EditMask");
            this.txtDate.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtDate.Properties.Mask.IgnoreMaskBlank")));
            this.txtDate.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtDate.Properties.Mask.MaskType")));
            this.txtDate.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtDate.Properties.Mask.PlaceHolder")));
            this.txtDate.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtDate.Properties.Mask.SaveLiteral")));
            this.txtDate.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtDate.Properties.Mask.ShowPlaceHolders")));
            this.txtDate.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtDate.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtDate.Properties.NullValuePrompt = resources.GetString("txtDate.Properties.NullValuePrompt");
            this.txtDate.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtDate.Properties.NullValuePromptShowForEmptyValue")));
            this.txtDate.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txtDate.Properties.VistaTimeProperties.AccessibleDescription = resources.GetString("txtDate.Properties.VistaTimeProperties.AccessibleDescription");
            this.txtDate.Properties.VistaTimeProperties.AccessibleName = resources.GetString("txtDate.Properties.VistaTimeProperties.AccessibleName");
            this.txtDate.Properties.VistaTimeProperties.AutoHeight = ((bool)(resources.GetObject("txtDate.Properties.VistaTimeProperties.AutoHeight")));
            this.txtDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDate.Properties.VistaTimeProperties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtDate.Properties.VistaTimeProperties.Mask.AutoComplete")));
            this.txtDate.Properties.VistaTimeProperties.Mask.BeepOnError = ((bool)(resources.GetObject("txtDate.Properties.VistaTimeProperties.Mask.BeepOnError")));
            this.txtDate.Properties.VistaTimeProperties.Mask.EditMask = resources.GetString("txtDate.Properties.VistaTimeProperties.Mask.EditMask");
            this.txtDate.Properties.VistaTimeProperties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtDate.Properties.VistaTimeProperties.Mask.IgnoreMaskBlank")));
            this.txtDate.Properties.VistaTimeProperties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtDate.Properties.VistaTimeProperties.Mask.MaskType")));
            this.txtDate.Properties.VistaTimeProperties.Mask.PlaceHolder = ((char)(resources.GetObject("txtDate.Properties.VistaTimeProperties.Mask.PlaceHolder")));
            this.txtDate.Properties.VistaTimeProperties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtDate.Properties.VistaTimeProperties.Mask.SaveLiteral")));
            this.txtDate.Properties.VistaTimeProperties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtDate.Properties.VistaTimeProperties.Mask.ShowPlaceHolders")));
            this.txtDate.Properties.VistaTimeProperties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtDate.Properties.VistaTimeProperties.Mask.UseMaskAsDisplayFormat")));
            this.txtDate.Properties.VistaTimeProperties.NullValuePrompt = resources.GetString("txtDate.Properties.VistaTimeProperties.NullValuePrompt");
            this.txtDate.Properties.VistaTimeProperties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtDate.Properties.VistaTimeProperties.NullValuePromptShowForEmptyValue")));
            // 
            // btnAllOrders
            // 
            resources.ApplyResources(this.btnAllOrders, "btnAllOrders");
            this.btnAllOrders.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnAllOrders.Appearance.BackColor")));
            this.btnAllOrders.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnAllOrders.Appearance.Font")));
            this.btnAllOrders.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnAllOrders.Appearance.ForeColor")));
            this.btnAllOrders.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnAllOrders.Appearance.GradientMode")));
            this.btnAllOrders.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnAllOrders.Appearance.Image")));
            this.btnAllOrders.Appearance.Options.UseBackColor = true;
            this.btnAllOrders.Appearance.Options.UseFont = true;
            this.btnAllOrders.Appearance.Options.UseForeColor = true;
            this.btnAllOrders.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnAllOrders.Image = global::MinAFastFoodClient.Properties.Resources.order_history_icon;
            this.btnAllOrders.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnAllOrders.Name = "btnAllOrders";
            this.btnAllOrders.Click += new System.EventHandler(this.btnAllOrders_Click);
            // 
            // btnBackToMain
            // 
            resources.ApplyResources(this.btnBackToMain, "btnBackToMain");
            this.btnBackToMain.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnBackToMain.Appearance.BackColor")));
            this.btnBackToMain.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnBackToMain.Appearance.Font")));
            this.btnBackToMain.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnBackToMain.Appearance.ForeColor")));
            this.btnBackToMain.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnBackToMain.Appearance.GradientMode")));
            this.btnBackToMain.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnBackToMain.Appearance.Image")));
            this.btnBackToMain.Appearance.Options.UseBackColor = true;
            this.btnBackToMain.Appearance.Options.UseFont = true;
            this.btnBackToMain.Appearance.Options.UseForeColor = true;
            this.btnBackToMain.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnBackToMain.Image = global::MinAFastFoodClient.Properties.Resources.go_back_icon;
            this.btnBackToMain.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnBackToMain.Name = "btnBackToMain";
            this.btnBackToMain.Click += new System.EventHandler(this.btnBackToMain_Click);
            // 
            // btnVoidOrder
            // 
            resources.ApplyResources(this.btnVoidOrder, "btnVoidOrder");
            this.btnVoidOrder.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnVoidOrder.Appearance.BackColor")));
            this.btnVoidOrder.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnVoidOrder.Appearance.Font")));
            this.btnVoidOrder.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnVoidOrder.Appearance.GradientMode")));
            this.btnVoidOrder.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnVoidOrder.Appearance.Image")));
            this.btnVoidOrder.Appearance.Options.UseBackColor = true;
            this.btnVoidOrder.Appearance.Options.UseFont = true;
            this.btnVoidOrder.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnVoidOrder.Image = global::MinAFastFoodClient.Properties.Resources.Document_Delete_icon;
            this.btnVoidOrder.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnVoidOrder.Name = "btnVoidOrder";
            this.btnVoidOrder.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // btnClosedOrders
            // 
            resources.ApplyResources(this.btnClosedOrders, "btnClosedOrders");
            this.btnClosedOrders.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnClosedOrders.Appearance.BackColor")));
            this.btnClosedOrders.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnClosedOrders.Appearance.Font")));
            this.btnClosedOrders.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnClosedOrders.Appearance.ForeColor")));
            this.btnClosedOrders.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnClosedOrders.Appearance.GradientMode")));
            this.btnClosedOrders.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnClosedOrders.Appearance.Image")));
            this.btnClosedOrders.Appearance.Options.UseBackColor = true;
            this.btnClosedOrders.Appearance.Options.UseFont = true;
            this.btnClosedOrders.Appearance.Options.UseForeColor = true;
            this.btnClosedOrders.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnClosedOrders.Image = global::MinAFastFoodClient.Properties.Resources.pay;
            this.btnClosedOrders.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnClosedOrders.Name = "btnClosedOrders";
            this.btnClosedOrders.Click += new System.EventHandler(this.btnClosedOrders_Click);
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnSearch.Appearance.Font")));
            this.btnSearch.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnSearch.Appearance.GradientMode")));
            this.btnSearch.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Appearance.Image")));
            this.btnSearch.Appearance.Options.UseFont = true;
            this.btnSearch.Image = global::MinAFastFoodClient.Properties.Resources._1365941552_analysis;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnBack
            // 
            resources.ApplyResources(this.btnBack, "btnBack");
            this.btnBack.Image = global::MinAFastFoodClient.Properties.Resources.Previous_icon;
            this.btnBack.Name = "btnBack";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnOpenOrders
            // 
            resources.ApplyResources(this.btnOpenOrders, "btnOpenOrders");
            this.btnOpenOrders.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnOpenOrders.Appearance.BackColor")));
            this.btnOpenOrders.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnOpenOrders.Appearance.Font")));
            this.btnOpenOrders.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnOpenOrders.Appearance.ForeColor")));
            this.btnOpenOrders.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnOpenOrders.Appearance.GradientMode")));
            this.btnOpenOrders.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenOrders.Appearance.Image")));
            this.btnOpenOrders.Appearance.Options.UseBackColor = true;
            this.btnOpenOrders.Appearance.Options.UseFont = true;
            this.btnOpenOrders.Appearance.Options.UseForeColor = true;
            this.btnOpenOrders.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnOpenOrders.Image = global::MinAFastFoodClient.Properties.Resources.user_group_icon;
            this.btnOpenOrders.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnOpenOrders.Name = "btnOpenOrders";
            this.btnOpenOrders.Click += new System.EventHandler(this.btnOpenOrders_Click);
            // 
            // btnMyOrders
            // 
            resources.ApplyResources(this.btnMyOrders, "btnMyOrders");
            this.btnMyOrders.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnMyOrders.Appearance.BackColor")));
            this.btnMyOrders.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnMyOrders.Appearance.Font")));
            this.btnMyOrders.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnMyOrders.Appearance.ForeColor")));
            this.btnMyOrders.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnMyOrders.Appearance.GradientMode")));
            this.btnMyOrders.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnMyOrders.Appearance.Image")));
            this.btnMyOrders.Appearance.Options.UseBackColor = true;
            this.btnMyOrders.Appearance.Options.UseFont = true;
            this.btnMyOrders.Appearance.Options.UseForeColor = true;
            this.btnMyOrders.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnMyOrders.Image = global::MinAFastFoodClient.Properties.Resources.Office_Customer_Male_Dark_icon;
            this.btnMyOrders.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnMyOrders.Name = "btnMyOrders";
            this.btnMyOrders.Click += new System.EventHandler(this.btnMyOrders_Click);
            // 
            // btnModifyOrder
            // 
            resources.ApplyResources(this.btnModifyOrder, "btnModifyOrder");
            this.btnModifyOrder.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnModifyOrder.Appearance.BackColor")));
            this.btnModifyOrder.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnModifyOrder.Appearance.Font")));
            this.btnModifyOrder.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnModifyOrder.Appearance.GradientMode")));
            this.btnModifyOrder.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnModifyOrder.Appearance.Image")));
            this.btnModifyOrder.Appearance.Options.UseBackColor = true;
            this.btnModifyOrder.Appearance.Options.UseFont = true;
            this.btnModifyOrder.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnModifyOrder.Image = global::MinAFastFoodClient.Properties.Resources.Notepad_Bloc_notes_icon;
            this.btnModifyOrder.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnModifyOrder.Name = "btnModifyOrder";
            this.btnModifyOrder.Click += new System.EventHandler(this.btnModifyOrder_Click);
            // 
            // btnNewOrder
            // 
            resources.ApplyResources(this.btnNewOrder, "btnNewOrder");
            this.btnNewOrder.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnNewOrder.Appearance.BackColor")));
            this.btnNewOrder.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnNewOrder.Appearance.Font")));
            this.btnNewOrder.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnNewOrder.Appearance.ForeColor")));
            this.btnNewOrder.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnNewOrder.Appearance.GradientMode")));
            this.btnNewOrder.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnNewOrder.Appearance.Image")));
            this.btnNewOrder.Appearance.Options.UseBackColor = true;
            this.btnNewOrder.Appearance.Options.UseFont = true;
            this.btnNewOrder.Appearance.Options.UseForeColor = true;
            this.btnNewOrder.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnNewOrder.Image = global::MinAFastFoodClient.Properties.Resources.Actions_appointment_new_icon;
            this.btnNewOrder.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnNewOrder.Name = "btnNewOrder";
            this.btnNewOrder.Click += new System.EventHandler(this.btnNewOrder_Click);
            // 
            // OrdersListView
            // 
            resources.ApplyResources(this, "$this");
            this.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("OrdersListView.Appearance.ForeColor")));
            this.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("OrdersListView.Appearance.GradientMode")));
            this.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("OrdersListView.Appearance.Image")));
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.btnAllOrders);
            this.Controls.Add(this.btnBackToMain);
            this.Controls.Add(this.btnVoidOrder);
            this.Controls.Add(this.btnClosedOrders);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.txtTicketNumber);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.btnOpenOrders);
            this.Controls.Add(this.btnMyOrders);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.btnModifyOrder);
            this.Controls.Add(this.btnNewOrder);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.dataRepeater1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrdersListView";
            this.Load += new System.EventHandler(this.OrdersListView_Load);
            this.dataRepeater1.ItemTemplate.ResumeLayout(false);
            this.dataRepeater1.ItemTemplate.PerformLayout();
            this.dataRepeater1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtYekun.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUmMebleg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTicket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTicketNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater1;
        private DevExpress.XtraEditors.TextEdit txtYekun;
        private DevExpress.XtraEditors.TextEdit txtDiscount;
        private DevExpress.XtraEditors.TextEdit txtOrderDate;
        private DevExpress.XtraEditors.TextEdit txtUmMebleg;
        private DevExpress.XtraEditors.TextEdit txtTicket;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl lblOid;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton btnNewOrder;
        private DevExpress.XtraEditors.SimpleButton btnModifyOrder;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SimpleButton btnMyOrders;
        private DevExpress.XtraEditors.SimpleButton btnOpenOrders;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtTicketNumber;
        private DevExpress.XtraEditors.SimpleButton btnBack;
        private DevExpress.XtraEditors.SimpleButton btn7;
        private DevExpress.XtraEditors.SimpleButton btn8;
        private DevExpress.XtraEditors.SimpleButton btn9;
        private DevExpress.XtraEditors.SimpleButton btn4;
        private DevExpress.XtraEditors.SimpleButton btn1;
        private DevExpress.XtraEditors.SimpleButton btn5;
        private DevExpress.XtraEditors.SimpleButton btn6;
        private DevExpress.XtraEditors.SimpleButton btn2;
        private DevExpress.XtraEditors.SimpleButton btn3;
        private DevExpress.XtraEditors.SimpleButton btn0;
        private DevExpress.XtraEditors.SimpleButton btnSearch;
        private DevExpress.XtraEditors.SimpleButton btnClosedOrders;
        private DevExpress.XtraEditors.LabelControl lblOrderStatus;
        private DevExpress.XtraEditors.SimpleButton btnVoidOrder;
        private DevExpress.XtraEditors.SimpleButton btnBackToMain;
        private DevExpress.XtraEditors.SimpleButton btnAllOrders;
        private DevExpress.XtraEditors.DateEdit txtDate;
    }
}