﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using System.Globalization;

namespace MinAFastFoodClient
{
    public partial class DiscountChoise : DevExpress.XtraEditors.XtraForm
    {
        private int language;
        public DiscountChoise(int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.language = language;
            
        }
        public DiscountChoise(int itemOrder,int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.language = language;
        }

        

        private void btnDiscOrder_Click(object sender, EventArgs e)
        {
            Discounts discounts = new Discounts("O",this.language);
            this.Hide();
            discounts.ShowDialog();
            Close();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
         
            ItemForDiscount ifd = new ItemForDiscount(this.language);
            this.Hide();
            ifd.ShowDialog();
            Close();
        }

        private void DiscountChoise_Load(object sender, EventArgs e)
        {
            if (MinAFastFoodClient.Menu.orderItems.Count <= 0)
                btnDiscItem.Enabled = false;
        }

        
    }
}