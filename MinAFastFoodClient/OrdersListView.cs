﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using MinAFastFoodClient.Classes;

namespace MinAFastFoodClient
{
    public partial class OrdersListView : DevExpress.XtraEditors.XtraForm
    {
        private User user;
        private int language;
        ResourceManager LocRM = new ResourceManager("MinAFastFoodClient.FastFoodLanguage", typeof(OrdersListView).Assembly);

        public OrdersListView(User user, int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.user = new User();
            this.user = user;

            if (user.AllowModifyOrder)
                btnModifyOrder.Enabled = true;
            else
                btnModifyOrder.Enabled = false;

            if (user.AllowVoidOrder)
                btnVoidOrder.Enabled = true;
            else
                btnVoidOrder.Enabled = false;
            this.language = language;
        }

        private List<Order> orders;

        protected DataSet PDataset(string select_statement)
        {
            SqlConnection conn = new SqlConnection(Utility.DBHelper.ConnectionString);
            SqlCommand cmd = new SqlCommand(select_statement, conn);
            conn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            ad.Fill(ds);

            conn.Close();
            return ds;
        }

        private void bindDataRepeator()
        {
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = orders;
            txtDiscount.DataBindings.Clear();
            txtOrderDate.DataBindings.Clear();
            txtTicket.DataBindings.Clear();
            txtUmMebleg.DataBindings.Clear();
            txtYekun.DataBindings.Clear();
            lblOid.DataBindings.Clear();
            lblOrderStatus.DataBindings.Clear();
            txtYekun.DataBindings.Add("Text", bindingSource1, "Yekun");
            txtUmMebleg.DataBindings.Add("Text", bindingSource1, "UmumiMebleg");
            txtOrderDate.DataBindings.Add("Text", bindingSource1, "OrderDate");
            txtDiscount.DataBindings.Add("Text", bindingSource1, "Discount");
            txtTicket.DataBindings.Add("Text", bindingSource1, "Ticket");
            lblOrderStatus.DataBindings.Add("Text", bindingSource1, "OrderStatus");
            lblOid.DataBindings.Add("Text", bindingSource1, "Oid");
            dataRepeater1.DataSource = bindingSource1;
            //  dataRepeater1.CurrentItemIndex = dataRepeater1.ItemCount - 1;
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            txtTicketNumber.Text = txtTicketNumber.Text + btn.Text;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                // int index = txtNumbers.SelectionStart;
                //  txtNumbers.Text = txtNumbers.Text.Remove(txtNumbers.SelectionStart - 1, 1);
                //   txtNumbers.Select(index - 1, 1);
                //    txtNumbers.Focus();
                txtTicketNumber.Text = txtTicketNumber.Text.Remove(txtTicketNumber.Text.Length - 1, 1);
            }
            catch (Exception ex)
            {
            }
        }

        private void btnNewOrder_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu(this.user, 1);

            Hide();

            menu.ShowDialog();
            Close();
        }

        private void btnMyOrders_Click(object sender, EventArgs e)
        {
            orders = new List<Order>();
            DataSet ds = PDataset("SELECT   [Oid],[OrderDate],[Amount],[TotalAmount],[OrderStatus],[TicketNumber],[DiscountAmount] FROM  [Orders] where [Employee]=N'" + this.user.UserEmployee.ToString() + "' and ([OrderStatus]=0 or [OrderStatus]=1) and  DATEADD(dd, 0, DATEDIFF(dd, 0, OrderDate))=DATEADD(dd, 0, DATEDIFF(dd, 0, '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))");
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Order o = new Order();
                o.Oid = dr[0].ToString();
                o.OrderDate = dr[1].ToString();
                o.UmumiMebleg = Math.Round(decimal.Parse(dr[2].ToString()), 2);
                o.Yekun = Math.Round(decimal.Parse(dr[3].ToString()), 2);
                o.OrderStatus = Int32.Parse(dr[4].ToString());
                o.Ticket = Int32.Parse(dr[5].ToString()).ToString();
                if (!string.IsNullOrEmpty(dr[6].ToString()))
                    o.Discount = Math.Round(decimal.Parse(dr[6].ToString()), 2);
                else
                    o.Discount = 0.00m;
                orders.Add(o);
            }
            bindDataRepeator();
        }

        private void btnOpenOrders_Click(object sender, EventArgs e)
        {
            orders = new List<Order>();
            DataSet ds = PDataset("SELECT   [Oid],[OrderDate],[Amount],[TotalAmount],[OrderStatus],[TicketNumber],[DiscountAmount] FROM [Orders] where [OrderStatus]=0 or [OrderStatus]=1");
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Order o = new Order();
                o.Oid = dr[0].ToString();
                o.OrderDate = dr[1].ToString();
                o.UmumiMebleg = Math.Round(decimal.Parse(dr[2].ToString()), 2);
                o.Yekun = Math.Round(decimal.Parse(dr[3].ToString()), 2);
                o.OrderStatus = Int32.Parse(dr[4].ToString());
                o.Ticket = Int32.Parse(dr[5].ToString()).ToString();
                if (!string.IsNullOrEmpty(dr[6].ToString()))
                    o.Discount = Math.Round(decimal.Parse(dr[6].ToString()), 2);
                else
                    o.Discount = 0.00m;
                orders.Add(o);
            }
            bindDataRepeator();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            orders = new List<Order>();
            int ticketNumber;
            if (int.TryParse(txtTicketNumber.Text, out ticketNumber))
            {
                DataSet ds = PDataset("SELECT   [Oid],[OrderDate],[Amount],[TotalAmount],[OrderStatus],[TicketNumber],[DiscountAmount] FROM [Orders] where [TicketNumber]=" + ticketNumber + " and ([OrderStatus]=0 or [OrderStatus]=1 or [OrderStatus]=2 ) and DATEADD(dd, 0, DATEDIFF(dd, 0, OrderDate))=DATEADD(dd, 0, DATEDIFF(dd, 0, '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))");
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Order o = new Order();
                    o.Oid = dr[0].ToString();
                    o.OrderDate = dr[1].ToString();
                    o.UmumiMebleg = Math.Round(decimal.Parse(dr[2].ToString()), 2);
                    o.Yekun = Math.Round(decimal.Parse(dr[3].ToString()), 2);
                    o.OrderStatus = Int32.Parse(dr[4].ToString());
                    o.Ticket = Int32.Parse(dr[5].ToString()).ToString();
                    if (!string.IsNullOrEmpty(dr[6].ToString()))
                        o.Discount = Math.Round(decimal.Parse(dr[6].ToString()), 2);
                    else
                        o.Discount = 0.00m;
                    orders.Add(o);
                }

                if (orders.Count > 0)
                    bindDataRepeator();
                else
                {
                    bindDataRepeator();
                    XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzNothingFounded") : LocRM.GetString("EngNothingFounded"));
                }
            }
        }

        //
        private void OrdersListView_Load(object sender, EventArgs e)
        {
            orders = new List<Order>();
            DataSet ds = PDataset("SELECT   [Oid],[OrderDate],[Amount],[TotalAmount],[OrderStatus],[TicketNumber],[DiscountAmount] FROM [Orders] where ([OrderStatus]=0 or [OrderStatus]=1)  and  DATEADD(dd, 0, DATEDIFF(dd, 0, OrderDate))=DATEADD(dd, 0, DATEDIFF(dd, 0, '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))");
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Order o = new Order();
                o.Oid = dr[0].ToString();
                o.OrderDate = dr[1].ToString();
                o.UmumiMebleg = Math.Round(decimal.Parse(dr[2].ToString()), 2);
                o.Yekun = Math.Round(decimal.Parse(dr[3].ToString()), 2);
                o.OrderStatus = Int32.Parse(dr[4].ToString());
                o.Ticket = Int32.Parse(dr[5].ToString()).ToString();
                if (!string.IsNullOrEmpty(dr[6].ToString()))
                    o.Discount = Math.Round(decimal.Parse(dr[6].ToString()), 2);
                else
                    o.Discount = 0.00m;
                orders.Add(o);
            }
            bindDataRepeator();
            txtDate.EditValue = DateTime.Now.ToShortDateString();
        }

        private void btnModifyOrder_Click(object sender, EventArgs e)
        {
            if (dataRepeater1.Controls.Count > 0)
            {
                LabelControl orderStatus = new LabelControl();
                orderStatus = (LabelControl)dataRepeater1.CurrentItem.Controls["lblOrderStatus"];
                if (int.Parse(orderStatus.Text) == 0 || int.Parse(orderStatus.Text) == 1)
                {
                    LabelControl orderId = new LabelControl();
                    orderId = (LabelControl)dataRepeater1.CurrentItem.Controls["lblOid"];

                    MinAFastFoodClient.Menu menu = new Menu(this.user, orderId.Text, this.language);
                    Hide();
                    menu.ShowDialog();
                    Close();
                }
                else
                {
                    XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzClosedOrderNotChange") : LocRM.GetString("EngClosedOrderNotChange"));
                }
            }

            else
            {
                XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzShouldSOrder") : LocRM.GetString("EngShouldSOrder"));
            }
        }

        private void btnClosedOrders_Click(object sender, EventArgs e)
        {
            orders = new List<Order>();
            DataSet ds = PDataset("SELECT   [Oid],[OrderDate],[DiscountAmount],[Amount],[TotalAmount],[OrderStatus],[TicketNumber] FROM  [Orders] where  [OrderStatus]=2 and  DATEADD(dd, 0, DATEDIFF(dd, 0, OrderDate))=DATEADD(dd, 0, DATEDIFF(dd, 0, '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))");
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Order o = new Order();
                o.Oid = dr[0].ToString();
                o.OrderDate = dr[1].ToString();
                o.UmumiMebleg = Math.Round(decimal.Parse(dr[3].ToString()), 2);
                o.Yekun = Math.Round(decimal.Parse(dr[4].ToString()), 2);
                o.OrderStatus = Int32.Parse(dr[5].ToString());
                o.Ticket = Int32.Parse(dr[6].ToString()).ToString();
                if (!string.IsNullOrEmpty(dr[2].ToString()))
                    o.Discount = Math.Round(decimal.Parse(dr[2].ToString()), 2);
                else
                    o.Discount = 0.00m;
                orders.Add(o);
            }
            bindDataRepeator();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (dataRepeater1.Controls.Count > 0)
            {
                LabelControl orderStatus = new LabelControl();
                orderStatus = (LabelControl)dataRepeater1.CurrentItem.Controls["lblOrderStatus"];
                TextEdit ticketNumber = new TextEdit();
                ticketNumber = (TextEdit)dataRepeater1.CurrentItem.Controls["txtTicket"];
                if (int.Parse(orderStatus.Text) == 0 || int.Parse(orderStatus.Text) == 1)
                {
                    if (MessageBox.Show(this.language == 0 ? LocRM.GetString("AzToDeleteOrder") : LocRM.GetString("EndToDeleteOrder") + ticketNumber.Text + "?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        int pId;
                        pId = System.Diagnostics.Process.Start("osk.exe").Id;
                        LabelControl orderId = new LabelControl();
                        orderId = (LabelControl)dataRepeater1.CurrentItem.Controls["lblOid"];
                        OrderVoidForm v = new OrderVoidForm(orderId.Text, this.language);
                        v.ShowDialog();
                        OrdersListView_Load(null, null);
                    }
                }
                else
                {
                    XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzCannotVoid") : LocRM.GetString("EngCannotVoid"));
                }
            }
            else
            {
                XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzShouldSOrder") : LocRM.GetString("EngShouldSOrder"));
            }
        }

        private void btnBackToMain_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAllOrders_Click(object sender, EventArgs e)
        {
            orders = new List<Order>();
            DataSet ds = PDataset("SELECT   [Oid],[OrderDate],[DiscountAmount],[Amount],[TotalAmount],[OrderStatus],[TicketNumber] FROM  [Orders] where   DATEADD(dd, 0, DATEDIFF(dd, 0, OrderDate))<=DATEADD(dd, 0, DATEDIFF(dd, 0, '" + DateTime.Now.ToString("yyyy-MM-dd") + "'))  and DATEADD(dd, 0, DATEDIFF(dd, 0, OrderDate))>=DATEADD(dd, 0, DATEDIFF(dd, 0, '" + DateTime.Parse(txtDate.Text).ToString("yyyy-MM-dd") + "')) ");
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                Order o = new Order();

                o.Oid = dr[0].ToString();
                o.OrderDate = dr[1].ToString();
                o.UmumiMebleg = Math.Round(decimal.Parse(dr[3].ToString()), 2);
                o.Yekun = Math.Round(decimal.Parse(dr[4].ToString()), 2);

                o.OrderStatus = Int32.Parse(dr[5].ToString());
                if (Int32.Parse(dr[5].ToString()) == 3)
                    o.Ticket = Int32.Parse(dr[6].ToString()).ToString() + " X ";
                else
                    o.Ticket = Int32.Parse(dr[6].ToString()).ToString();
                if (!string.IsNullOrEmpty(dr[2].ToString()))
                    o.Discount = Math.Round(decimal.Parse(dr[2].ToString()), 2);
                else
                    o.Discount = 0.00m;
                orders.Add(o);
            }
            bindDataRepeator();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            LabelControl orderStatus = new LabelControl();
            orderStatus = (LabelControl)dataRepeater1.CurrentItem.Controls["lblOrderStatus"];

            LabelControl orderId = new LabelControl();
            orderId = (LabelControl)dataRepeater1.CurrentItem.Controls["lblOid"];

            DataSet firmInfoDS = PDataset("Select (Select Top 1  [Logo] from [Info]) as Logo,(Select Top 1  [CompanyName] from [Info]) as FirmName,(Select Top 1  [PrintComment] from [Info]) as PrintComment,[Amount] as Amount,[TotalAmount] as TotalAmount,[TicketNumber] as TicketNumber,IsNull([DiscountAmount],0.00) as DiscountAmount,'s' as ItemsDiscountAmount,convert(varchar,dateadd(day,-1,OrderDate),104) as OrderDate,convert(varchar,dateadd(day,-1,OrderDate),108) as OrderTime,Isnull((Select TableName from Tables where Oid=TableId),'') as TableName  from [Orders] where [Oid]=N'" + orderId.Text + "'");
            DataTable firmInfoTable = firmInfoDS.Tables[0];
            firmInfoTable.Rows[0]["ItemsDiscountAmount"] = Math.Round(decimal.Parse(firmInfoTable.Rows[0]["Amount"].ToString()) - (decimal.Parse(firmInfoTable.Rows[0]["TotalAmount"].ToString()) + decimal.Parse(firmInfoTable.Rows[0]["DiscountAmount"].ToString())), 2);
            DataSet storeInfoDs = PDataset("SELECT  Distinct  Store.Oid, Store.StoreName as StoreName,Item.Store as StoreOid fROM         OrderDetail INNER JOIN   Item ON  OrderDetail.ItemId =  Item.Oid INNER JOIN       Store ON  Item.Store =Store.Oid where [OrderDetail].[OrderId]=N'" + orderId.Text + "'");
            DataTable storeInfoTable = storeInfoDs.Tables[0];

            DataSet itemsDS = PDataset("SELECT      OrderDetail.ItemId as ItemOid, OrderDetail.Quantity as Quantity,  OrderDetail.ItemName as ItemName,  OrderDetail.OptionsDescription as OptionsDescription,OrderDetail.AnItemsPrice as AnItemPrice,OrderDetail.ItemPrice as ItemsPrice,OrderDetail.OptionsPrice as OptionsPrice, IsNull(OrderDetail.DiscountAmount,0.00) as DiscountAmount, Item.Store as StoreOid,(Select [DiscountType] from [Discounts] where [Discounts].[Oid]=OrderDetail.DiscountId) as DiscId,'s' as DiscType FROM         OrderDetail INNER JOIN   Item ON OrderDetail.ItemId =Item.Oid where OrderDetail.OrderId=N'" + orderId.Text + "' ");
            DataTable itemTable = itemsDS.Tables[0];

            foreach (DataRow d in itemTable.Rows)
            {
                if (!string.IsNullOrEmpty(d["DiscId"].ToString()))
                {
                    if (int.Parse(d["DiscId"].ToString()) == 0)
                        d["DiscType"] = "%";
                    else
                        d["DiscType"] = "";
                }
                else
                {
                    d["DiscType"] = "";
                }
            }
            itemsDS.Tables.Clear();
            firmInfoDS.Tables.Clear();
            storeInfoDs.Tables.Clear();
            DataSet orderBillDataSet = new DataSet();
            firmInfoTable.TableName = "FirmInfo";
            storeInfoTable.TableName = "Stores";
            itemTable.TableName = "Items";
            orderBillDataSet.Tables.Add(firmInfoTable);
            orderBillDataSet.Tables.Add(storeInfoTable);
            orderBillDataSet.Tables.Add(itemTable);

            DataColumn colParent = orderBillDataSet.Tables[1].Columns["StoreOid"];
            DataColumn colChild = orderBillDataSet.Tables[2].Columns["StoreOid"];
            DataRelation drEmployeeStatus = new DataRelation("FK_Stores_Items", colParent, colChild);

            orderBillDataSet.Relations.Add(drEmployeeStatus);
            string msg = "";

            if (Int32.Parse(orderStatus.Text) == 0 || Int32.Parse(orderStatus.Text) == 1)
            {
                updateOrderSetPrinted(orderId.Text, 1);
                msg = this.language == 0 ? LocRM.GetString("AzActive") : LocRM.GetString("EngActive");
            }
            else if (Int32.Parse(orderStatus.Text) == 2)
            {
                updateOrderSetPrinted(orderId.Text, 2);
                msg = this.language == 0 ? LocRM.GetString("AzPaid") : LocRM.GetString("EngPaid");
            }
            else if (Int32.Parse(orderStatus.Text) == 3)
            {
                updateOrderSetPrinted(orderId.Text, 3);
                msg = this.language == 0 ? LocRM.GetString("AzDeleted") : LocRM.GetString("EngDeleted");
            }
            ReportPrintTool printTool = new ReportPrintTool(new Report.OrderBill(orderBillDataSet, msg));
            printTool.Report.CreateDocument(false);

            printTool.ShowPreviewDialog();
        }

        private void updateOrderSetPrinted(string orderId, int orderStatus)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update [Orders] Set [OrderStatus]=" + orderStatus + ",[Printed]=1 where  [Oid]=N'" + orderId + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }
    }
}