﻿namespace MinAFastFoodClient
{
    partial class OrderVoidForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderVoidForm));
            this.txtVoidReason = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnDone = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtVoidReason.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtVoidReason
            // 
            resources.ApplyResources(this.txtVoidReason, "txtVoidReason");
            this.txtVoidReason.Name = "txtVoidReason";
            this.txtVoidReason.Properties.AccessibleDescription = resources.GetString("txtVoidReason.Properties.AccessibleDescription");
            this.txtVoidReason.Properties.AccessibleName = resources.GetString("txtVoidReason.Properties.AccessibleName");
            this.txtVoidReason.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtVoidReason.Properties.Appearance.Font")));
            this.txtVoidReason.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtVoidReason.Properties.Appearance.GradientMode")));
            this.txtVoidReason.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtVoidReason.Properties.Appearance.Image")));
            this.txtVoidReason.Properties.Appearance.Options.UseFont = true;
            this.txtVoidReason.Properties.NullValuePrompt = resources.GetString("txtVoidReason.Properties.NullValuePrompt");
            this.txtVoidReason.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtVoidReason.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // labelControl1
            // 
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.DisabledImage")));
            this.labelControl1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl1.Appearance.Font")));
            this.labelControl1.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl1.Appearance.GradientMode")));
            this.labelControl1.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.HoverImage")));
            this.labelControl1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.Image")));
            this.labelControl1.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.PressedImage")));
            this.labelControl1.Name = "labelControl1";
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Appearance.Font")));
            this.btnCancel.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnCancel.Appearance.GradientMode")));
            this.btnCancel.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Appearance.Image")));
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Image = global::MinAFastFoodClient.Properties.Resources.Actions_dialog_cancel_icon__1_;
            this.btnCancel.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnDone
            // 
            resources.ApplyResources(this.btnDone, "btnDone");
            this.btnDone.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDone.Appearance.Font")));
            this.btnDone.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnDone.Appearance.GradientMode")));
            this.btnDone.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnDone.Appearance.Image")));
            this.btnDone.Appearance.Options.UseFont = true;
            this.btnDone.Image = global::MinAFastFoodClient.Properties.Resources.Accept_icon__1_;
            this.btnDone.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDone.Name = "btnDone";
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // OrderVoidForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtVoidReason);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderVoidForm";
            this.Load += new System.EventHandler(this.OrderVoidForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtVoidReason.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit txtVoidReason;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnDone;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
    }
}