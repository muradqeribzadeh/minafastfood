﻿using System;
using System.Data;
using System.Data.SqlClient;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using System.Resources;
using DevExpress.XtraGrid.Views.Grid;
using System.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
 

namespace MinAFastFoodClient
{
    public partial class FoodsQuantity : DevExpress.XtraEditors.XtraForm
    {
        ResourceManager LocRM;
        private int language;

        public FoodsQuantity(int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();

            this.language = language;
            LocRM = new ResourceManager("MinAFastFoodClient.FastFoodLanguage", typeof(FoodsQuantity).Assembly); 
        }

        protected DataSet PDataset(string select_statement)
        {
            SqlConnection conn = new SqlConnection(Utility.DBHelper.ConnectionString);
            SqlCommand cmd = new SqlCommand(select_statement, conn);
            conn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            ad.Fill(ds);

            conn.Close();
            return ds;
        }

        private void FoodsQuantity_Load(object sender, EventArgs e)
        {
            DataSet pDataSet = PDataset("Select Oid as Oid,ItemName,QtyOnHand from [Item] where IsStockItem=1 and Visible=1");
            gridControl1.DataSource = pDataSet.Tables[0];
            //  for (int i = 0; i < gridView1.Columns.Count; i++)
            //{
            gridView1.Columns[0].Visible = false;

            gridView1.Columns[1].OptionsColumn.ReadOnly = false;
            gridView1.Columns[1].OptionsColumn.AllowEdit = false;
            gridView1.Columns[2].OptionsColumn.ReadOnly = false;
            gridView1.Columns[2].OptionsColumn.AllowEdit = false;

        }

        private void dataGridItems_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 2) // 1 should be your column index
            {
                int i;

                if (!int.TryParse(Convert.ToString(e.FormattedValue), out i))
                {
                    e.Cancel = true;
                    XtraMessageBox.Show(language == 0 ? LocRM.GetString("AzEnterNumericNumber") : LocRM.GetString("EngEnterNumericNumber"));
                }
                else
                {
                    // the input is numeric 
                }
            }
        }

        private void btnDone_Click(object sender, EventArgs e)
        {

            //foreach (DataGridViewRow r in dataGridItems.Rows) { 
            //
            // updateFood(r.Cells[0].Value.ToString(),Int32.Parse(r.Cells[2].Value.ToString()));
            //}

            //XtraMessageBox.Show(language == 0 ? "Məlumat saxlanıldı!" : "Data saved!");
        }

        private void updateFood(string oid, int qty) 
        {

            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update [Item] Set [QtyOnHand]="+qty+" where  [Oid]=N'" + oid + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                 
                }
            }
            catch (SystemException ex)
            {
                XtraMessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridItems_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.ColumnIndex == 2) {

            //    NumbersKeyboard n = new NumbersKeyboard("s");
            //    n.ShowDialog();
            //    if (!NumbersKeyboard.password.Equals("a"))
            //    {
            //        dataGridItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = NumbersKeyboard.a;
            //    }
            //}
        }

        private void dataGridItems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;

            Point pt = view.GridControl.PointToClient(Control.MousePosition);

            DoRowDoubleClick(view, pt);
        }
        private void DoRowDoubleClick(GridView view, Point pt)
        {

            GridHitInfo info = view.CalcHitInfo(pt);

            if ((info.InRow || info.InRowCell) && info.Column.Name.Equals("colQtyOnHand"))
            {
                
                NumbersKeyboard n = new NumbersKeyboard("s");
                n.ShowDialog();
                if (!NumbersKeyboard.password.Equals("a"))
                {
                    if (XtraMessageBox.Show("Dəyişikliyi bazada saxlamaq istəyirsiz?", "Yemək miqdarının dəyişdirilməsi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        view.GetDataRow(info.RowHandle)[2] = NumbersKeyboard.a.ToString();
                        updateFood(view.GetDataRow(info.RowHandle)[0].ToString(), Int32.Parse(NumbersKeyboard.a.ToString()));
                    }

                }

            }
        }

      
       
    }
}