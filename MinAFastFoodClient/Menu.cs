﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Transactions;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using MinAFastFoodClient.Classes;

namespace MinAFastFoodClient
{
    public partial class Menu : DevExpress.XtraEditors.XtraForm
    {
        private User user;
        private String orderOid;
        private bool isNew;
        public static bool shorOrderType;
        private int language;
        ResourceManager LocRM = new ResourceManager("MinAFastFoodClient.FastFoodLanguage", typeof(MinAFastFoodClient.Menu).Assembly);

        public Menu(User user, int newOrder, int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.language = language;
            fillMenus();
            //orderItems = new List<OrderItems>();
            btnDone.Enabled = false;
            btnDelete.Enabled = false;
            btnCancelDiscount.Enabled = false;
            btnDiscount.Enabled = false;
            btnTable.Enabled = false;
            btnNotes.Enabled = false;
            this.user = new User();
            this.user = user;
            discountViewLabel = new LabelControl();
            discountViewLabel = lblDiscount;
            orderOid = "";
            tableNameLableControl = lblTableName;
            tableId = "";
            btnNewOrder_Click(null, null);
            isNew = true;
        }

        public Menu(User user, int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.language = language;
            fillMenus();
            //orderItems = new List<OrderItems>();
            btnDone.Enabled = false;
            btnDelete.Enabled = false;
            btnCancelDiscount.Enabled = false;
            btnDiscount.Enabled = false;
            btnTable.Enabled = false;
            btnNotes.Enabled = false;
            this.user = new User();
            this.user = user;
            tableNameLableControl = lblTableName;
            discountViewLabel = new LabelControl();
            discountViewLabel = lblDiscount;
            orderOid = "";
            isNew = false;
        }

        public Menu(User user, string orderId, int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.language = language;
            fillMenus();
            //orderItems = new List<OrderItems>();
            btnDone.Enabled = true;
            btnDelete.Enabled = true;
            btnCancelDiscount.Enabled = true;
            btnDiscount.Enabled = true;
            btnTable.Enabled = true;
            btnNotes.Enabled = true;
            this.user = new User();
            this.user = user;
            discountViewLabel = new LabelControl();
            discountViewLabel = lblDiscount;
            btnNewOrder.Enabled = false;
            orderItems = new List<OrderItems>();
            itemOptions = new List<ItemOptions>();
            orderDiscount = new OrderDiscounts();
            orderDeletedItems = new List<OrderItems>();
            fillOrder(orderId);
            tableNameLableControl = lblTableName;
            orderOid = orderId;
            isNew = true;
        }

        public static string notes;
        public static LabelControl discountViewLabel;
        int XIndent = 0;
        int YIndent = 0;
        int count = 0;

        public static List<OrderItems> orderItems;
        private List<ItemOptions> itemOptions;
        public static SimpleButton cancelButton;
        public static OrderDiscounts orderDiscount;
        public static string tableId;
        public static LabelControl tableNameLableControl;
        public List<OrderItems> orderDeletedItems;

        protected DataSet PDataset(string select_statement)
        {
            SqlConnection conn = new SqlConnection(Utility.DBHelper.ConnectionString);
            SqlCommand cmd = new SqlCommand(select_statement, conn);
            conn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            ad.Fill(ds);

            conn.Close();
            return ds;
        }

        public void fillOrder(string orderId)
        {
            DataSet orderDataSet = PDataset("SELECT   [Oid],[OrderDate],[DiscountAmount],[DiscountId],[Discount],[DiscountType],[Amount],[TotalAmount],[TicketNumber],[Notes],[TableId],Isnull((Select TableName from Tables where Oid=TableId),'') as TableName  FROM  [Orders] where [Oid]=N'" + orderId + "'");
            DataTable orderTable = orderDataSet.Tables[0];
            foreach (DataRow r in orderTable.Rows)
            {
                lblDate.Text = (this.language == 0 ? LocRM.GetString("AzDate") : LocRM.GetString("EngDate")) + " : " + r[1].ToString();
                if (string.IsNullOrEmpty(r[2].ToString()))
                    txtEndirim.Text = "";
                else
                    txtEndirim.Text = r[2].ToString();
                txtUmMebleg.Text = r[6].ToString();
                txtYekun.Text = r[7].ToString();
                lblTicket.Text = (this.language == 0 ? LocRM.GetString("AzTicket") : LocRM.GetString("EngTicket")) + r[8].ToString();
                notes = r[9].ToString();
                if (!string.IsNullOrEmpty(r[10].ToString()))
                {
                    btnTable.Visible = true;
                    lblTableName.Text = (this.language == 0 ? LocRM.GetString("AzTable") : LocRM.GetString("EngTable")) + r[11].ToString();
                    tableId = r[10].ToString();
                }

                if (string.IsNullOrEmpty(r[3].ToString()))
                {
                    orderDiscount.DiscId = "0";
                }
                else
                {
                    orderDiscount.DiscId = r[3].ToString();
                    orderDiscount.DiscType = Int32.Parse(r[5].ToString());

                    orderDiscount.DiscAmount = decimal.Parse(r[4].ToString());

                    if (orderDiscount.DiscType == 0)
                        lblDiscount.Text = Math.Round(Decimal.Parse(r[4].ToString()), 2).ToString() + "%";
                    else
                        lblDiscount.Text = Math.Round(Decimal.Parse(r[4].ToString()), 2).ToString();
                }
            }

            DataSet orderItemsDataSet = PDataset("Select [Oid],[ItemId],[Quantity],[DiscountId],[DiscountAmount],[ItemPrice],[ItemName],[OptionsDescription],[ItemNumber],[AnItemsPrice],[OptionsPrice] from [OrderDetail] where [OrderId]=N'" + orderId + "'");
            DataTable orderItemsTable = orderItemsDataSet.Tables[0];

            foreach (DataRow r in orderItemsTable.Rows)
            {
                OrderItems o = new OrderItems();
                o.ItemId = r[1].ToString();
                o.ItemName = r[6].ToString();
                o.ItemPrice = Decimal.Parse(r[5].ToString());
                o.Quantity = decimal.Parse(r[2].ToString());
                o.OptionsPrice = decimal.Parse(r[10].ToString()); //?????
                o.ItemNumber = Int32.Parse(r[8].ToString());
                o.Oneprice = Decimal.Parse(r[9].ToString());
                if (string.IsNullOrEmpty(r[3].ToString()))
                {
                    o.DiscountId = "0";
                }
                else
                {
                    o.DiscountId = r[3].ToString();
                    o.DiscountAmount = decimal.Parse(r[4].ToString());
                }
                o.OptionsDescription = r[7].ToString();
                orderItems.Add(o);
            }
            foreach (OrderItems item in orderItems)
            {
                orderDeletedItems.Add(item);
            }

            DataSet orderOptionsDataset = PDataset("Select [ItemId],[OptionId],[OptionsCost],[ItemNumber] from [OrderOptions] where [OrderId]=N'" + orderId + "'");
            DataTable orderOptionsDataTable = orderOptionsDataset.Tables[0];
            foreach (DataRow r in orderOptionsDataTable.Rows)
            {
                ItemOptions io = new ItemOptions();
                io.ItemId = r[0].ToString();
                io.OptionId = r[1].ToString();
                io.OptionsCost = Decimal.Parse(r[2].ToString());
                io.ItemNumber = Int32.Parse(r[3].ToString());
                itemOptions.Add(io);
            }
            bindDataRepeator();
        }

        private void fillMenus()
        {
            DataSet menuDataSet = PDataset("Select Oid,GroupName,Picture from [MinAFastFood].[dbo].[Group] where visible=1 order by displayorder");
            DataTable menuTable = menuDataSet.Tables[0];

            foreach (DataRow r in menuTable.Rows)
            {
                SimpleButton menuButton = new SimpleButton();
                menuButton.Name = "group" + r[0].ToString();
                menuButton.Width = 98;
                menuButton.ImageLocation = ImageLocation.TopCenter;
                menuButton.Height = 90;
                menuButton.Text = r[1].ToString();

                menuButton.Left = XIndent;
                menuButton.Top = YIndent;
                byte[] barrImg = (byte[])r[2];
                MemoryStream mstream = new MemoryStream(barrImg);
                //  Image img =ScaleImage(Image.FromStream(mstream),32,32);
                Bitmap b = new Bitmap(Image.FromStream(mstream));
                b = ResizeBitmap(b, 45, 45);

                menuButton.Image = (Image)b;
                menuControl.Controls.Add(menuButton);
                XIndent += menuButton.Width + 5;

                menuButton.Click += new EventHandler(menuButton_Click);
                count++;

                if (count % 6 == 0)
                {
                    XIndent = 0;
                    YIndent += menuButton.Height + 10;
                }
            }
        }

        public Bitmap ResizeBitmap(Bitmap b, int nWidth, int nHeight)
        {
            Bitmap result = new Bitmap(nWidth, nHeight);
            using (Graphics g = Graphics.FromImage((Image)result))
                g.DrawImage(b, 0, 0, nWidth, nHeight);
            return result;
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            SimpleButton btn = sender as SimpleButton;
            XIndent = 0;
            YIndent = 0;
            count = 0;

            //  XtraMessageBox.Show(btn.Name);
            //  btn.Name.Substring(5);
            fillItems(btn.Name.Substring(5).Trim());
            //  itemId = btn.Name.Substring(5).Trim();
        }

        private void clearItemAndOptionsTable(string orderId)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("DELETE FROM [OrderDetail] WHERE  [OrderId] = N'" + orderId + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                    con.Open();
                    using (SqlCommand command = new SqlCommand("DELETE FROM [OrderOptions] WHERE  [OrderId] = N'" + orderId + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        public void fillItems(string groupId)
        {
            DataSet itemDataSet = PDataset("SELECT  Oid,ItemName,Picture,DefaultItemPrice,IsStockItem FROM [MinAFastFood].[dbo].[Item] where [MinAFastFood].[dbo].[Item].[Group]='" + groupId + "' and visible=1 order by displayindex ");
            DataTable itemTable = itemDataSet.Tables[0];
            itemControl.Controls.Clear();
            optionControl.Controls.Clear();
            foreach (DataRow r in itemTable.Rows)
            {
                SimpleButton itemButton = new SimpleButton();
                if (Boolean.Parse(r[4].ToString()))
                    itemButton.Name = "itet" + r[0].ToString();
                else
                    itemButton.Name = "itef" + r[0].ToString();
                itemButton.Width = 98;
                itemButton.ImageLocation = ImageLocation.TopCenter;
                itemButton.Height = 90;
                itemButton.Text = r[1].ToString();

                itemButton.Left = XIndent;
                itemButton.Top = YIndent;
                byte[] barrImg = (byte[])r[2];
                MemoryStream mstream = new MemoryStream(barrImg);
                //  Image img =ScaleImage(Image.FromStream(mstream),32,32);
                Bitmap b = new Bitmap(Image.FromStream(mstream));
                b = ResizeBitmap(b, 45, 45);

                itemButton.Image = (Image)b;
                itemControl.Controls.Add(itemButton);
                XIndent += itemButton.Width + 5;
                itemButton.Tag = r[3].ToString();
                itemButton.Click += new EventHandler(itemButton_Click);
                count++;

                if (count % 6 == 0)
                {
                    XIndent = 0;
                    YIndent += itemButton.Height + 10;
                }
            }
        }

        public void fillOptions(string itemId)
        {
            DataSet optionDataSet = PDataset("SELECT  Oid,ModifierName,ModifierPrice FROM [MinAFastFood].[dbo].[Modifiers] where [MinAFastFood].[dbo].[Modifiers].[Item]='" + itemId + "' and visible=1");
            DataTable optionTable = optionDataSet.Tables[0];
            optionControl.Controls.Clear();
            foreach (DataRow r in optionTable.Rows)
            {
                SimpleButton optionButton = new SimpleButton();
                optionButton.Name = "option" + r[0].ToString();
                optionButton.Width = 112;
                optionButton.ImageLocation = ImageLocation.TopCenter;
                optionButton.Height = 70;
                optionButton.Text = r[1].ToString();

                optionButton.Left = XIndent;
                optionButton.Top = YIndent;
                optionButton.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                optionButton.Tag = r[2].ToString();

                optionControl.Controls.Add(optionButton);
                XIndent += optionButton.Width + 5;

                optionButton.Click += new EventHandler(optionButtonClick);
                count++;

                if (count % 6 == 0)
                {
                    XIndent = 0;
                    YIndent += optionButton.Height + 10;
                }
            }
        }

        private void itemButton_Click(object sender, EventArgs e)
        {
            SimpleButton btnItem = sender as SimpleButton;
            XIndent = 0;
            YIndent = 0;
            count = 0;
            //  XtraMessageBox.Show(btn.Name);

            //  fillItems(btn.Name.Trim());

            if (string.IsNullOrEmpty(orderOid))
            {
                if (isStockItem(btnItem.Name.Substring(4).Trim()))
                {
                    int itemsqty = getItemQuantity(btnItem.Name.Substring(4).Trim());

                    int currentqty = getCurrentItemCount(btnItem.Name.Substring(4).Trim()) + 1;
                    if (itemsqty >= currentqty)
                    {
                         if(dataRepeater1.ItemCount==0)
                    fillOptions(btnItem.Name.Substring(4).Trim());
                        if (isNew)
                        {
                            OrderItems o = new OrderItems();
                            o.ItemId = btnItem.Name.Substring(4).Trim();
                            o.ItemName = btnItem.Text.Trim();
                            o.ItemPrice = Math.Round(Decimal.Parse(btnItem.Tag.ToString()), 2);
                            o.Quantity = 1;
                            o.OptionsPrice = 0.00m;
                            o.ItemNumber = getNextItemNumber();
                            o.Oneprice = Math.Round(Decimal.Parse(btnItem.Tag.ToString()), 2);
                            o.DiscountId = "0";
                            o.OptionsDescription = "";
                            orderItems.Add(o);
                            bindDataRepeator();
                            btnDone.Enabled = true;
                            txtOdeme.Enabled = false;
                            btnOde.Enabled = false;
                            lblQaliq.Text = "";
                            txtOdeme.Text = "";
                            //    txtUmMebleg.Text = sumOfOrder().ToString();
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzNotEnFood") : LocRM.GetString("EngNotEnFood"));
                    }
                }
                else
                {
                    if (dataRepeater1.ItemCount == 0)
                  fillOptions(btnItem.Name.Substring(4).Trim());
                    if (isNew)
                    {
                        OrderItems o = new OrderItems();
                        o.ItemId = btnItem.Name.Substring(4).Trim();
                        o.ItemName = btnItem.Text.Trim();
                        o.ItemPrice = Math.Round(Decimal.Parse(btnItem.Tag.ToString()), 2);
                        o.Quantity = 1;
                        o.OptionsPrice = 0.00m;
                        o.ItemNumber = getNextItemNumber();
                        o.Oneprice = Math.Round(Decimal.Parse(btnItem.Tag.ToString()), 2);
                        o.DiscountId = "0";
                        o.OptionsDescription = "";
                        orderItems.Add(o);
                        bindDataRepeator();
                        btnDone.Enabled = true;
                        txtOdeme.Enabled = false;
                        btnOde.Enabled = false;
                        lblQaliq.Text = "";
                        txtOdeme.Text = "";
                        //    txtUmMebleg.Text = sumOfOrder().ToString();
                    }
                }
            }
            else
            {
                if (isStockItem(btnItem.Name.Substring(4).Trim()))
                {
                    int currentqty = getCurrentItemCount(btnItem.Name.Substring(4).Trim());
                    int deletedCount = getCurrentOldItemCount(orderDeletedItems, btnItem.Name.Substring(4).Trim());
                    int itemsqty;
                    if (deletedCount == 0)
                        itemsqty = getItemQuantity(btnItem.Name.Substring(4).Trim());
                    else
                        itemsqty = getItemQuantity(btnItem.Name.Substring(4).Trim()) + deletedCount;
                    if (itemsqty > currentqty)
                    {
                        if (dataRepeater1.ItemCount == 0)
                     fillOptions(btnItem.Name.Substring(4).Trim());
                        if (isNew)
                        {
                            OrderItems o = new OrderItems();
                            o.ItemId = btnItem.Name.Substring(4).Trim();
                            o.ItemName = btnItem.Text.Trim();
                            o.ItemPrice = Math.Round(Decimal.Parse(btnItem.Tag.ToString()), 2);
                            o.Quantity = 1;
                            o.OptionsPrice = 0.00m;
                            o.ItemNumber = getNextItemNumber();
                            o.Oneprice = Math.Round(Decimal.Parse(btnItem.Tag.ToString()), 2);
                            o.DiscountId = "0";
                            o.OptionsDescription = "";
                            orderItems.Add(o);
                            bindDataRepeator();
                            btnDone.Enabled = true;
                            txtOdeme.Enabled = false;
                            btnOde.Enabled = false;
                            lblQaliq.Text = "";
                            txtOdeme.Text = "";
                            //    txtUmMebleg.Text = sumOfOrder().ToString();
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzNotEnFood") : LocRM.GetString("EngNotEnFood"));
                    }
                }
                else
                {
                    if (dataRepeater1.ItemCount == 0)
                    fillOptions(btnItem.Name.Substring(4).Trim());
                    if (isNew)
                    {
                        OrderItems o = new OrderItems();
                        o.ItemId = btnItem.Name.Substring(4).Trim();
                        o.ItemName = btnItem.Text.Trim();
                        o.ItemPrice = Math.Round(Decimal.Parse(btnItem.Tag.ToString()), 2);
                        o.Quantity = 1;
                        o.OptionsPrice = 0.00m;
                        o.ItemNumber = getNextItemNumber();
                        o.Oneprice = Math.Round(Decimal.Parse(btnItem.Tag.ToString()), 2);
                        o.DiscountId = "0";
                        o.OptionsDescription = "";
                        orderItems.Add(o);
                        bindDataRepeator();
                        btnDone.Enabled = true;
                        txtOdeme.Enabled = false;
                        btnOde.Enabled = false;
                        lblQaliq.Text = "";
                        txtOdeme.Text = "";
                        //    txtUmMebleg.Text = sumOfOrder().ToString();
                    }
                }
            }
        }

        private int getNextItemNumber()
        {
            try
            {
                return orderItems.Max(w => w.ItemNumber) + 1;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }

        private int getItemQuantity(string oid)
        {
            DataSet ds = PDataset("Select QtyOnHand from Item where Oid=N'" + oid + "' and  Visible=1");
            try
            {
                return Int32.Parse(ds.Tables[0].Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private int getCurrentItemCount(string oid)
        {
            decimal a = orderItems.Where(w => w.ItemId.Equals(oid)).Sum(w => w.Quantity);
            return (int)a;
        }

        private int getCurrentOldItemCount(List<OrderItems> list, string oid)
        {
            int a = 0;

            foreach (OrderItems o in list)
            {
                if (o.ItemId.Equals(oid))
                    a++;
            }

            return a;
        }

        private int getCurrentItemCountWithoutCur(string oid, int itemNumber)
        {
            decimal a = orderItems.Where(w => w.ItemId.Equals(oid) && w.ItemNumber != itemNumber).Sum(w => w.Quantity);
            return (int)a;
        }

        private decimal sumOfOrder()
        {
            decimal sumItemPrice = orderItems.Sum(w => w.Oneprice * w.Quantity);
            decimal sumOption = orderItems.Sum(w => w.OptionsPrice);
            return sumItemPrice + sumOption;
        }

        private decimal sumOfOrderForYekun()
        {
            decimal sumItemPrice = orderItems.Sum(w => w.ItemPrice);
            decimal sumOption = orderItems.Sum(w => w.OptionsPrice);
            return sumItemPrice + sumOption;
        }

        private decimal sumOfOrderWithDiscount()
        {
            decimal sumItemPrice = orderItems.Where(w => w.DiscountId != "0").Sum(w => w.ItemPrice);
            decimal sumOption = orderItems.Where(w => w.DiscountId != "0").Sum(w => w.OptionsPrice);
            return sumItemPrice + sumOption;
        }

        private void deleteOptionsByItemId(string itemId)
        {
            itemOptions.RemoveAll(x => x.ItemId.Equals(itemId));
            // List<ItemOptions> s = itemOptions.Where(x => x.ItemId.Equals(itemId)).ToList();//(from w in itemOptions where w.ItemId.Equals(itemId) select w)<ItemOptions>();
        }

        private void deleteItemByIdFromDb(string oid, int qty)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update [Item] Set [QtyOnHand]=[QtyOnHand]+" + qty + " where  [Oid]=N'" + oid + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                XtraMessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        private decimal sumOfOrderWithoutDiscount()
        {
            decimal sumItemPrice = orderItems.Where(w => w.DiscountId == "0").Sum(w => w.ItemPrice);
            decimal sumOption = orderItems.Where(w => w.DiscountId == "0").Sum(w => w.OptionsPrice);
            return sumItemPrice + sumOption;
        }

        private void optionButtonClick(object sender, EventArgs e)
        {
            if (isNew)
            {
                SimpleButton btnOption = sender as SimpleButton;

                LabelControl itemId = new LabelControl();
                int drIndex = dataRepeater1.CurrentItemIndex;
                itemId = (LabelControl)dataRepeater1.CurrentItem.Controls["lblItemId"];
                string i = itemId.Text;
                LabelControl itemNumber = new LabelControl();
                itemNumber = (LabelControl)dataRepeater1.CurrentItem.Controls["lblItemNumber"];
                int index = getOrderItemById(int.Parse(itemNumber.Text));

                orderItems[index].OptionsDescription = orderItems[index].OptionsDescription + btnOption.Text + " , ";
                orderItems[index].OptionsPrice = orderItems[index].OptionsPrice + Math.Round(Decimal.Parse(btnOption.Tag.ToString()), 2);
                bindDataRepeator();
                //    txtUmMebleg.Text = sumOfOrder().ToString();
                ItemOptions io = new ItemOptions();
                io.ItemId = i.Trim();
                io.OptionId = btnOption.Name.Substring(6).Trim();
                io.OptionsCost = Math.Round(Decimal.Parse(btnOption.Tag.ToString()), 2);
                io.ItemNumber = orderItems[index].ItemNumber;
                itemOptions.Add(io);
                btnDone.Enabled = true;
                txtOdeme.Enabled = false;
                btnOde.Enabled = false;
                lblQaliq.Text = "";
                txtOdeme.Text = "";
                dataRepeater1.CurrentItemIndex = drIndex;
            }
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            cancelButton = btnCancelDiscount;
            btnCancelDiscount.Visible = false;
            txtOdeme.Enabled = false;
            btnOde.Enabled = false;
            lblQaliq.Text = "";
            tableNameLableControl = lblTableName;
        }

        private int getOrderItemById(int id)
        {
            int k = orderItems.FindIndex(x => x.ItemNumber.Equals(id));
            return k;
        }

        private void txtItemAmount_Click(object sender, EventArgs e)
        { 
            NumbersKeyboard numbersKeyboard = new NumbersKeyboard();
            numbersKeyboard.ShowDialog();

            if (NumbersKeyboard.a>0)
            {
                decimal value = NumbersKeyboard.a;
                LabelControl itemNumber = new LabelControl();
                itemNumber = (LabelControl)dataRepeater1.CurrentItem.Controls["lblItemNumber"];
                int index = getOrderItemById(int.Parse(itemNumber.Text));
                int drIndex = dataRepeater1.CurrentItemIndex;

                if (string.IsNullOrEmpty(orderOid))
                {
                    if (isStockItem(orderItems[index].ItemId))
                    {
                        int c;
                        if (int.TryParse(value.ToString(), out c))
                        {
                            int countqtycur = getCurrentItemCountWithoutCur(orderItems[index].ItemId, orderItems[index].ItemNumber) + c;
                            int getqtydb = getItemQuantity(orderItems[index].ItemId);

                            if (getqtydb >= countqtycur)
                            {
                                orderItems[index].ItemPrice = Math.Round((orderItems[index].ItemPrice * c) / orderItems[index].Quantity, 2);
                                orderItems[index].OptionsPrice = Math.Round((orderItems[index].OptionsPrice * c) / orderItems[index].Quantity, 2);
                                orderItems[index].Quantity = c;

                                //        orderItems[index].OptionsDescription = orderItems[index].OptionsDescription + orderItems[index].OptionsDescription;
                                itemOptions.AddRange(itemOptions.FindAll(x => x.ItemId.Equals(orderItems[index].ItemId)));

                                bindDataRepeator();
                            }
                            else
                            {
                                XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzNotEnFood") : LocRM.GetString("EngNotEnFood"));
                            }
                        }
                        else
                        {
                            XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzEnterNumericNumber") : LocRM.GetString("EngEnterNumericNumber"));
                        }
                    }
                    else
                    {
                        orderItems[index].ItemPrice = Math.Round((orderItems[index].ItemPrice * value) / orderItems[index].Quantity, 2);
                        orderItems[index].OptionsPrice = Math.Round((orderItems[index].OptionsPrice * value) / orderItems[index].Quantity, 2);
                        orderItems[index].Quantity = value;

                        //        orderItems[index].OptionsDescription = orderItems[index].OptionsDescription + orderItems[index].OptionsDescription;
                        itemOptions.AddRange(itemOptions.FindAll(x => x.ItemId.Equals(orderItems[index].ItemId)));

                        bindDataRepeator();
                    }
                }
                else
                {
                    if (isStockItem(orderItems[index].ItemId))
                    {
                        int c;
                        if (int.TryParse(value.ToString(), out c))
                        {
                            int deletedCount = getCurrentOldItemCount(orderDeletedItems, orderItems[index].ItemId);
                            int countqtycur = getCurrentItemCountWithoutCur(orderItems[index].ItemId, orderItems[index].ItemNumber) + c;
                            int getqtydb;
                            if (deletedCount == 0)
                                getqtydb = getItemQuantity(orderItems[index].ItemId);
                            else

                                getqtydb = getItemQuantity(orderItems[index].ItemId) + deletedCount;
                            if (getqtydb >= countqtycur)
                            {
                                orderItems[index].ItemPrice = Math.Round((orderItems[index].ItemPrice * c) / orderItems[index].Quantity, 2);
                                orderItems[index].OptionsPrice = Math.Round((orderItems[index].OptionsPrice * c) / orderItems[index].Quantity, 2);
                                orderItems[index].Quantity = c;

                                //        orderItems[index].OptionsDescription = orderItems[index].OptionsDescription + orderItems[index].OptionsDescription;
                                itemOptions.AddRange(itemOptions.FindAll(x => x.ItemId.Equals(orderItems[index].ItemId)));

                                bindDataRepeator();
                            }
                            else
                            {
                                XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzNotEnFood") : LocRM.GetString("EngNotEnFood"));
                            }
                        }
                        else
                        {
                            XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzEnterNumericNumber") : LocRM.GetString("EngEnterNumericNumber"));
                        }
                    }
                    else
                    {
                        orderItems[index].ItemPrice = Math.Round((orderItems[index].ItemPrice * value) / orderItems[index].Quantity, 2);
                        orderItems[index].OptionsPrice = Math.Round((orderItems[index].OptionsPrice * value) / orderItems[index].Quantity, 2);
                        orderItems[index].Quantity = value;

                        //        orderItems[index].OptionsDescription = orderItems[index].OptionsDescription + orderItems[index].OptionsDescription;
                        itemOptions.AddRange(itemOptions.FindAll(x => x.ItemId.Equals(orderItems[index].ItemId)));

                        bindDataRepeator();
                    }
                }

                dataRepeater1.CurrentItemIndex = drIndex;
            }
        }

        //private int oldQtyCount(List<OrderItems> list, string oid, int itemNumber)
        //{
        //    decimal a = list.Where(w => w.ItemId.Equals(oid) && w.ItemNumber != itemNumber).Sum(w => w.Quantity);
        //    return (int)a;
        //}

        private bool isStockItem(string oid)
        {
            DataSet ds = PDataset("Select IsStockItem from Item where Oid=N'" + oid + "' and  Visible=1");

            return Boolean.Parse(ds.Tables[0].Rows[0][0].ToString());
        }

        private void bindDataRepeator()
        {
            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = orderItems;
            txtItemName.DataBindings.Clear();
            txtItemAmount.DataBindings.Clear();
            txtItemPrices.DataBindings.Clear();
            richTextOptions.DataBindings.Clear();
            txtOptionPrice.DataBindings.Clear();
            lblItemId.DataBindings.Clear();
            lblItemNumber.DataBindings.Clear();
            txtOnePrice.DataBindings.Clear();
            txtItemName.DataBindings.Add("Text", bindingSource1, "ItemName");
            txtItemAmount.DataBindings.Add("Text", bindingSource1, "Quantity");
            txtItemPrices.DataBindings.Add("Text", bindingSource1, "ItemPrice");
            richTextOptions.DataBindings.Add("Text", bindingSource1, "OptionsDescription");
            txtOptionPrice.DataBindings.Add("Text", bindingSource1, "OptionsPrice");
            lblItemId.DataBindings.Add("Text", bindingSource1, "ItemId");
            lblItemNumber.DataBindings.Add("Text", bindingSource1, "ItemNumber");
            txtOnePrice.DataBindings.Add("Text", bindingSource1, "Oneprice");
            dataRepeater1.DataSource = bindingSource1;
            if (dataRepeater1.ItemCount > 0)
                dataRepeater1.CurrentItemIndex = dataRepeater1.ItemCount - 1;
            if (!orderDiscount.DiscId.Equals("0"))
            {
                if (orderDiscount.DiscType == 0)
                {
                    txtEndirim.Text = Math.Round(((sumOfOrderWithoutDiscount() * orderDiscount.DiscAmount) / 100), 2).ToString();
                }
                else
                {
                    txtEndirim.Text = Math.Round(orderDiscount.DiscAmount, 2).ToString();
                }
            }
            txtUmMebleg.Text = Math.Round(sumOfOrder(), 2).ToString();

            if (!string.IsNullOrEmpty(txtEndirim.Text))
                txtYekun.Text = Math.Round((sumOfOrderWithDiscount() + (sumOfOrderWithoutDiscount() - Decimal.Parse(txtEndirim.Text))), 2).ToString();
            else
                txtYekun.Text = Math.Round(sumOfOrderForYekun(), 2).ToString();
        }

        private void btnNewOrder_Click(object sender, EventArgs e)
        {
            btnNewOrder.Enabled = false;
            orderItems = new List<OrderItems>();
            itemOptions = new List<ItemOptions>();
            orderDiscount = new OrderDiscounts();
            orderDiscount.DiscId = "0";
            txtYekun.Text = "";
            txtUmMebleg.Text = "";
            txtEndirim.Text = "";
            btnDone.Enabled = true;
            btnDelete.Enabled = true;
            btnCancelDiscount.Enabled = true;
            btnDiscount.Enabled = true;
            btnTable.Enabled = true;
            btnNotes.Enabled = true;
            btnCancelDiscount.Visible = false;
            txtOdeme.Enabled = false;
            btnOde.Enabled = false;
            lblQaliq.Text = "";
            txtOdeme.Text = "";
            notes = "";
            tableId = "";
            lblTableName.Text = "";
            lblDiscount.Text = "";
            isNew = true;
            dataRepeater1.Controls.Clear();
            optionControl.Controls.Clear();
            itemControl.Controls.Clear();

            lblTicket.Text = this.language == 0 ? LocRM.GetString("AzTicket") : LocRM.GetString("EngTicket");

            if (shorOrderType)
            {
                OrderType oType = new OrderType(this.language);
                oType.ShowDialog();

                if (OrderType.orderType == 0)
                {
                    btnTable.Visible = true;

                    OrderTables tables = new OrderTables(this.language);
                    tables.ShowDialog();
                }
            }
            lblDate.Text = String.Concat(this.language == 0 ? LocRM.GetString("AzDate") : LocRM.GetString("EngDate"), " : ", DateTime.Now);
        }

        private void richTextOptions_Leave(object sender, EventArgs e)
        {
            try
            {
                RichTextBox rText = (RichTextBox)sender;
                // rText = (RichTextBox)dataRepeater1.CurrentItem.Controls["richTextOptions"];

                LabelControl itemNumber = new LabelControl();
                itemNumber = (LabelControl)dataRepeater1.CurrentItem.Controls["lblItemNumber"];
                int index = getOrderItemById(int.Parse(itemNumber.Text));
                orderItems[index].OptionsDescription = rText.Text;
                //  orderItems[index].OptionsPrice = orderItems[index].OptionsPrice + Decimal.Parse(btnOption.Tag.ToString());
                bindDataRepeator();
            }
            catch (Exception ex)
            {
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.language == 0 ? LocRM.GetString("AzMsgRefuseChanges") : LocRM.GetString("EngMsgRefuseChanges"), "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                lblDate.Text = "";
                btnNewOrder.Enabled = true;
                orderItems = new List<OrderItems>();
                itemOptions = new List<ItemOptions>();

                btnDone.Enabled = false;
                btnDelete.Enabled = false;
                btnCancelDiscount.Enabled = false;
                btnDiscount.Enabled = false;
                btnTable.Enabled = false;
                btnNotes.Enabled = false;
                dataRepeater1.Controls.Clear();
              //  dataRepeater1 = new Microsoft.VisualBasic.PowerPacks.DataRepeater();
                bindDataRepeator();
                itemControl.Controls.Clear();
                optionControl.Controls.Clear();
                txtUmMebleg.Text = "";
                txtYekun.Text = "";
                txtEndirim.Text = "";
            }
            else
            {
                //   MessageBox.Show("No clicked");
            }
        }

        private void clearOrder()
        {
            lblDate.Text = (this.language == 0 ? LocRM.GetString("AzDate") : LocRM.GetString("EngDate")) + " : " + DateTime.Now;
            orderItems = new List<OrderItems>();
            itemOptions = new List<ItemOptions>();
            orderDiscount = new OrderDiscounts();
            btnDone.Enabled = true;
            btnDelete.Enabled = true;
            btnCancelDiscount.Enabled = true;
            btnDiscount.Enabled = true;
            btnTable.Enabled = false;
            btnNotes.Enabled = false;
            dataRepeater1.Controls.Clear();
            itemControl.Controls.Clear();
            optionControl.Controls.Clear();
            txtUmMebleg.Text = "";
            txtYekun.Text = "";
            txtEndirim.Text = "";
            orderDiscount.DiscId = "0";
            txtOdeme.Text = "";

            lblQaliq.Text = "";
            isNew = true;

            lblTicket.Text = this.language == 0 ? LocRM.GetString("AzTicket") : LocRM.GetString("EngTicket");

            btnTable.Enabled = true;
            btnNotes.Enabled = true;
            btnCancelDiscount.Visible = false;
            txtOdeme.Enabled = false;
            btnOde.Enabled = false;
            lblQaliq.Text = "";
            notes = "";
            lblDiscount.Text = "";
        }

        private void btnNotes_Click(object sender, EventArgs e)
        {
            int pId;
            pId = System.Diagnostics.Process.Start("osk.exe").Id;

            Notes note = new Notes(this.language);
            note.ShowDialog();
            // System.Diagnostics.Process.GetProcessById(pId).Kill();
        }

        private void btnDiscount_Click(object sender, EventArgs e)
        {
            DiscountChoise discChoise = new DiscountChoise(this.language);
            discChoise.ShowDialog();
            bindDataRepeator();
            btnDone.Enabled = true;
            txtOdeme.Enabled = false;
            btnOde.Enabled = false;
            lblQaliq.Text = "";
            txtOdeme.Text = "";
        }

        private void btnCancelDiscount_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.language == 0 ? LocRM.GetString("AzMsgCancelDisc") : LocRM.GetString("EngMsgCancelDisc"), "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                txtEndirim.Text = "";
                orderDiscount = new OrderDiscounts();
                orderDiscount.DiscId = "0";
                btnCancelDiscount.Visible = false;
                lblDiscount.Text = "";
                bindDataRepeator();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dataRepeater1.Controls.Count > 0)
            {
                if (MessageBox.Show(this.language == 0 ? LocRM.GetString("AzMsgDeleteSelectedItem") : LocRM.GetString("EngMsgDeleteSelectedItem"), "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    LabelControl itemNumber = new LabelControl();
                    itemNumber = (LabelControl)dataRepeater1.CurrentItem.Controls["lblItemNumber"];
                    int index = getOrderItemById(int.Parse(itemNumber.Text));
                    deleteOptionsByItemId(orderItems[index].ItemId);
                    //    if (!string.IsNullOrEmpty(orderOid))
                    //      orderDeletedItems.Add(orderItems[index]);
                    orderItems.RemoveAt(index);

                    bindDataRepeator();
                    btnDone.Enabled = true;
                    txtOdeme.Enabled = false;
                    btnOde.Enabled = false;
                    lblQaliq.Text = "";
                    txtOdeme.Text = "";
                }
            }
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                if (dataRepeater1.Controls.Count > 0)
                {
                    insertOrderAndDetails();

                    btnDone.Enabled = false;
                    if (this.user.CanTakeCash)
                    {
                        txtOdeme.Enabled = true;
                        btnOde.Enabled = true;
                    }
                    else
                    {
                        txtOdeme.Enabled = false;
                        btnOde.Enabled = false;
                        print(orderOid);
                        btnNewOrder_Click(null, null);
                    }
                    btnNewOrder.Enabled = true;
                    orderDeletedItems = new List<OrderItems>();
                    btnTable.Visible = false;
                }

                else
                {
                    XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzShouldAddItem") : LocRM.GetString("EndShouldAddItem"));
                }
                scope.Complete();
            }
        }

        private void insertOrderAndDetails()
        {
            if (hasError())
            {
                XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzNotEnFood") : LocRM.GetString("EngNotEnFood"));
            }
            else
            {
                SqlConnection conn = new SqlConnection(Utility.DBHelper.ConnectionString);

                SqlCommand cmd = new SqlCommand("dbo.insertOrUpdateOrder", conn);

                SqlCommand cmdOrderItems = new SqlCommand("dbo.insertOrderDetails", conn);
                SqlCommand cmdOrderOptions = new SqlCommand("dbo.insertOrderOptions", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmdOrderItems.CommandType = CommandType.StoredProcedure;

                cmdOrderOptions.CommandType = CommandType.StoredProcedure;
                string orderId1 = "";
                int ticketNumber = 0;
                if (string.IsNullOrEmpty(orderOid))
                {
                    cmd.Parameters.Add(new SqlParameter("@OrderDate", DateTime.Parse(lblDate.Text.Substring(7))));

                    if (this.user.UserEmployee.Equals("0"))
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", System.Data.SqlTypes.SqlGuid.Null));
                    else
                        cmd.Parameters.Add(new SqlParameter("@EmployeeId", this.user.UserEmployee));

                    if (!orderDiscount.DiscId.Equals("0"))
                        cmd.Parameters.Add(new SqlParameter("@DiscountId", orderDiscount.DiscId));
                    else
                        cmd.Parameters.Add(new SqlParameter("@DiscountId", System.Data.SqlTypes.SqlGuid.Null));

                    if (!orderDiscount.DiscId.Equals("0"))
                        cmd.Parameters.Add(new SqlParameter("@DiscountAmount", decimal.Parse(txtEndirim.Text)));
                    else
                        cmd.Parameters.Add(new SqlParameter("@DiscountAmount", System.Data.SqlTypes.SqlMoney.Null));

                    if (!orderDiscount.DiscId.Equals("0"))

                        cmd.Parameters.Add(new SqlParameter("@DiscountType", orderDiscount.DiscType));
                    else
                        cmd.Parameters.Add(new SqlParameter("@DiscountType", System.Data.SqlTypes.SqlInt32.Null));

                    if (!orderDiscount.DiscId.Equals("0"))
                        cmd.Parameters.Add(new SqlParameter("@Discount", orderDiscount.DiscAmount));
                    else
                        cmd.Parameters.Add(new SqlParameter("@Discount", System.Data.SqlTypes.SqlMoney.Null));

                    cmd.Parameters.Add(new SqlParameter("@Amount", Decimal.Parse(txtUmMebleg.Text)));
                    cmd.Parameters.Add(new SqlParameter("@TotalAmount", Decimal.Parse(txtYekun.Text)));
                    cmd.Parameters.Add("@Notes", notes);

                    cmd.Parameters.Add(new SqlParameter("@OrderStatus", int.Parse("0")));
                    cmd.Parameters.Add(new SqlParameter("@Printed", false));
                    if (string.IsNullOrEmpty(tableId))
                    {
                        cmd.Parameters.Add(new SqlParameter("@TableId", System.Data.SqlTypes.SqlGuid.Null));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@TableId", tableId));
                    }

                    //@OldOid
                    cmd.Parameters.Add(new SqlParameter("@OldOid", System.Data.SqlTypes.SqlGuid.Null));
                    //@IoU
                    cmd.Parameters.Add(new SqlParameter("@IoU", int.Parse("0")));
                    cmd.Parameters.Add("@Oid", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@TicketNumber", SqlDbType.Int).Direction = ParameterDirection.Output;
                    // set parameter values
                    //     cmd.Parameters["@ContractNumber"].Value = contractNumber;

                    // open connection and execute stored procedure

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    orderId1 = cmd.Parameters["@Oid"].Value.ToString();
                    ticketNumber = Convert.ToInt32(cmd.Parameters["@TicketNumber"].Value);
                    conn.Close();
                }
                else
                {
                    orderId1 = orderOid;
                    cmd.Parameters.Add(new SqlParameter("@OrderDate", DateTime.Now));

                    cmd.Parameters.Add(new SqlParameter("@EmployeeId", System.Data.SqlTypes.SqlGuid.Null));

                    if (!orderDiscount.DiscId.Equals("0"))
                        cmd.Parameters.Add(new SqlParameter("@DiscountId", orderDiscount.DiscId));
                    else
                        cmd.Parameters.Add(new SqlParameter("@DiscountId", System.Data.SqlTypes.SqlGuid.Null));

                    if (!orderDiscount.DiscId.Equals("0"))
                        cmd.Parameters.Add(new SqlParameter("@DiscountAmount", decimal.Parse(txtEndirim.Text)));
                    else
                        cmd.Parameters.Add(new SqlParameter("@DiscountAmount", System.Data.SqlTypes.SqlMoney.Null));

                    if (!orderDiscount.DiscId.Equals("0"))

                        cmd.Parameters.Add(new SqlParameter("@DiscountType", orderDiscount.DiscType));
                    else
                        cmd.Parameters.Add(new SqlParameter("@DiscountType", System.Data.SqlTypes.SqlInt32.Null));

                    if (!orderDiscount.DiscId.Equals("0"))
                        cmd.Parameters.Add(new SqlParameter("@Discount", orderDiscount.DiscAmount));
                    else
                        cmd.Parameters.Add(new SqlParameter("@Discount", System.Data.SqlTypes.SqlMoney.Null));

                    cmd.Parameters.Add(new SqlParameter("@Amount", Decimal.Parse(txtUmMebleg.Text)));
                    cmd.Parameters.Add(new SqlParameter("@TotalAmount", Decimal.Parse(txtYekun.Text)));
                    cmd.Parameters.Add("@Notes", notes);
                    cmd.Parameters.Add(new SqlParameter("@IoU", int.Parse("1")));
                    //Order Status 0-active,1-printed,2-paid,3-Voided
                    cmd.Parameters.Add(new SqlParameter("@OrderStatus", int.Parse("0")));
                    cmd.Parameters.Add(new SqlParameter("@Printed", false));

                    if (string.IsNullOrEmpty(tableId))
                    {
                        cmd.Parameters.Add(new SqlParameter("@TableId", System.Data.SqlTypes.SqlGuid.Null));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@TableId", tableId));
                    }

                    //@OldOid
                    cmd.Parameters.Add(new SqlParameter("@OldOid", new Guid(orderOid)));

                    cmd.Parameters.Add("@Oid", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@TicketNumber", SqlDbType.Int).Direction = ParameterDirection.Output;
                    // set parameter values
                    //     cmd.Parameters["@ContractNumber"].Value = contractNumber;

                    // open connection and execute stored procedure

                    conn.Open();
                    cmd.ExecuteNonQuery();

                    conn.Close();
                    clearItemAndOptionsTable(orderOid.ToString());
                    restoreDeletedItems();
                }

                Guid orderId = new Guid(orderId1);
                orderOid = orderId.ToString();

                foreach (OrderItems oi in orderItems)
                {
                    cmdOrderItems.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier)).Value = orderId;
                    cmdOrderItems.Parameters.Add(new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier)).Value = new Guid(oi.ItemId);
                    cmdOrderItems.Parameters.Add(new SqlParameter("@Quantity", oi.Quantity));

                    if (oi.DiscountId.Equals("0"))
                        cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountId", System.Data.SqlTypes.SqlGuid.Null));
                    else
                        cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountId", SqlDbType.UniqueIdentifier)).Value = new Guid(oi.DiscountId);

                    if (oi.DiscountId.Equals("0"))
                        cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountAmount", System.Data.SqlTypes.SqlMoney.Null));
                    else
                        cmdOrderItems.Parameters.Add(new SqlParameter("@DiscountAmount", oi.DiscountAmount));

                    cmdOrderItems.Parameters.Add(new SqlParameter("@ItemPrice", oi.ItemPrice));
                    cmdOrderItems.Parameters.Add(new SqlParameter("@ItemName", oi.ItemName));
                    cmdOrderItems.Parameters.Add(new SqlParameter("@OptionsDescription", oi.OptionsDescription));
                    cmdOrderItems.Parameters.Add(new SqlParameter("@PrintedTo", int.Parse("0")));
                    cmdOrderItems.Parameters.Add(new SqlParameter("@AlreadyPrinted", int.Parse("0")));

                    cmdOrderItems.Parameters.Add(new SqlParameter("@AnItemsPrice", oi.Oneprice));
                    cmdOrderItems.Parameters.Add(new SqlParameter("@ItemNumber", oi.ItemNumber));
                    cmdOrderItems.Parameters.Add(new SqlParameter("@OptionsPrice", oi.OptionsPrice));

                    conn.Open();
                    cmdOrderItems.ExecuteNonQuery();
                    conn.Close();
                    cmdOrderItems.Parameters.Clear();
                    if (isStockItem(oi.ItemId))
                        decreaseItemQuantity(oi.ItemId, (int)oi.Quantity);
                }

                foreach (ItemOptions io in itemOptions)
                {
                    cmdOrderOptions.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.UniqueIdentifier)).Value = orderId;
                    cmdOrderOptions.Parameters.Add(new SqlParameter("@ItemId", SqlDbType.UniqueIdentifier)).Value = new Guid(io.ItemId);
                    //ItemNumber
                    cmdOrderOptions.Parameters.Add(new SqlParameter("@ItemNumber", io.ItemNumber));

                    cmdOrderOptions.Parameters.Add(new SqlParameter("@OptionId", SqlDbType.UniqueIdentifier)).Value = new Guid(io.OptionId);
                    cmdOrderOptions.Parameters.Add(new SqlParameter("@OptionCost", io.OptionsCost));
                    conn.Open();
                    cmdOrderOptions.ExecuteNonQuery();
                    conn.Close();
                    cmdOrderOptions.Parameters.Clear();
                }

                if (ticketNumber != 0)
                    lblTicket.Text = (this.language == 0 ? LocRM.GetString("AzTicket") : LocRM.GetString("EngTicket")) + ticketNumber;
            }
        }

        private void decreaseItemQuantity(string oid, int qty)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update [Item] Set [QtyOnHand]=[QtyOnHand]-" + qty + " where  [Oid]=N'" + oid + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                XtraMessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        private void restoreDeletedItems()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    foreach (OrderItems o in orderDeletedItems)
                    {
                        con.Open();
                        using (SqlCommand command = new SqlCommand("Update [Item] Set [QtyOnHand]=[QtyOnHand]+" + o.Quantity + " where  [Oid]=N'" + o.ItemId + "'", con))
                        {
                            command.ExecuteNonQuery();
                        }
                        con.Close();
                    }
                }
            }
            catch (SystemException ex)
            {
                XtraMessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        private void txtOdeme_Click(object sender, EventArgs e)
        {
            NumbersKeyboard numbersKeyboard = new NumbersKeyboard();
            numbersKeyboard.ShowDialog();

            if (!NumbersKeyboard.a.ToString().Equals("-1"))
            {
                decimal value = NumbersKeyboard.a;
                txtOdeme.Text = value.ToString();
                if (Decimal.Parse(txtOdeme.Text) >= decimal.Parse(txtYekun.Text))
                    lblQaliq.Text = Math.Round((Decimal.Parse(txtOdeme.Text) - decimal.Parse(txtYekun.Text)), 2).ToString();

                else
                    lblQaliq.Text = "";
            }
        }

        private void print(string orderId)
        {
            DataSet firmInfoDS = PDataset("Select (Select Top 1  [Logo] from [Info]) as Logo,(Select Top 1  [CompanyName] from [Info]) as FirmName,(Select Top 1  [PrintComment] from [Info]) as PrintComment,[Amount] as Amount,[TotalAmount] as TotalAmount,[TicketNumber] as TicketNumber,IsNull([DiscountAmount],0.00) as DiscountAmount,'s' as ItemsDiscountAmount,convert(varchar,dateadd(day,-1,OrderDate),104) as OrderDate,convert(varchar,dateadd(day,-1,OrderDate),108) as OrderTime,Isnull((Select TableName from Tables where Oid=TableId),'') as TableName  from [Orders] where [Oid]=N'" + orderId + "'");
            DataTable firmInfoTable = firmInfoDS.Tables[0];
            firmInfoTable.Rows[0]["ItemsDiscountAmount"] = Math.Round(decimal.Parse(firmInfoTable.Rows[0]["Amount"].ToString()) - (decimal.Parse(firmInfoTable.Rows[0]["TotalAmount"].ToString()) + decimal.Parse(firmInfoTable.Rows[0]["DiscountAmount"].ToString())), 2);
            DataSet storeInfoDs = PDataset("SELECT  Distinct  Store.Oid, Store.StoreName as StoreName,Item.Store as StoreOid fROM         OrderDetail INNER JOIN   Item ON  OrderDetail.ItemId =  Item.Oid INNER JOIN       Store ON  Item.Store =Store.Oid where [OrderDetail].[OrderId]=N'" + orderId + "'");
            DataTable storeInfoTable = storeInfoDs.Tables[0];

            DataSet itemsDS = PDataset("SELECT      OrderDetail.ItemId as ItemOid, OrderDetail.Quantity as Quantity,  OrderDetail.ItemName as ItemName,  OrderDetail.OptionsDescription as OptionsDescription,OrderDetail.AnItemsPrice as AnItemPrice,OrderDetail.ItemPrice as ItemsPrice,OrderDetail.OptionsPrice as OptionsPrice, OrderDetail.DiscountAmount as DiscountAmount, Item.Store as StoreOid,(Select [DiscountType] from [Discounts] where [Discounts].[Oid]=OrderDetail.DiscountId) as DiscId,'s' as DiscType FROM         OrderDetail INNER JOIN   Item ON OrderDetail.ItemId =Item.Oid where OrderDetail.OrderId=N'" + orderId + "' ");
            DataTable itemTable = itemsDS.Tables[0];
            foreach (DataRow d in itemTable.Rows)
            {
                if (!string.IsNullOrEmpty(d["DiscId"].ToString()))
                {
                    if (int.Parse(d["DiscId"].ToString()) == 0)
                        d["DiscType"] = "%";
                    else
                        d["DiscType"] = "";
                }
                else
                {
                    d["DiscType"] = "";
                }
            }
            itemsDS.Tables.Clear();
            firmInfoDS.Tables.Clear();
            storeInfoDs.Tables.Clear();
            DataSet orderBillDataSet = new DataSet();
            firmInfoTable.TableName = "FirmInfo";
            storeInfoTable.TableName = "Stores";
            itemTable.TableName = "Items";
            orderBillDataSet.Tables.Add(firmInfoTable);
            orderBillDataSet.Tables.Add(storeInfoTable);
            orderBillDataSet.Tables.Add(itemTable);

            DataColumn colParent = orderBillDataSet.Tables[1].Columns["StoreOid"];
            DataColumn colChild = orderBillDataSet.Tables[2].Columns["StoreOid"];
            DataRelation drEmployeeStatus = new DataRelation("FK_Stores_Items", colParent, colChild);

            orderBillDataSet.Relations.Add(drEmployeeStatus);
            //??????????????????????????????????
            //  updateOrderSetPrinted(orderId, 2);
            int orderStatus = getOrderStatusByOrderId(orderId);
            string msg = "";

            if (orderStatus == 0 || orderStatus == 1)
            {
                updateOrderSetPrinted(orderId, 1);
                msg = this.language == 0 ? LocRM.GetString("AzActive") : LocRM.GetString("EngActive");
            }
            else if (orderStatus == 2)
            {
                updateOrderSetPrinted(orderId, 2);
                msg = this.language == 0 ? LocRM.GetString("AzPaid") : LocRM.GetString("EngPaid");
            }
            else if (orderStatus == 3)
            {
                updateOrderSetPrinted(orderId, 3);
                msg = this.language == 0 ? LocRM.GetString("AzDeleted") : LocRM.GetString("EngDeleted");
            }

            ReportPrintTool printTool = new ReportPrintTool(new Report.OrderBill(orderBillDataSet, msg));
            printTool.Report.CreateDocument(false);

            printTool.ShowPreviewDialog();
        }

        private void updateOrderSetPrinted(string orderId, int orderStatus)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update [Orders] Set [OrderStatus]=" + orderStatus + ",[Printed]=1 where  [Oid]=N'" + orderId + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        private int getOrderStatusByOrderId(string orderId)
        {
            DataSet firmInfoDS = PDataset("Select OrderStatus from [Orders] where [Oid]=N'" + orderId + "'");
            return Int32.Parse(firmInfoDS.Tables[0].Rows[0][0].ToString());
        }

        private void btnOde_Click(object sender, EventArgs e)
        {
            if (Decimal.Parse(txtOdeme.Text) >= decimal.Parse(txtYekun.Text))
            {
                using (var con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    var cmd = "update [MinAFastFood].[dbo].[Orders] set [OrderStatus]=2 where [Oid]=N'" + orderOid + "' ";
                    using (var updateCmd = new SqlCommand(cmd, con))
                    {
                        con.Open();
                        updateCmd.ExecuteNonQuery();
                        con.Close();
                    }
                }

                print(orderOid);
                btnNewOrder_Click(null, null);
            }
            else
            {
                XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzNotEnoughMoney") : LocRM.GetString("EngNotEnoughMoney"));
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtItemAmount_Paint(object sender, PaintEventArgs e)
        {
            RectangleF rec = e.Graphics.ClipBounds;
            rec.Inflate(-1, -1);
            e.Graphics.DrawRectangle(Pens.Red,
                rec.Left,
                rec.Top,
                rec.Width,
                rec.Height);
        }

        private void btnTable_Click(object sender, EventArgs e)
        {
            OrderTables tables = new OrderTables(this.language);
            tables.ShowDialog();
        }

        private bool hasError()
        {
            bool ch = false;
            if (string.IsNullOrEmpty(orderOid))
            {
                foreach (OrderItems o in orderItems)
                {
                    if (isStockItem(o.ItemId))
                    {
                        int curCount = getCurrentItemCount(o.ItemId);
                        int dbqtyCount = getItemQuantity(o.ItemId);
                        if (dbqtyCount < curCount)
                            ch = true;
                    }
                }
            }
            else
            {
                foreach (OrderItems o in orderItems)
                {
                    if (isStockItem(o.ItemId))
                    {
                        int deletedCount = getCurrentOldItemCount(orderDeletedItems, o.ItemId);
                        int curCount = getCurrentItemCount(o.ItemId);
                        int dbqtyCount = getItemQuantity(o.ItemId);
                        if ((dbqtyCount + deletedCount) < curCount)
                            ch = true;
                    }
                }
            }
            return ch;
        }


        private void dataRepeater1_CurrentItemIndexChanged(object sender, EventArgs e)
        {
            XIndent = 0;
            YIndent = 0;
            this.count = 0;
        
            
           LabelControl itemId = (LabelControl)dataRepeater1.CurrentItem.Controls["lblItemId"];
            fillOptions(itemId.Text);
             
        }

        
        

         
    }
}