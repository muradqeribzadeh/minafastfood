﻿using System;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace MinAFastFoodClient
{
    public partial class NumbersKeyboard : DevExpress.XtraEditors.XtraForm
    {
        public NumbersKeyboard()
        {
            InitializeComponent();
        }
        public NumbersKeyboard(int password) {
            InitializeComponent();
            txtNumbers.Properties.PasswordChar = '*';
            btnDot.Enabled = false;
        }
        public NumbersKeyboard(string qty)
        {
            InitializeComponent();
      
            btnDot.Enabled = false;
        }
        public static decimal a;
        public static string password;
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
               // int index = txtNumbers.SelectionStart;
              //  txtNumbers.Text = txtNumbers.Text.Remove(txtNumbers.SelectionStart - 1, 1);
             //   txtNumbers.Select(index - 1, 1);
                //    txtNumbers.Focus();
                txtNumbers.Text = txtNumbers.Text.Remove(txtNumbers.Text.Length - 1, 1);
            }
            catch (Exception ex)
            {
            }
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            txtNumbers.Text = txtNumbers.Text + btn.Text;
        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            try
            {
                a = Decimal.Parse(txtNumbers.Text);
                password = txtNumbers.Text;
                Close();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Həqiqi ədəd daxil edin!");
            }
        }

        private void NumbersKeyboard_Load(object sender, EventArgs e)
        {
            a = -1;
            password = "a";
        }

        private void txtNumbers_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                simpleButton13_Click(null, null);

            }
        }

         
    }
}