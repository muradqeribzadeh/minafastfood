﻿namespace MinAFastFoodClient
{
    partial class ClockForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClockForm));
            this.lblInOrOut = new DevExpress.XtraEditors.LabelControl();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblTimer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblInOrOut
            // 
            resources.ApplyResources(this.lblInOrOut, "lblInOrOut");
            this.lblInOrOut.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("lblInOrOut.Appearance.DisabledImage")));
            this.lblInOrOut.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblInOrOut.Appearance.Font")));
            this.lblInOrOut.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblInOrOut.Appearance.ForeColor")));
            this.lblInOrOut.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("lblInOrOut.Appearance.GradientMode")));
            this.lblInOrOut.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("lblInOrOut.Appearance.HoverImage")));
            this.lblInOrOut.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("lblInOrOut.Appearance.Image")));
            this.lblInOrOut.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("lblInOrOut.Appearance.PressedImage")));
            this.lblInOrOut.Name = "lblInOrOut";
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnOk.Appearance.Font")));
            this.btnOk.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnOk.Appearance.GradientMode")));
            this.btnOk.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Appearance.Image")));
            this.btnOk.Appearance.Options.UseFont = true;
            this.btnOk.Name = "btnOk";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblTimer
            // 
            resources.ApplyResources(this.lblTimer, "lblTimer");
            this.lblTimer.Name = "lblTimer";
            // 
            // ClockForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblTimer);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblInOrOut);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ClockForm";
            this.Load += new System.EventHandler(this.ClockForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblInOrOut;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblTimer;
    }
}