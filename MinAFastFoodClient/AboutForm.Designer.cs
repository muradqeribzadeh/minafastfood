﻿namespace MinAFastFoodClient
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.DisabledImage")));
            this.labelControl1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl1.Appearance.Font")));
            this.labelControl1.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl1.Appearance.FontSizeDelta")));
            this.labelControl1.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl1.Appearance.FontStyleDelta")));
            this.labelControl1.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl1.Appearance.GradientMode")));
            this.labelControl1.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.HoverImage")));
            this.labelControl1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.Image")));
            this.labelControl1.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.PressedImage")));
            this.labelControl1.Name = "labelControl1";
            // 
            // labelControl3
            // 
            resources.ApplyResources(this.labelControl3, "labelControl3");
            this.labelControl3.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.DisabledImage")));
            this.labelControl3.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl3.Appearance.Font")));
            this.labelControl3.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl3.Appearance.FontSizeDelta")));
            this.labelControl3.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl3.Appearance.FontStyleDelta")));
            this.labelControl3.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl3.Appearance.GradientMode")));
            this.labelControl3.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.HoverImage")));
            this.labelControl3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.Image")));
            this.labelControl3.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.PressedImage")));
            this.labelControl3.Name = "labelControl3";
            // 
            // labelControl4
            // 
            resources.ApplyResources(this.labelControl4, "labelControl4");
            this.labelControl4.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.DisabledImage")));
            this.labelControl4.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl4.Appearance.Font")));
            this.labelControl4.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl4.Appearance.FontSizeDelta")));
            this.labelControl4.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl4.Appearance.FontStyleDelta")));
            this.labelControl4.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl4.Appearance.GradientMode")));
            this.labelControl4.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.HoverImage")));
            this.labelControl4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.Image")));
            this.labelControl4.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.PressedImage")));
            this.labelControl4.Name = "labelControl4";
            // 
            // labelControl5
            // 
            resources.ApplyResources(this.labelControl5, "labelControl5");
            this.labelControl5.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.DisabledImage")));
            this.labelControl5.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl5.Appearance.Font")));
            this.labelControl5.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl5.Appearance.FontSizeDelta")));
            this.labelControl5.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl5.Appearance.FontStyleDelta")));
            this.labelControl5.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl5.Appearance.GradientMode")));
            this.labelControl5.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.HoverImage")));
            this.labelControl5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.Image")));
            this.labelControl5.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.PressedImage")));
            this.labelControl5.Name = "labelControl5";
            // 
            // linkLabel1
            // 
            resources.ApplyResources(this.linkLabel1, "linkLabel1");
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.TabStop = true;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // labelControl6
            // 
            resources.ApplyResources(this.labelControl6, "labelControl6");
            this.labelControl6.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.DisabledImage")));
            this.labelControl6.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl6.Appearance.Font")));
            this.labelControl6.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl6.Appearance.FontSizeDelta")));
            this.labelControl6.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl6.Appearance.FontStyleDelta")));
            this.labelControl6.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl6.Appearance.ForeColor")));
            this.labelControl6.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl6.Appearance.GradientMode")));
            this.labelControl6.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.HoverImage")));
            this.labelControl6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.Image")));
            this.labelControl6.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.PressedImage")));
            this.labelControl6.Name = "labelControl6";
            // 
            // labelControl7
            // 
            resources.ApplyResources(this.labelControl7, "labelControl7");
            this.labelControl7.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.DisabledImage")));
            this.labelControl7.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl7.Appearance.Font")));
            this.labelControl7.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl7.Appearance.FontSizeDelta")));
            this.labelControl7.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl7.Appearance.FontStyleDelta")));
            this.labelControl7.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl7.Appearance.GradientMode")));
            this.labelControl7.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.HoverImage")));
            this.labelControl7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.Image")));
            this.labelControl7.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.PressedImage")));
            this.labelControl7.Name = "labelControl7";
            // 
            // labelControl2
            // 
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.DisabledImage")));
            this.labelControl2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl2.Appearance.Font")));
            this.labelControl2.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl2.Appearance.FontSizeDelta")));
            this.labelControl2.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl2.Appearance.FontStyleDelta")));
            this.labelControl2.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl2.Appearance.ForeColor")));
            this.labelControl2.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl2.Appearance.GradientMode")));
            this.labelControl2.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.HoverImage")));
            this.labelControl2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.Image")));
            this.labelControl2.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.PressedImage")));
            this.labelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.TopLeft;
            this.labelControl2.Name = "labelControl2";
            // 
            // labelControl8
            // 
            resources.ApplyResources(this.labelControl8, "labelControl8");
            this.labelControl8.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.DisabledImage")));
            this.labelControl8.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl8.Appearance.Font")));
            this.labelControl8.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl8.Appearance.FontSizeDelta")));
            this.labelControl8.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl8.Appearance.FontStyleDelta")));
            this.labelControl8.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl8.Appearance.GradientMode")));
            this.labelControl8.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.HoverImage")));
            this.labelControl8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.Image")));
            this.labelControl8.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.PressedImage")));
            this.labelControl8.Name = "labelControl8";
            // 
            // labelControl9
            // 
            resources.ApplyResources(this.labelControl9, "labelControl9");
            this.labelControl9.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl9.Appearance.DisabledImage")));
            this.labelControl9.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl9.Appearance.Font")));
            this.labelControl9.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl9.Appearance.FontSizeDelta")));
            this.labelControl9.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl9.Appearance.FontStyleDelta")));
            this.labelControl9.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl9.Appearance.GradientMode")));
            this.labelControl9.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl9.Appearance.HoverImage")));
            this.labelControl9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl9.Appearance.Image")));
            this.labelControl9.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl9.Appearance.PressedImage")));
            this.labelControl9.Name = "labelControl9";
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::MinAFastFoodClient.Properties.Resources.mina21;
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // AboutForm
            // 
            resources.ApplyResources(this, "$this");
            this.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("AboutForm.Appearance.BackColor")));
            this.Appearance.FontSizeDelta = ((int)(resources.GetObject("AboutForm.Appearance.FontSizeDelta")));
            this.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("AboutForm.Appearance.FontStyleDelta")));
            this.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("AboutForm.Appearance.GradientMode")));
            this.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("AboutForm.Appearance.Image")));
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseFont = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AboutForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
    }
}