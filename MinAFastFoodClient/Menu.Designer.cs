﻿namespace MinAFastFoodClient
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.dataRepeater1 = new Microsoft.VisualBasic.PowerPacks.DataRepeater();
            this.txtItemAmount = new System.Windows.Forms.TextBox();
            this.lblItemNumber = new DevExpress.XtraEditors.LabelControl();
            this.lblItemId = new DevExpress.XtraEditors.LabelControl();
            this.txtOptionPrice = new System.Windows.Forms.TextBox();
            this.richTextOptions = new System.Windows.Forms.RichTextBox();
            this.txtItemPrices = new System.Windows.Forms.TextBox();
            this.txtOnePrice = new System.Windows.Forms.TextBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.btnCancelDiscount = new DevExpress.XtraEditors.SimpleButton();
            this.txtOdeme = new DevExpress.XtraEditors.TextEdit();
            this.lblQaliq = new DevExpress.XtraEditors.LabelControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.optionControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.itemControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.menuControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDiscount = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtYekun = new DevExpress.XtraEditors.TextEdit();
            this.txtEndirim = new DevExpress.XtraEditors.TextEdit();
            this.txtUmMebleg = new DevExpress.XtraEditors.TextEdit();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnNewOrder = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.lblDate = new DevExpress.XtraEditors.LabelControl();
            this.lblTicket = new DevExpress.XtraEditors.LabelControl();
            this.lblTableName = new DevExpress.XtraEditors.LabelControl();
            this.btnOde = new DevExpress.XtraEditors.SimpleButton();
            this.btnDone = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnDiscount = new DevExpress.XtraEditors.SimpleButton();
            this.btnTable = new DevExpress.XtraEditors.SimpleButton();
            this.btnNotes = new DevExpress.XtraEditors.SimpleButton();
            this.dataRepeater1.ItemTemplate.SuspendLayout();
            this.dataRepeater1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtOdeme.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYekun.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndirim.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUmMebleg.Properties)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataRepeater1
            // 
            resources.ApplyResources(this.dataRepeater1, "dataRepeater1");
            this.dataRepeater1.BackColor = System.Drawing.Color.White;
            // 
            // dataRepeater1.ItemTemplate
            // 
            resources.ApplyResources(this.dataRepeater1.ItemTemplate, "dataRepeater1.ItemTemplate");
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtItemAmount);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.lblItemNumber);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.lblItemId);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtOptionPrice);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.richTextOptions);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtItemPrices);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtOnePrice);
            this.dataRepeater1.ItemTemplate.Controls.Add(this.txtItemName);
            this.dataRepeater1.Name = "dataRepeater1";
            this.dataRepeater1.CurrentItemIndexChanged += new System.EventHandler(this.dataRepeater1_CurrentItemIndexChanged);
            // 
            // txtItemAmount
            // 
            resources.ApplyResources(this.txtItemAmount, "txtItemAmount");
            this.txtItemAmount.BackColor = System.Drawing.Color.Snow;
            this.txtItemAmount.ForeColor = System.Drawing.Color.Black;
            this.txtItemAmount.Name = "txtItemAmount";
            this.txtItemAmount.Click += new System.EventHandler(this.txtItemAmount_Click);
            // 
            // lblItemNumber
            // 
            resources.ApplyResources(this.lblItemNumber, "lblItemNumber");
            this.lblItemNumber.Name = "lblItemNumber";
            // 
            // lblItemId
            // 
            resources.ApplyResources(this.lblItemId, "lblItemId");
            this.lblItemId.Name = "lblItemId";
            // 
            // txtOptionPrice
            // 
            resources.ApplyResources(this.txtOptionPrice, "txtOptionPrice");
            this.txtOptionPrice.BackColor = System.Drawing.Color.White;
            this.txtOptionPrice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOptionPrice.ForeColor = System.Drawing.Color.Blue;
            this.txtOptionPrice.Name = "txtOptionPrice";
            this.txtOptionPrice.ReadOnly = true;
            // 
            // richTextOptions
            // 
            resources.ApplyResources(this.richTextOptions, "richTextOptions");
            this.richTextOptions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextOptions.ForeColor = System.Drawing.Color.Blue;
            this.richTextOptions.Name = "richTextOptions";
            // 
            // txtItemPrices
            // 
            resources.ApplyResources(this.txtItemPrices, "txtItemPrices");
            this.txtItemPrices.BackColor = System.Drawing.Color.White;
            this.txtItemPrices.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtItemPrices.Name = "txtItemPrices";
            this.txtItemPrices.ReadOnly = true;
            // 
            // txtOnePrice
            // 
            resources.ApplyResources(this.txtOnePrice, "txtOnePrice");
            this.txtOnePrice.BackColor = System.Drawing.Color.White;
            this.txtOnePrice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOnePrice.Name = "txtOnePrice";
            this.txtOnePrice.ReadOnly = true;
            // 
            // txtItemName
            // 
            resources.ApplyResources(this.txtItemName, "txtItemName");
            this.txtItemName.BackColor = System.Drawing.Color.White;
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.ReadOnly = true;
            // 
            // btnCancelDiscount
            // 
            resources.ApplyResources(this.btnCancelDiscount, "btnCancelDiscount");
            this.btnCancelDiscount.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnCancelDiscount.Appearance.BackColor")));
            this.btnCancelDiscount.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnCancelDiscount.Appearance.Font")));
            this.btnCancelDiscount.Appearance.FontSizeDelta = ((int)(resources.GetObject("btnCancelDiscount.Appearance.FontSizeDelta")));
            this.btnCancelDiscount.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("btnCancelDiscount.Appearance.FontStyleDelta")));
            this.btnCancelDiscount.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnCancelDiscount.Appearance.GradientMode")));
            this.btnCancelDiscount.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelDiscount.Appearance.Image")));
            this.btnCancelDiscount.Appearance.Options.UseBackColor = true;
            this.btnCancelDiscount.Appearance.Options.UseFont = true;
            this.btnCancelDiscount.Appearance.Options.UseTextOptions = true;
            this.btnCancelDiscount.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnCancelDiscount.Name = "btnCancelDiscount";
            this.btnCancelDiscount.Click += new System.EventHandler(this.btnCancelDiscount_Click);
            // 
            // txtOdeme
            // 
            resources.ApplyResources(this.txtOdeme, "txtOdeme");
            this.txtOdeme.Name = "txtOdeme";
            this.txtOdeme.Properties.AccessibleDescription = resources.GetString("txtOdeme.Properties.AccessibleDescription");
            this.txtOdeme.Properties.AccessibleName = resources.GetString("txtOdeme.Properties.AccessibleName");
            this.txtOdeme.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtOdeme.Properties.Appearance.Font")));
            this.txtOdeme.Properties.Appearance.FontSizeDelta = ((int)(resources.GetObject("txtOdeme.Properties.Appearance.FontSizeDelta")));
            this.txtOdeme.Properties.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("txtOdeme.Properties.Appearance.FontStyleDelta")));
            this.txtOdeme.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtOdeme.Properties.Appearance.ForeColor")));
            this.txtOdeme.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtOdeme.Properties.Appearance.GradientMode")));
            this.txtOdeme.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtOdeme.Properties.Appearance.Image")));
            this.txtOdeme.Properties.Appearance.Options.UseFont = true;
            this.txtOdeme.Properties.Appearance.Options.UseForeColor = true;
            this.txtOdeme.Properties.AutoHeight = ((bool)(resources.GetObject("txtOdeme.Properties.AutoHeight")));
            this.txtOdeme.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtOdeme.Properties.Mask.AutoComplete")));
            this.txtOdeme.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtOdeme.Properties.Mask.BeepOnError")));
            this.txtOdeme.Properties.Mask.EditMask = resources.GetString("txtOdeme.Properties.Mask.EditMask");
            this.txtOdeme.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtOdeme.Properties.Mask.IgnoreMaskBlank")));
            this.txtOdeme.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtOdeme.Properties.Mask.MaskType")));
            this.txtOdeme.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtOdeme.Properties.Mask.PlaceHolder")));
            this.txtOdeme.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtOdeme.Properties.Mask.SaveLiteral")));
            this.txtOdeme.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtOdeme.Properties.Mask.ShowPlaceHolders")));
            this.txtOdeme.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtOdeme.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtOdeme.Properties.NullValuePrompt = resources.GetString("txtOdeme.Properties.NullValuePrompt");
            this.txtOdeme.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtOdeme.Properties.NullValuePromptShowForEmptyValue")));
            this.txtOdeme.Click += new System.EventHandler(this.txtOdeme_Click);
            // 
            // lblQaliq
            // 
            resources.ApplyResources(this.lblQaliq, "lblQaliq");
            this.lblQaliq.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("lblQaliq.Appearance.DisabledImage")));
            this.lblQaliq.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblQaliq.Appearance.Font")));
            this.lblQaliq.Appearance.FontSizeDelta = ((int)(resources.GetObject("lblQaliq.Appearance.FontSizeDelta")));
            this.lblQaliq.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("lblQaliq.Appearance.FontStyleDelta")));
            this.lblQaliq.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("lblQaliq.Appearance.GradientMode")));
            this.lblQaliq.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("lblQaliq.Appearance.HoverImage")));
            this.lblQaliq.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("lblQaliq.Appearance.Image")));
            this.lblQaliq.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("lblQaliq.Appearance.PressedImage")));
            this.lblQaliq.Name = "lblQaliq";
            // 
            // groupBox3
            // 
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Controls.Add(this.optionControl);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // optionControl
            // 
            resources.ApplyResources(this.optionControl, "optionControl");
            this.optionControl.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("optionControl.Appearance.BackColor")));
            this.optionControl.Appearance.FontSizeDelta = ((int)(resources.GetObject("optionControl.Appearance.FontSizeDelta")));
            this.optionControl.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("optionControl.Appearance.FontStyleDelta")));
            this.optionControl.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("optionControl.Appearance.GradientMode")));
            this.optionControl.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("optionControl.Appearance.Image")));
            this.optionControl.Appearance.Options.UseBackColor = true;
            this.optionControl.Name = "optionControl";
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.itemControl);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // itemControl
            // 
            resources.ApplyResources(this.itemControl, "itemControl");
            this.itemControl.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("itemControl.Appearance.BackColor")));
            this.itemControl.Appearance.FontSizeDelta = ((int)(resources.GetObject("itemControl.Appearance.FontSizeDelta")));
            this.itemControl.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("itemControl.Appearance.FontStyleDelta")));
            this.itemControl.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("itemControl.Appearance.GradientMode")));
            this.itemControl.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("itemControl.Appearance.Image")));
            this.itemControl.Appearance.Options.UseBackColor = true;
            this.itemControl.Name = "itemControl";
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.menuControl);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // menuControl
            // 
            resources.ApplyResources(this.menuControl, "menuControl");
            this.menuControl.AllowTouchScroll = true;
            this.menuControl.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("menuControl.Appearance.BackColor")));
            this.menuControl.Appearance.FontSizeDelta = ((int)(resources.GetObject("menuControl.Appearance.FontSizeDelta")));
            this.menuControl.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("menuControl.Appearance.FontStyleDelta")));
            this.menuControl.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("menuControl.Appearance.GradientMode")));
            this.menuControl.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("menuControl.Appearance.Image")));
            this.menuControl.Appearance.Options.UseBackColor = true;
            this.menuControl.Name = "menuControl";
            this.menuControl.ScrollBarSmallChange = 30;
            // 
            // labelControl5
            // 
            resources.ApplyResources(this.labelControl5, "labelControl5");
            this.labelControl5.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.DisabledImage")));
            this.labelControl5.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl5.Appearance.Font")));
            this.labelControl5.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl5.Appearance.FontSizeDelta")));
            this.labelControl5.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl5.Appearance.FontStyleDelta")));
            this.labelControl5.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl5.Appearance.GradientMode")));
            this.labelControl5.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.HoverImage")));
            this.labelControl5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.Image")));
            this.labelControl5.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl5.Appearance.PressedImage")));
            this.labelControl5.Name = "labelControl5";
            // 
            // labelControl3
            // 
            resources.ApplyResources(this.labelControl3, "labelControl3");
            this.labelControl3.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.DisabledImage")));
            this.labelControl3.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl3.Appearance.Font")));
            this.labelControl3.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl3.Appearance.FontSizeDelta")));
            this.labelControl3.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl3.Appearance.FontStyleDelta")));
            this.labelControl3.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl3.Appearance.GradientMode")));
            this.labelControl3.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.HoverImage")));
            this.labelControl3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.Image")));
            this.labelControl3.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl3.Appearance.PressedImage")));
            this.labelControl3.Name = "labelControl3";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.lblDiscount);
            this.panel1.Controls.Add(this.labelControl4);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.txtYekun);
            this.panel1.Controls.Add(this.txtEndirim);
            this.panel1.Controls.Add(this.txtUmMebleg);
            this.panel1.Name = "panel1";
            // 
            // lblDiscount
            // 
            resources.ApplyResources(this.lblDiscount, "lblDiscount");
            this.lblDiscount.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("lblDiscount.Appearance.DisabledImage")));
            this.lblDiscount.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblDiscount.Appearance.Font")));
            this.lblDiscount.Appearance.FontSizeDelta = ((int)(resources.GetObject("lblDiscount.Appearance.FontSizeDelta")));
            this.lblDiscount.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("lblDiscount.Appearance.FontStyleDelta")));
            this.lblDiscount.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblDiscount.Appearance.ForeColor")));
            this.lblDiscount.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("lblDiscount.Appearance.GradientMode")));
            this.lblDiscount.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("lblDiscount.Appearance.HoverImage")));
            this.lblDiscount.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("lblDiscount.Appearance.Image")));
            this.lblDiscount.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("lblDiscount.Appearance.PressedImage")));
            this.lblDiscount.Name = "lblDiscount";
            // 
            // labelControl4
            // 
            resources.ApplyResources(this.labelControl4, "labelControl4");
            this.labelControl4.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.DisabledImage")));
            this.labelControl4.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl4.Appearance.Font")));
            this.labelControl4.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl4.Appearance.FontSizeDelta")));
            this.labelControl4.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl4.Appearance.FontStyleDelta")));
            this.labelControl4.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl4.Appearance.ForeColor")));
            this.labelControl4.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl4.Appearance.GradientMode")));
            this.labelControl4.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.HoverImage")));
            this.labelControl4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.Image")));
            this.labelControl4.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl4.Appearance.PressedImage")));
            this.labelControl4.Name = "labelControl4";
            // 
            // labelControl2
            // 
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.DisabledImage")));
            this.labelControl2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl2.Appearance.Font")));
            this.labelControl2.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl2.Appearance.FontSizeDelta")));
            this.labelControl2.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl2.Appearance.FontStyleDelta")));
            this.labelControl2.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl2.Appearance.ForeColor")));
            this.labelControl2.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl2.Appearance.GradientMode")));
            this.labelControl2.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.HoverImage")));
            this.labelControl2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.Image")));
            this.labelControl2.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl2.Appearance.PressedImage")));
            this.labelControl2.Name = "labelControl2";
            // 
            // labelControl1
            // 
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.DisabledImage")));
            this.labelControl1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl1.Appearance.Font")));
            this.labelControl1.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl1.Appearance.FontSizeDelta")));
            this.labelControl1.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl1.Appearance.FontStyleDelta")));
            this.labelControl1.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl1.Appearance.GradientMode")));
            this.labelControl1.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.HoverImage")));
            this.labelControl1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.Image")));
            this.labelControl1.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl1.Appearance.PressedImage")));
            this.labelControl1.Name = "labelControl1";
            // 
            // txtYekun
            // 
            resources.ApplyResources(this.txtYekun, "txtYekun");
            this.txtYekun.Name = "txtYekun";
            this.txtYekun.Properties.AccessibleDescription = resources.GetString("txtYekun.Properties.AccessibleDescription");
            this.txtYekun.Properties.AccessibleName = resources.GetString("txtYekun.Properties.AccessibleName");
            this.txtYekun.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtYekun.Properties.Appearance.Font")));
            this.txtYekun.Properties.Appearance.FontSizeDelta = ((int)(resources.GetObject("txtYekun.Properties.Appearance.FontSizeDelta")));
            this.txtYekun.Properties.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("txtYekun.Properties.Appearance.FontStyleDelta")));
            this.txtYekun.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtYekun.Properties.Appearance.GradientMode")));
            this.txtYekun.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtYekun.Properties.Appearance.Image")));
            this.txtYekun.Properties.Appearance.Options.UseFont = true;
            this.txtYekun.Properties.AutoHeight = ((bool)(resources.GetObject("txtYekun.Properties.AutoHeight")));
            this.txtYekun.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtYekun.Properties.Mask.AutoComplete")));
            this.txtYekun.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtYekun.Properties.Mask.BeepOnError")));
            this.txtYekun.Properties.Mask.EditMask = resources.GetString("txtYekun.Properties.Mask.EditMask");
            this.txtYekun.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtYekun.Properties.Mask.IgnoreMaskBlank")));
            this.txtYekun.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtYekun.Properties.Mask.MaskType")));
            this.txtYekun.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtYekun.Properties.Mask.PlaceHolder")));
            this.txtYekun.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtYekun.Properties.Mask.SaveLiteral")));
            this.txtYekun.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtYekun.Properties.Mask.ShowPlaceHolders")));
            this.txtYekun.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtYekun.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtYekun.Properties.NullValuePrompt = resources.GetString("txtYekun.Properties.NullValuePrompt");
            this.txtYekun.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtYekun.Properties.NullValuePromptShowForEmptyValue")));
            this.txtYekun.Properties.ReadOnly = true;
            // 
            // txtEndirim
            // 
            resources.ApplyResources(this.txtEndirim, "txtEndirim");
            this.txtEndirim.Name = "txtEndirim";
            this.txtEndirim.Properties.AccessibleDescription = resources.GetString("txtEndirim.Properties.AccessibleDescription");
            this.txtEndirim.Properties.AccessibleName = resources.GetString("txtEndirim.Properties.AccessibleName");
            this.txtEndirim.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtEndirim.Properties.Appearance.Font")));
            this.txtEndirim.Properties.Appearance.FontSizeDelta = ((int)(resources.GetObject("txtEndirim.Properties.Appearance.FontSizeDelta")));
            this.txtEndirim.Properties.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("txtEndirim.Properties.Appearance.FontStyleDelta")));
            this.txtEndirim.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtEndirim.Properties.Appearance.GradientMode")));
            this.txtEndirim.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtEndirim.Properties.Appearance.Image")));
            this.txtEndirim.Properties.Appearance.Options.UseFont = true;
            this.txtEndirim.Properties.AutoHeight = ((bool)(resources.GetObject("txtEndirim.Properties.AutoHeight")));
            this.txtEndirim.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtEndirim.Properties.Mask.AutoComplete")));
            this.txtEndirim.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtEndirim.Properties.Mask.BeepOnError")));
            this.txtEndirim.Properties.Mask.EditMask = resources.GetString("txtEndirim.Properties.Mask.EditMask");
            this.txtEndirim.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtEndirim.Properties.Mask.IgnoreMaskBlank")));
            this.txtEndirim.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtEndirim.Properties.Mask.MaskType")));
            this.txtEndirim.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtEndirim.Properties.Mask.PlaceHolder")));
            this.txtEndirim.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtEndirim.Properties.Mask.SaveLiteral")));
            this.txtEndirim.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtEndirim.Properties.Mask.ShowPlaceHolders")));
            this.txtEndirim.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtEndirim.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtEndirim.Properties.NullValuePrompt = resources.GetString("txtEndirim.Properties.NullValuePrompt");
            this.txtEndirim.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtEndirim.Properties.NullValuePromptShowForEmptyValue")));
            this.txtEndirim.Properties.ReadOnly = true;
            // 
            // txtUmMebleg
            // 
            resources.ApplyResources(this.txtUmMebleg, "txtUmMebleg");
            this.txtUmMebleg.Name = "txtUmMebleg";
            this.txtUmMebleg.Properties.AccessibleDescription = resources.GetString("txtUmMebleg.Properties.AccessibleDescription");
            this.txtUmMebleg.Properties.AccessibleName = resources.GetString("txtUmMebleg.Properties.AccessibleName");
            this.txtUmMebleg.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtUmMebleg.Properties.Appearance.Font")));
            this.txtUmMebleg.Properties.Appearance.FontSizeDelta = ((int)(resources.GetObject("txtUmMebleg.Properties.Appearance.FontSizeDelta")));
            this.txtUmMebleg.Properties.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("txtUmMebleg.Properties.Appearance.FontStyleDelta")));
            this.txtUmMebleg.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtUmMebleg.Properties.Appearance.GradientMode")));
            this.txtUmMebleg.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtUmMebleg.Properties.Appearance.Image")));
            this.txtUmMebleg.Properties.Appearance.Options.UseFont = true;
            this.txtUmMebleg.Properties.AutoHeight = ((bool)(resources.GetObject("txtUmMebleg.Properties.AutoHeight")));
            this.txtUmMebleg.Properties.Mask.AutoComplete = ((DevExpress.XtraEditors.Mask.AutoCompleteType)(resources.GetObject("txtUmMebleg.Properties.Mask.AutoComplete")));
            this.txtUmMebleg.Properties.Mask.BeepOnError = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.BeepOnError")));
            this.txtUmMebleg.Properties.Mask.EditMask = resources.GetString("txtUmMebleg.Properties.Mask.EditMask");
            this.txtUmMebleg.Properties.Mask.IgnoreMaskBlank = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.IgnoreMaskBlank")));
            this.txtUmMebleg.Properties.Mask.MaskType = ((DevExpress.XtraEditors.Mask.MaskType)(resources.GetObject("txtUmMebleg.Properties.Mask.MaskType")));
            this.txtUmMebleg.Properties.Mask.PlaceHolder = ((char)(resources.GetObject("txtUmMebleg.Properties.Mask.PlaceHolder")));
            this.txtUmMebleg.Properties.Mask.SaveLiteral = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.SaveLiteral")));
            this.txtUmMebleg.Properties.Mask.ShowPlaceHolders = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.ShowPlaceHolders")));
            this.txtUmMebleg.Properties.Mask.UseMaskAsDisplayFormat = ((bool)(resources.GetObject("txtUmMebleg.Properties.Mask.UseMaskAsDisplayFormat")));
            this.txtUmMebleg.Properties.NullValuePrompt = resources.GetString("txtUmMebleg.Properties.NullValuePrompt");
            this.txtUmMebleg.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtUmMebleg.Properties.NullValuePromptShowForEmptyValue")));
            this.txtUmMebleg.Properties.ReadOnly = true;
            // 
            // groupBox4
            // 
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.labelControl6);
            this.groupBox4.Controls.Add(this.labelControl11);
            this.groupBox4.Controls.Add(this.labelControl10);
            this.groupBox4.Controls.Add(this.labelControl9);
            this.groupBox4.Controls.Add(this.labelControl8);
            this.groupBox4.Controls.Add(this.labelControl7);
            this.groupBox4.Controls.Add(this.labelControl12);
            this.groupBox4.Controls.Add(this.lblDate);
            this.groupBox4.Controls.Add(this.lblTicket);
            this.groupBox4.Controls.Add(this.lblTableName);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // groupBox5
            // 
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Controls.Add(this.dataRepeater1);
            this.groupBox5.Controls.Add(this.labelControl15);
            this.groupBox5.Controls.Add(this.labelControl16);
            this.groupBox5.Controls.Add(this.labelControl17);
            this.groupBox5.Controls.Add(this.labelControl18);
            this.groupBox5.Controls.Add(this.labelControl19);
            this.groupBox5.Controls.Add(this.labelControl20);
            this.groupBox5.Controls.Add(this.labelControl21);
            this.groupBox5.Controls.Add(this.labelControl22);
            this.groupBox5.Controls.Add(this.labelControl23);
            this.groupBox5.Controls.Add(this.simpleButton3);
            this.groupBox5.Controls.Add(this.simpleButton2);
            this.groupBox5.Controls.Add(this.btnNewOrder);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // labelControl15
            // 
            resources.ApplyResources(this.labelControl15, "labelControl15");
            this.labelControl15.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl15.Appearance.DisabledImage")));
            this.labelControl15.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl15.Appearance.Font")));
            this.labelControl15.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl15.Appearance.FontSizeDelta")));
            this.labelControl15.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl15.Appearance.FontStyleDelta")));
            this.labelControl15.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl15.Appearance.GradientMode")));
            this.labelControl15.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl15.Appearance.HoverImage")));
            this.labelControl15.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl15.Appearance.Image")));
            this.labelControl15.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl15.Appearance.PressedImage")));
            this.labelControl15.Name = "labelControl15";
            // 
            // labelControl16
            // 
            resources.ApplyResources(this.labelControl16, "labelControl16");
            this.labelControl16.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl16.Appearance.DisabledImage")));
            this.labelControl16.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl16.Appearance.FontSizeDelta")));
            this.labelControl16.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl16.Appearance.FontStyleDelta")));
            this.labelControl16.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl16.Appearance.ForeColor")));
            this.labelControl16.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl16.Appearance.GradientMode")));
            this.labelControl16.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl16.Appearance.HoverImage")));
            this.labelControl16.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl16.Appearance.Image")));
            this.labelControl16.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl16.Appearance.PressedImage")));
            this.labelControl16.Name = "labelControl16";
            // 
            // labelControl17
            // 
            resources.ApplyResources(this.labelControl17, "labelControl17");
            this.labelControl17.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl17.Appearance.DisabledImage")));
            this.labelControl17.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl17.Appearance.FontSizeDelta")));
            this.labelControl17.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl17.Appearance.FontStyleDelta")));
            this.labelControl17.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl17.Appearance.ForeColor")));
            this.labelControl17.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl17.Appearance.GradientMode")));
            this.labelControl17.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl17.Appearance.HoverImage")));
            this.labelControl17.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl17.Appearance.Image")));
            this.labelControl17.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl17.Appearance.PressedImage")));
            this.labelControl17.Name = "labelControl17";
            // 
            // labelControl18
            // 
            resources.ApplyResources(this.labelControl18, "labelControl18");
            this.labelControl18.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl18.Appearance.DisabledImage")));
            this.labelControl18.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl18.Appearance.FontSizeDelta")));
            this.labelControl18.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl18.Appearance.FontStyleDelta")));
            this.labelControl18.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl18.Appearance.ForeColor")));
            this.labelControl18.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl18.Appearance.GradientMode")));
            this.labelControl18.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl18.Appearance.HoverImage")));
            this.labelControl18.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl18.Appearance.Image")));
            this.labelControl18.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl18.Appearance.PressedImage")));
            this.labelControl18.Name = "labelControl18";
            // 
            // labelControl19
            // 
            resources.ApplyResources(this.labelControl19, "labelControl19");
            this.labelControl19.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl19.Appearance.DisabledImage")));
            this.labelControl19.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl19.Appearance.FontSizeDelta")));
            this.labelControl19.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl19.Appearance.FontStyleDelta")));
            this.labelControl19.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl19.Appearance.ForeColor")));
            this.labelControl19.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl19.Appearance.GradientMode")));
            this.labelControl19.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl19.Appearance.HoverImage")));
            this.labelControl19.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl19.Appearance.Image")));
            this.labelControl19.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl19.Appearance.PressedImage")));
            this.labelControl19.Name = "labelControl19";
            // 
            // labelControl20
            // 
            resources.ApplyResources(this.labelControl20, "labelControl20");
            this.labelControl20.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl20.Appearance.DisabledImage")));
            this.labelControl20.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl20.Appearance.FontSizeDelta")));
            this.labelControl20.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl20.Appearance.FontStyleDelta")));
            this.labelControl20.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl20.Appearance.ForeColor")));
            this.labelControl20.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl20.Appearance.GradientMode")));
            this.labelControl20.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl20.Appearance.HoverImage")));
            this.labelControl20.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl20.Appearance.Image")));
            this.labelControl20.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl20.Appearance.PressedImage")));
            this.labelControl20.Name = "labelControl20";
            // 
            // labelControl21
            // 
            resources.ApplyResources(this.labelControl21, "labelControl21");
            this.labelControl21.Name = "labelControl21";
            // 
            // labelControl22
            // 
            resources.ApplyResources(this.labelControl22, "labelControl22");
            this.labelControl22.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl22.Appearance.DisabledImage")));
            this.labelControl22.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl22.Appearance.Font")));
            this.labelControl22.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl22.Appearance.FontSizeDelta")));
            this.labelControl22.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl22.Appearance.FontStyleDelta")));
            this.labelControl22.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl22.Appearance.ForeColor")));
            this.labelControl22.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl22.Appearance.GradientMode")));
            this.labelControl22.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl22.Appearance.HoverImage")));
            this.labelControl22.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl22.Appearance.Image")));
            this.labelControl22.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl22.Appearance.PressedImage")));
            this.labelControl22.Name = "labelControl22";
            // 
            // labelControl23
            // 
            resources.ApplyResources(this.labelControl23, "labelControl23");
            this.labelControl23.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl23.Appearance.DisabledImage")));
            this.labelControl23.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl23.Appearance.Font")));
            this.labelControl23.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl23.Appearance.FontSizeDelta")));
            this.labelControl23.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl23.Appearance.FontStyleDelta")));
            this.labelControl23.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl23.Appearance.GradientMode")));
            this.labelControl23.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl23.Appearance.HoverImage")));
            this.labelControl23.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl23.Appearance.Image")));
            this.labelControl23.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl23.Appearance.PressedImage")));
            this.labelControl23.Name = "labelControl23";
            // 
            // simpleButton3
            // 
            resources.ApplyResources(this.simpleButton3, "simpleButton3");
            this.simpleButton3.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.Appearance.BackColor")));
            this.simpleButton3.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("simpleButton3.Appearance.BackColor2")));
            this.simpleButton3.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton3.Appearance.Font")));
            this.simpleButton3.Appearance.FontSizeDelta = ((int)(resources.GetObject("simpleButton3.Appearance.FontSizeDelta")));
            this.simpleButton3.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("simpleButton3.Appearance.FontStyleDelta")));
            this.simpleButton3.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("simpleButton3.Appearance.GradientMode")));
            this.simpleButton3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Appearance.Image")));
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            resources.ApplyResources(this.simpleButton2, "simpleButton2");
            this.simpleButton2.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.BackColor")));
            this.simpleButton2.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.BackColor2")));
            this.simpleButton2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton2.Appearance.Font")));
            this.simpleButton2.Appearance.FontSizeDelta = ((int)(resources.GetObject("simpleButton2.Appearance.FontSizeDelta")));
            this.simpleButton2.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("simpleButton2.Appearance.FontStyleDelta")));
            this.simpleButton2.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("simpleButton2.Appearance.GradientMode")));
            this.simpleButton2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Appearance.Image")));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // btnNewOrder
            // 
            resources.ApplyResources(this.btnNewOrder, "btnNewOrder");
            this.btnNewOrder.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnNewOrder.Appearance.BackColor")));
            this.btnNewOrder.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("btnNewOrder.Appearance.BackColor2")));
            this.btnNewOrder.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnNewOrder.Appearance.Font")));
            this.btnNewOrder.Appearance.FontSizeDelta = ((int)(resources.GetObject("btnNewOrder.Appearance.FontSizeDelta")));
            this.btnNewOrder.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("btnNewOrder.Appearance.FontStyleDelta")));
            this.btnNewOrder.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnNewOrder.Appearance.GradientMode")));
            this.btnNewOrder.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnNewOrder.Appearance.Image")));
            this.btnNewOrder.Appearance.Options.UseBackColor = true;
            this.btnNewOrder.Appearance.Options.UseFont = true;
            this.btnNewOrder.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btnNewOrder.Name = "btnNewOrder";
            this.btnNewOrder.Click += new System.EventHandler(this.btnNewOrder_Click);
            // 
            // labelControl6
            // 
            resources.ApplyResources(this.labelControl6, "labelControl6");
            this.labelControl6.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.DisabledImage")));
            this.labelControl6.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl6.Appearance.Font")));
            this.labelControl6.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl6.Appearance.FontSizeDelta")));
            this.labelControl6.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl6.Appearance.FontStyleDelta")));
            this.labelControl6.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl6.Appearance.GradientMode")));
            this.labelControl6.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.HoverImage")));
            this.labelControl6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.Image")));
            this.labelControl6.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl6.Appearance.PressedImage")));
            this.labelControl6.Name = "labelControl6";
            // 
            // labelControl11
            // 
            resources.ApplyResources(this.labelControl11, "labelControl11");
            this.labelControl11.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl11.Appearance.DisabledImage")));
            this.labelControl11.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl11.Appearance.FontSizeDelta")));
            this.labelControl11.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl11.Appearance.FontStyleDelta")));
            this.labelControl11.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl11.Appearance.ForeColor")));
            this.labelControl11.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl11.Appearance.GradientMode")));
            this.labelControl11.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl11.Appearance.HoverImage")));
            this.labelControl11.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl11.Appearance.Image")));
            this.labelControl11.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl11.Appearance.PressedImage")));
            this.labelControl11.Name = "labelControl11";
            // 
            // labelControl10
            // 
            resources.ApplyResources(this.labelControl10, "labelControl10");
            this.labelControl10.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl10.Appearance.DisabledImage")));
            this.labelControl10.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl10.Appearance.FontSizeDelta")));
            this.labelControl10.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl10.Appearance.FontStyleDelta")));
            this.labelControl10.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl10.Appearance.ForeColor")));
            this.labelControl10.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl10.Appearance.GradientMode")));
            this.labelControl10.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl10.Appearance.HoverImage")));
            this.labelControl10.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl10.Appearance.Image")));
            this.labelControl10.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl10.Appearance.PressedImage")));
            this.labelControl10.Name = "labelControl10";
            // 
            // labelControl9
            // 
            resources.ApplyResources(this.labelControl9, "labelControl9");
            this.labelControl9.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl9.Appearance.DisabledImage")));
            this.labelControl9.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl9.Appearance.FontSizeDelta")));
            this.labelControl9.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl9.Appearance.FontStyleDelta")));
            this.labelControl9.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl9.Appearance.ForeColor")));
            this.labelControl9.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl9.Appearance.GradientMode")));
            this.labelControl9.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl9.Appearance.HoverImage")));
            this.labelControl9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl9.Appearance.Image")));
            this.labelControl9.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl9.Appearance.PressedImage")));
            this.labelControl9.Name = "labelControl9";
            // 
            // labelControl8
            // 
            resources.ApplyResources(this.labelControl8, "labelControl8");
            this.labelControl8.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.DisabledImage")));
            this.labelControl8.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl8.Appearance.FontSizeDelta")));
            this.labelControl8.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl8.Appearance.FontStyleDelta")));
            this.labelControl8.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl8.Appearance.ForeColor")));
            this.labelControl8.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl8.Appearance.GradientMode")));
            this.labelControl8.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.HoverImage")));
            this.labelControl8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.Image")));
            this.labelControl8.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.PressedImage")));
            this.labelControl8.Name = "labelControl8";
            // 
            // labelControl7
            // 
            resources.ApplyResources(this.labelControl7, "labelControl7");
            this.labelControl7.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.DisabledImage")));
            this.labelControl7.Appearance.FontSizeDelta = ((int)(resources.GetObject("labelControl7.Appearance.FontSizeDelta")));
            this.labelControl7.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("labelControl7.Appearance.FontStyleDelta")));
            this.labelControl7.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl7.Appearance.ForeColor")));
            this.labelControl7.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("labelControl7.Appearance.GradientMode")));
            this.labelControl7.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.HoverImage")));
            this.labelControl7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.Image")));
            this.labelControl7.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("labelControl7.Appearance.PressedImage")));
            this.labelControl7.Name = "labelControl7";
            // 
            // labelControl12
            // 
            resources.ApplyResources(this.labelControl12, "labelControl12");
            this.labelControl12.Name = "labelControl12";
            // 
            // lblDate
            // 
            resources.ApplyResources(this.lblDate, "lblDate");
            this.lblDate.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("lblDate.Appearance.DisabledImage")));
            this.lblDate.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblDate.Appearance.Font")));
            this.lblDate.Appearance.FontSizeDelta = ((int)(resources.GetObject("lblDate.Appearance.FontSizeDelta")));
            this.lblDate.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("lblDate.Appearance.FontStyleDelta")));
            this.lblDate.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblDate.Appearance.ForeColor")));
            this.lblDate.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("lblDate.Appearance.GradientMode")));
            this.lblDate.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("lblDate.Appearance.HoverImage")));
            this.lblDate.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("lblDate.Appearance.Image")));
            this.lblDate.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("lblDate.Appearance.PressedImage")));
            this.lblDate.Name = "lblDate";
            // 
            // lblTicket
            // 
            resources.ApplyResources(this.lblTicket, "lblTicket");
            this.lblTicket.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("lblTicket.Appearance.DisabledImage")));
            this.lblTicket.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTicket.Appearance.Font")));
            this.lblTicket.Appearance.FontSizeDelta = ((int)(resources.GetObject("lblTicket.Appearance.FontSizeDelta")));
            this.lblTicket.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("lblTicket.Appearance.FontStyleDelta")));
            this.lblTicket.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("lblTicket.Appearance.GradientMode")));
            this.lblTicket.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("lblTicket.Appearance.HoverImage")));
            this.lblTicket.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("lblTicket.Appearance.Image")));
            this.lblTicket.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("lblTicket.Appearance.PressedImage")));
            this.lblTicket.Name = "lblTicket";
            // 
            // lblTableName
            // 
            resources.ApplyResources(this.lblTableName, "lblTableName");
            this.lblTableName.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("lblTableName.Appearance.DisabledImage")));
            this.lblTableName.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTableName.Appearance.Font")));
            this.lblTableName.Appearance.FontSizeDelta = ((int)(resources.GetObject("lblTableName.Appearance.FontSizeDelta")));
            this.lblTableName.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("lblTableName.Appearance.FontStyleDelta")));
            this.lblTableName.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("lblTableName.Appearance.GradientMode")));
            this.lblTableName.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("lblTableName.Appearance.HoverImage")));
            this.lblTableName.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("lblTableName.Appearance.Image")));
            this.lblTableName.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("lblTableName.Appearance.PressedImage")));
            this.lblTableName.Name = "lblTableName";
            // 
            // btnOde
            // 
            resources.ApplyResources(this.btnOde, "btnOde");
            this.btnOde.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnOde.Appearance.Font")));
            this.btnOde.Appearance.FontSizeDelta = ((int)(resources.GetObject("btnOde.Appearance.FontSizeDelta")));
            this.btnOde.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("btnOde.Appearance.FontStyleDelta")));
            this.btnOde.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnOde.Appearance.GradientMode")));
            this.btnOde.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnOde.Appearance.Image")));
            this.btnOde.Appearance.Options.UseFont = true;
            this.btnOde.Image = global::MinAFastFoodClient.Properties.Resources.payment_icon__1_;
            this.btnOde.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnOde.Name = "btnOde";
            this.btnOde.Click += new System.EventHandler(this.btnOde_Click);
            // 
            // btnDone
            // 
            resources.ApplyResources(this.btnDone, "btnDone");
            this.btnDone.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDone.Appearance.Font")));
            this.btnDone.Appearance.FontSizeDelta = ((int)(resources.GetObject("btnDone.Appearance.FontSizeDelta")));
            this.btnDone.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("btnDone.Appearance.FontStyleDelta")));
            this.btnDone.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnDone.Appearance.GradientMode")));
            this.btnDone.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnDone.Appearance.Image")));
            this.btnDone.Appearance.Options.UseFont = true;
            this.btnDone.Image = global::MinAFastFoodClient.Properties.Resources.Accept_icon__1_;
            this.btnDone.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDone.Name = "btnDone";
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnDelete
            // 
            resources.ApplyResources(this.btnDelete, "btnDelete");
            this.btnDelete.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDelete.Appearance.Font")));
            this.btnDelete.Appearance.FontSizeDelta = ((int)(resources.GetObject("btnDelete.Appearance.FontSizeDelta")));
            this.btnDelete.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("btnDelete.Appearance.FontStyleDelta")));
            this.btnDelete.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnDelete.Appearance.GradientMode")));
            this.btnDelete.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Appearance.Image")));
            this.btnDelete.Appearance.Options.UseFont = true;
            this.btnDelete.Image = global::MinAFastFoodClient.Properties.Resources.Action_remove_icon__1_;
            this.btnDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnDiscount
            // 
            resources.ApplyResources(this.btnDiscount, "btnDiscount");
            this.btnDiscount.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDiscount.Appearance.Font")));
            this.btnDiscount.Appearance.FontSizeDelta = ((int)(resources.GetObject("btnDiscount.Appearance.FontSizeDelta")));
            this.btnDiscount.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("btnDiscount.Appearance.FontStyleDelta")));
            this.btnDiscount.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnDiscount.Appearance.GradientMode")));
            this.btnDiscount.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnDiscount.Appearance.Image")));
            this.btnDiscount.Appearance.Options.UseFont = true;
            this.btnDiscount.Image = global::MinAFastFoodClient.Properties.Resources._1365347850_bag;
            this.btnDiscount.Name = "btnDiscount";
            this.btnDiscount.Click += new System.EventHandler(this.btnDiscount_Click);
            // 
            // btnTable
            // 
            resources.ApplyResources(this.btnTable, "btnTable");
            this.btnTable.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnTable.Appearance.Font")));
            this.btnTable.Appearance.FontSizeDelta = ((int)(resources.GetObject("btnTable.Appearance.FontSizeDelta")));
            this.btnTable.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("btnTable.Appearance.FontStyleDelta")));
            this.btnTable.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnTable.Appearance.GradientMode")));
            this.btnTable.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnTable.Appearance.Image")));
            this.btnTable.Appearance.Options.UseFont = true;
            this.btnTable.Image = global::MinAFastFoodClient.Properties.Resources.table_icon;
            this.btnTable.Name = "btnTable";
            this.btnTable.Click += new System.EventHandler(this.btnTable_Click);
            // 
            // btnNotes
            // 
            resources.ApplyResources(this.btnNotes, "btnNotes");
            this.btnNotes.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnNotes.Appearance.Font")));
            this.btnNotes.Appearance.FontSizeDelta = ((int)(resources.GetObject("btnNotes.Appearance.FontSizeDelta")));
            this.btnNotes.Appearance.FontStyleDelta = ((System.Drawing.FontStyle)(resources.GetObject("btnNotes.Appearance.FontStyleDelta")));
            this.btnNotes.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnNotes.Appearance.GradientMode")));
            this.btnNotes.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnNotes.Appearance.Image")));
            this.btnNotes.Appearance.Options.UseFont = true;
            this.btnNotes.Image = global::MinAFastFoodClient.Properties.Resources.note_edit_icon;
            this.btnNotes.Name = "btnNotes";
            this.btnNotes.Click += new System.EventHandler(this.btnNotes_Click);
            // 
            // Menu
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOde);
            this.Controls.Add(this.lblQaliq);
            this.Controls.Add(this.txtOdeme);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnCancelDiscount);
            this.Controls.Add(this.btnDiscount);
            this.Controls.Add(this.btnTable);
            this.Controls.Add(this.btnNotes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Menu";
            this.Load += new System.EventHandler(this.Menu_Load);
            this.dataRepeater1.ItemTemplate.ResumeLayout(false);
            this.dataRepeater1.ItemTemplate.PerformLayout();
            this.dataRepeater1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtOdeme.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYekun.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndirim.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUmMebleg.Properties)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

         
        
        private DevExpress.XtraEditors.SimpleButton btnNotes;
        private DevExpress.XtraEditors.SimpleButton btnTable;
        private DevExpress.XtraEditors.SimpleButton btnDiscount;
        private DevExpress.XtraEditors.SimpleButton btnCancelDiscount;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnDone;
      
        
   
        private DevExpress.XtraEditors.SimpleButton btnOde;
    
      
        private DevExpress.XtraEditors.TextEdit txtOdeme;
        private DevExpress.XtraEditors.LabelControl lblQaliq;
      
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.XtraScrollableControl optionControl;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.XtraScrollableControl itemControl;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.XtraScrollableControl menuControl;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl lblDiscount;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtYekun;
        private DevExpress.XtraEditors.TextEdit txtEndirim;
        private DevExpress.XtraEditors.TextEdit txtUmMebleg;
        private System.Windows.Forms.GroupBox groupBox4;
      
   
        private DevExpress.XtraEditors.LabelControl lblTableName;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl lblDate;
        private DevExpress.XtraEditors.LabelControl lblTicket;
        private System.Windows.Forms.GroupBox groupBox5;
        private Microsoft.VisualBasic.PowerPacks.DataRepeater dataRepeater1;
        private System.Windows.Forms.TextBox txtItemAmount;
        private DevExpress.XtraEditors.LabelControl lblItemNumber;
        private DevExpress.XtraEditors.LabelControl lblItemId;
        private System.Windows.Forms.TextBox txtOptionPrice;
        private System.Windows.Forms.RichTextBox richTextOptions;
        private System.Windows.Forms.TextBox txtItemPrices;
        private System.Windows.Forms.TextBox txtOnePrice;
        private System.Windows.Forms.TextBox txtItemName;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton btnNewOrder;        

    }
}