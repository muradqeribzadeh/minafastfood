﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;
using System.Resources;

namespace MinAFastFoodClient
{
    public partial class Discounts : DevExpress.XtraEditors.XtraForm
    {

        private string discType;
        private int itemNumber;
        private int language;
        public Discounts(string type,int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            discType = type;
            this.language = language;
        }
        public Discounts(string type, int itemNumber,int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            discType = type;
            this.itemNumber = itemNumber;
            this.language = language;
        }

        protected DataSet PDataset(string select_statement)
        {
            SqlConnection conn = new SqlConnection(Utility.DBHelper.ConnectionString);
            SqlCommand cmd = new SqlCommand(select_statement, conn);
            conn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            ad.Fill(ds);

            conn.Close();
            return ds;
        }
        int XIndent = 10;
        int YIndent = 10;
        int count = 0;
        public void fillDiscounts()
        {
            DataSet discountDataset = PDataset("Select [Oid],[DiscountDesc],[DiscountAmount],[DiscountType] FROM [MinAFastFood].[dbo].[Discounts] where [Visible]=1");
            DataTable discountTable = discountDataset.Tables[0];

            foreach (DataRow r in discountTable.Rows)
            {
                SimpleButton discountBtn = new SimpleButton();
                discountBtn.Name = r[3].ToString() + r[0].ToString();
                discountBtn.Width = 162;
                discountBtn.ImageLocation = ImageLocation.TopCenter;
                discountBtn.Height = 71;
                discountBtn.Text = r[1].ToString();
                discountBtn.Tag = r[2].ToString();
                discountBtn.Left = XIndent;
                discountBtn.Top = YIndent;

                discountsControl.Controls.Add(discountBtn);
                XIndent += discountBtn.Width + 10;

                discountBtn.Click += new EventHandler(discountButton_Click);
                count++;

                if (count % 3 == 0)
                {
                    XIndent = 10;
                    YIndent += discountBtn.Height + 10;
                }
            }
        }
        private int getOrderItemById(int id)
        {

            int k = MinAFastFoodClient.Menu.orderItems.FindIndex(x => x.ItemNumber.Equals(id));
            return k;

        }
        private void discountButton_Click(object sender, EventArgs e)
        {
            SimpleButton discBtn = (SimpleButton)sender;

            if (discType.Equals("O"))
            {
                MinAFastFoodClient.Menu.orderDiscount.DiscId = discBtn.Name.Substring(1);
                MinAFastFoodClient.Menu.orderDiscount.DiscType = int.Parse(discBtn.Name[0].ToString());
                MinAFastFoodClient.Menu.orderDiscount.DiscAmount =Math.Round( Decimal.Parse(discBtn.Tag.ToString()),2);
                MinAFastFoodClient.Menu.cancelButton.Visible = true;
                if (int.Parse(discBtn.Name[0].ToString()) == 0)

                    MinAFastFoodClient.Menu.discountViewLabel.Text = Math.Round(Decimal.Parse(discBtn.Tag.ToString()), 2).ToString() + " % ";
                else
                    MinAFastFoodClient.Menu.discountViewLabel.Text = Math.Round(Decimal.Parse(discBtn.Tag.ToString()), 2).ToString();
                Close();
            }
            else
            {



                int index = getOrderItemById(this.itemNumber);
                if (int.Parse(discBtn.Name[0].ToString()) == 0)
                {//Faiznen endirim hali
                    MinAFastFoodClient.Menu.orderItems[index].ItemPrice = Math.Round(MinAFastFoodClient.Menu.orderItems[index].ItemPrice - ((MinAFastFoodClient.Menu.orderItems[index].ItemPrice * Decimal.Parse(discBtn.Tag.ToString())) / 100), 2);
                    MinAFastFoodClient.Menu.orderItems[index].OptionsPrice =Math.Round(  MinAFastFoodClient.Menu.orderItems[index].OptionsPrice - ((MinAFastFoodClient.Menu.orderItems[index].OptionsPrice * Decimal.Parse(discBtn.Tag.ToString())) / 100),2);
                    MinAFastFoodClient.Menu.orderItems[index].DiscountId = discBtn.Name.Substring(1);
                    MinAFastFoodClient.Menu.orderItems[index].DiscountAmount =Math.Round( Decimal.Parse(discBtn.Tag.ToString()),2);
                   
                    ItemForDiscount ifd = new ItemForDiscount(this.language);

                    Hide();
                    ifd.ShowDialog();
                    Close();
                }
                else
                { //Meblegnen endirim hali
                    if (Decimal.Parse(discBtn.Tag.ToString()) > (MinAFastFoodClient.Menu.orderItems[index].ItemPrice + MinAFastFoodClient.Menu.orderItems[index].OptionsPrice))
                    {
                        ResourceManager LocRM = new ResourceManager("MinAFastFoodClient.FastFoodLanguage", typeof(Discounts).Assembly);

                        XtraMessageBox.Show(this.language == 0 ? LocRM.GetString("AzDiscCantApplied") : LocRM.GetString("EngDiscCantApplied"));
                    }
                    else
                    {
                        decimal optionPart = MinAFastFoodClient.Menu.orderItems[index].OptionsPrice;
                        decimal itemPart = MinAFastFoodClient.Menu.orderItems[index].ItemPrice;


                        MinAFastFoodClient.Menu.orderItems[index].ItemPrice = decimal.Round((1 - Decimal.Parse(discBtn.Tag.ToString()) / (optionPart + itemPart)) * itemPart, 2, MidpointRounding.AwayFromZero); ;
                        MinAFastFoodClient.Menu.orderItems[index].OptionsPrice = decimal.Round((1 - Decimal.Parse(discBtn.Tag.ToString()) / (optionPart + itemPart)) * optionPart, 2, MidpointRounding.AwayFromZero); ;
                        MinAFastFoodClient.Menu.orderItems[index].DiscountId = discBtn.Name.Substring(1);
                        MinAFastFoodClient.Menu.orderItems[index].DiscountAmount = Math.Round(Decimal.Parse(discBtn.Tag.ToString()), 2);
                        ItemForDiscount ifd = new ItemForDiscount(this.language);
                        Hide();
                        ifd.ShowDialog();
                        Close();
                    }
                }
            }

        }

        private void Discounts_Load(object sender, EventArgs e)
        {
            fillDiscounts();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            Close();
        }





    }
}