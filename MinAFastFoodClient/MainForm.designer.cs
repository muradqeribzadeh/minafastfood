﻿namespace MinAFastFoodClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnFoodsQuantity = new DevExpress.XtraEditors.SimpleButton();
            this.btnNewOrder = new DevExpress.XtraEditors.SimpleButton();
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAbout = new DevExpress.XtraEditors.SimpleButton();
            this.btnBackOffice = new DevExpress.XtraEditors.SimpleButton();
            this.btnTimecard = new DevExpress.XtraEditors.SimpleButton();
            this.btnLogout = new DevExpress.XtraEditors.SimpleButton();
            this.btnOpenBox = new DevExpress.XtraEditors.SimpleButton();
            this.btnOrders = new DevExpress.XtraEditors.SimpleButton();
            this.btnMenu = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnFoodsQuantity
            // 
            resources.ApplyResources(this.btnFoodsQuantity, "btnFoodsQuantity");
            this.btnFoodsQuantity.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnFoodsQuantity.Appearance.Font")));
            this.btnFoodsQuantity.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnFoodsQuantity.Appearance.GradientMode")));
            this.btnFoodsQuantity.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnFoodsQuantity.Appearance.Image")));
            this.btnFoodsQuantity.Appearance.Options.UseFont = true;
            this.btnFoodsQuantity.Appearance.Options.UseTextOptions = true;
            this.btnFoodsQuantity.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnFoodsQuantity.Image = global::MinAFastFoodClient.Properties.Resources.Pork_Chop_Set_icon;
            this.btnFoodsQuantity.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnFoodsQuantity.Name = "btnFoodsQuantity";
            this.btnFoodsQuantity.Click += new System.EventHandler(this.btnFoodsQuantity_Click);
            // 
            // btnNewOrder
            // 
            resources.ApplyResources(this.btnNewOrder, "btnNewOrder");
            this.btnNewOrder.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnNewOrder.Appearance.Font")));
            this.btnNewOrder.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnNewOrder.Appearance.GradientMode")));
            this.btnNewOrder.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnNewOrder.Appearance.Image")));
            this.btnNewOrder.Appearance.Options.UseFont = true;
            this.btnNewOrder.Image = global::MinAFastFoodClient.Properties.Resources.Files_New_Window_icon;
            this.btnNewOrder.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnNewOrder.Name = "btnNewOrder";
            this.btnNewOrder.Click += new System.EventHandler(this.btnNewOrder_Click);
            // 
            // btnExit
            // 
            resources.ApplyResources(this.btnExit, "btnExit");
            this.btnExit.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnExit.Appearance.Font")));
            this.btnExit.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnExit.Appearance.GradientMode")));
            this.btnExit.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Appearance.Image")));
            this.btnExit.Appearance.Options.UseFont = true;
            this.btnExit.Image = global::MinAFastFoodClient.Properties.Resources.Apps_session_logout_icon__1_;
            this.btnExit.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnExit.Name = "btnExit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Image = global::MinAFastFoodClient.Properties.Resources.monitor_touchScreen;
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // btnAbout
            // 
            resources.ApplyResources(this.btnAbout, "btnAbout");
            this.btnAbout.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnAbout.Appearance.Font")));
            this.btnAbout.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnAbout.Appearance.GradientMode")));
            this.btnAbout.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnAbout.Appearance.Image")));
            this.btnAbout.Appearance.Options.UseFont = true;
            this.btnAbout.Image = global::MinAFastFoodClient.Properties.Resources.Actions_help_about_icon;
            this.btnAbout.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnBackOffice
            // 
            resources.ApplyResources(this.btnBackOffice, "btnBackOffice");
            this.btnBackOffice.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnBackOffice.Appearance.Font")));
            this.btnBackOffice.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnBackOffice.Appearance.GradientMode")));
            this.btnBackOffice.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnBackOffice.Appearance.Image")));
            this.btnBackOffice.Appearance.Options.UseFont = true;
            this.btnBackOffice.Appearance.Options.UseTextOptions = true;
            this.btnBackOffice.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnBackOffice.Image = global::MinAFastFoodClient.Properties.Resources.Computer_icon;
            this.btnBackOffice.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnBackOffice.Name = "btnBackOffice";
            this.btnBackOffice.Click += new System.EventHandler(this.btnBackOffice_Click);
            // 
            // btnTimecard
            // 
            resources.ApplyResources(this.btnTimecard, "btnTimecard");
            this.btnTimecard.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnTimecard.Appearance.Font")));
            this.btnTimecard.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnTimecard.Appearance.GradientMode")));
            this.btnTimecard.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnTimecard.Appearance.Image")));
            this.btnTimecard.Appearance.Options.UseFont = true;
            this.btnTimecard.Image = global::MinAFastFoodClient.Properties.Resources.hitchhikeguidetogalaxy3_clock;
            this.btnTimecard.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnTimecard.Name = "btnTimecard";
            this.btnTimecard.Click += new System.EventHandler(this.btnTimecard_Click);
            // 
            // btnLogout
            // 
            resources.ApplyResources(this.btnLogout, "btnLogout");
            this.btnLogout.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnLogout.Appearance.Font")));
            this.btnLogout.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnLogout.Appearance.GradientMode")));
            this.btnLogout.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Appearance.Image")));
            this.btnLogout.Appearance.Options.UseFont = true;
            this.btnLogout.Image = global::MinAFastFoodClient.Properties.Resources._1366827764_logout;
            this.btnLogout.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnOpenBox
            // 
            resources.ApplyResources(this.btnOpenBox, "btnOpenBox");
            this.btnOpenBox.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnOpenBox.Appearance.Font")));
            this.btnOpenBox.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnOpenBox.Appearance.GradientMode")));
            this.btnOpenBox.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenBox.Appearance.Image")));
            this.btnOpenBox.Appearance.Options.UseFont = true;
            this.btnOpenBox.Appearance.Options.UseTextOptions = true;
            this.btnOpenBox.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnOpenBox.Image = global::MinAFastFoodClient.Properties.Resources.drawer;
            this.btnOpenBox.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnOpenBox.Name = "btnOpenBox";
            // 
            // btnOrders
            // 
            resources.ApplyResources(this.btnOrders, "btnOrders");
            this.btnOrders.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnOrders.Appearance.Font")));
            this.btnOrders.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnOrders.Appearance.GradientMode")));
            this.btnOrders.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnOrders.Appearance.Image")));
            this.btnOrders.Appearance.Options.UseFont = true;
            this.btnOrders.Image = global::MinAFastFoodClient.Properties.Resources.write;
            this.btnOrders.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnOrders.Name = "btnOrders";
            this.btnOrders.Click += new System.EventHandler(this.btnOrders_Click);
            // 
            // btnMenu
            // 
            resources.ApplyResources(this.btnMenu, "btnMenu");
            this.btnMenu.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnMenu.Appearance.Font")));
            this.btnMenu.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnMenu.Appearance.GradientMode")));
            this.btnMenu.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu.Appearance.Image")));
            this.btnMenu.Appearance.Options.UseFont = true;
            this.btnMenu.Image = global::MinAFastFoodClient.Properties.Resources.new_folder;
            this.btnMenu.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnFoodsQuantity);
            this.Controls.Add(this.btnNewOrder);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAbout);
            this.Controls.Add(this.btnBackOffice);
            this.Controls.Add(this.btnTimecard);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.btnOpenBox);
            this.Controls.Add(this.btnOrders);
            this.Controls.Add(this.btnMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnMenu;
        private DevExpress.XtraEditors.SimpleButton btnOrders;
        private DevExpress.XtraEditors.SimpleButton btnOpenBox;
        private DevExpress.XtraEditors.SimpleButton btnLogout;
        private DevExpress.XtraEditors.SimpleButton btnTimecard;
        private DevExpress.XtraEditors.SimpleButton btnBackOffice;
        private DevExpress.XtraEditors.SimpleButton btnAbout;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton btnExit;
        private DevExpress.XtraEditors.SimpleButton btnNewOrder;
        private DevExpress.XtraEditors.SimpleButton btnFoodsQuantity;
    }
}

