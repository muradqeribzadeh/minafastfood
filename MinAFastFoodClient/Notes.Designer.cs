﻿namespace MinAFastFoodClient
{
    partial class Notes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Notes));
            this.txtNote = new DevExpress.XtraEditors.MemoEdit();
            this.btnDone = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Properties.AccessibleDescription = resources.GetString("txtNote.Properties.AccessibleDescription");
            this.txtNote.Properties.AccessibleName = resources.GetString("txtNote.Properties.AccessibleName");
            this.txtNote.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtNote.Properties.Appearance.Font")));
            this.txtNote.Properties.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("txtNote.Properties.Appearance.GradientMode")));
            this.txtNote.Properties.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("txtNote.Properties.Appearance.Image")));
            this.txtNote.Properties.Appearance.Options.UseFont = true;
            this.txtNote.Properties.NullValuePrompt = resources.GetString("txtNote.Properties.NullValuePrompt");
            this.txtNote.Properties.NullValuePromptShowForEmptyValue = ((bool)(resources.GetObject("txtNote.Properties.NullValuePromptShowForEmptyValue")));
            // 
            // btnDone
            // 
            resources.ApplyResources(this.btnDone, "btnDone");
            this.btnDone.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDone.Appearance.Font")));
            this.btnDone.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnDone.Appearance.GradientMode")));
            this.btnDone.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnDone.Appearance.Image")));
            this.btnDone.Appearance.Options.UseFont = true;
            this.btnDone.Image = global::MinAFastFoodClient.Properties.Resources.Accept_icon__1_;
            this.btnDone.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDone.Name = "btnDone";
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // Notes
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.txtNote);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Notes";
            this.Load += new System.EventHandler(this.Notes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtNote.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit txtNote;
        private DevExpress.XtraEditors.SimpleButton btnDone;

    }
}