﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using System.Globalization;
using System.Resources;

namespace MinAFastFoodClient
{
    public partial class ClockForm : DevExpress.XtraEditors.XtraForm
    {
        public ClockForm(bool isPresent,DateTime time,int language)
        {   
            InitializeComponent();
             
            // Assign the string for the "strMessage" key to a message box.


            ResourceManager LocRM = new ResourceManager("MinAFastFoodClient.FastFoodLanguage", typeof(ClockForm).Assembly);
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            if (isPresent)
            {

                this.Text = language == 0 ? LocRM.GetString("AzClockOutTime") : LocRM.GetString("EngClockOutTime");
                lblInOrOut.Text =string.Concat( language == 0 ? LocRM.GetString("AzClockOutTime") : LocRM.GetString("EngClockOutTime") , new DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, 0).ToString("HH:mm:ss"));
            }
            else {

                this.Text = language == 0 ? LocRM.GetString("AzClockInTime") : LocRM.GetString("EngClockInTime");
                lblInOrOut.Text =string.Concat( language == 0 ? LocRM.GetString("AzClockInTime") : LocRM.GetString("EngClockInTime") , new DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, 0).ToString("HH:mm:ss"));
            }
           
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Int32.Parse(lblTimer.Text) == 0)
            {
                timer1.Stop();
                Close();
             
            }
            else {

                lblTimer.Text =( Int32.Parse(lblTimer.Text) - 1).ToString();
            }
        }

        private void ClockForm_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }
    }
}