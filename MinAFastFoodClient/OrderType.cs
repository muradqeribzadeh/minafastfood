﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using System.Globalization;

namespace MinAFastFoodClient
{
    public partial class OrderType : DevExpress.XtraEditors.XtraForm
    {
        public static int orderType;
        public OrderType(int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
        }

        private void btnTable_Click(object sender, EventArgs e)
        {
            orderType = 0;
            Close();
        }

        private void btnHere_Click(object sender, EventArgs e)
        {
            orderType = 1;
            Close();
        }

        private void btnTogo_Click(object sender, EventArgs e)
        {
            orderType = 2;
            Close();
        }
    }
}