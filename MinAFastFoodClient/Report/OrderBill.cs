﻿using System.Data;
using System.Drawing;

namespace MinAFastFoodClient.Report
{
    public partial class OrderBill : DevExpress.XtraReports.UI.XtraReport
    {
        public OrderBill(DataSet ds, string msg)
        {
            InitializeComponent();
            lblDeleted.Text = msg;
            this.DataSource = ds;

            if (msg.Equals("Active"))
                lblDeleted.ForeColor = Color.Green;
            else if (msg.Equals("Paid"))
                lblDeleted.ForeColor = Color.Blue;
            else lblDeleted.ForeColor = Color.Red;
            if (string.IsNullOrEmpty(ds.Tables[0].Rows[0]["TableName"].ToString()))
            {
                lblTableId.Visible = false;
            }
            else
            {
                lblTableId.Text = "Table  # " + ds.Tables[0].Rows[0]["TableName"].ToString();
            }
        }
    }
}