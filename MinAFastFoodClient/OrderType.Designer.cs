﻿namespace MinAFastFoodClient
{
    partial class OrderType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderType));
            this.btnHere = new DevExpress.XtraEditors.SimpleButton();
            this.btnTogo = new DevExpress.XtraEditors.SimpleButton();
            this.btnTable = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btnHere
            // 
            resources.ApplyResources(this.btnHere, "btnHere");
            this.btnHere.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnHere.Appearance.BackColor")));
            this.btnHere.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnHere.Appearance.Font")));
            this.btnHere.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnHere.Appearance.GradientMode")));
            this.btnHere.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnHere.Appearance.Image")));
            this.btnHere.Appearance.Options.UseBackColor = true;
            this.btnHere.Appearance.Options.UseFont = true;
            this.btnHere.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnHere.Image = global::MinAFastFoodClient.Properties.Resources.hereLogo;
            this.btnHere.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnHere.Name = "btnHere";
            this.btnHere.Click += new System.EventHandler(this.btnHere_Click);
            // 
            // btnTogo
            // 
            resources.ApplyResources(this.btnTogo, "btnTogo");
            this.btnTogo.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnTogo.Appearance.BackColor")));
            this.btnTogo.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnTogo.Appearance.Font")));
            this.btnTogo.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnTogo.Appearance.GradientMode")));
            this.btnTogo.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnTogo.Appearance.Image")));
            this.btnTogo.Appearance.Options.UseBackColor = true;
            this.btnTogo.Appearance.Options.UseFont = true;
            this.btnTogo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnTogo.Image = global::MinAFastFoodClient.Properties.Resources.TogoLogo1;
            this.btnTogo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnTogo.Name = "btnTogo";
            this.btnTogo.Click += new System.EventHandler(this.btnTogo_Click);
            // 
            // btnTable
            // 
            resources.ApplyResources(this.btnTable, "btnTable");
            this.btnTable.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnTable.Appearance.BackColor")));
            this.btnTable.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnTable.Appearance.Font")));
            this.btnTable.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnTable.Appearance.GradientMode")));
            this.btnTable.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnTable.Appearance.Image")));
            this.btnTable.Appearance.Options.UseBackColor = true;
            this.btnTable.Appearance.Options.UseFont = true;
            this.btnTable.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnTable.Image = global::MinAFastFoodClient.Properties.Resources.TableSelection;
            this.btnTable.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnTable.Name = "btnTable";
            this.btnTable.Click += new System.EventHandler(this.btnTable_Click);
            // 
            // OrderType
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add(this.btnHere);
            this.Controls.Add(this.btnTogo);
            this.Controls.Add(this.btnTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "OrderType";
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnTable;
        private DevExpress.XtraEditors.SimpleButton btnTogo;
        private DevExpress.XtraEditors.SimpleButton btnHere;
    }
}