﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MinAFastFoodClient.Classes
{
    public class OrderDiscounts
    {
        private string discId;

        public string DiscId
        {
            get { return discId; }
            set { discId = value; }
        }
        private int discType;

        public int DiscType
        {
            get { return discType; }
            set { discType = value; }
        }
        private decimal discAmount;

        public decimal DiscAmount
        {
            get { return discAmount; }
            set { discAmount = value; }
        }

    }
}
