﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MinAFastFoodClient.Classes
{
    class ItemOptions
    {
        private string itemId;

        public string ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }
        private string optionId;

        public string OptionId
        {
            get { return optionId; }
            set { optionId = value; }
        }
        private decimal optionsCost;

        public decimal OptionsCost
        {
            get { return optionsCost; }
            set { optionsCost = value; }
        }
        private int itemNumber;

        public int ItemNumber
        {
            get { return itemNumber; }
            set { itemNumber = value; }
        }
             
    }
}
