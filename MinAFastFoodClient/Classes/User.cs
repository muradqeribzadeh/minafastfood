﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MinAFastFoodClient.Classes
{
  public  class User
    {
        private bool allowModifyOrder;

        public bool AllowModifyOrder
        {
            get { return allowModifyOrder; }
            set { allowModifyOrder = value; }
        }
        private bool allowVoidOrder;

        public bool AllowVoidOrder
        {
            get { return allowVoidOrder; }
            set { allowVoidOrder = value; }
        }
        private bool canTakeCash;

        public bool CanTakeCash
        {
            get { return canTakeCash; }
            set { canTakeCash = value; }
        }

        private string userId;

        public string UserId
        {
            get { return userId; }
            set { userId = value; }
        }
        private string userName;

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        private string userEmployee;

        public string UserEmployee
        {
            get { return userEmployee; }
            set { userEmployee = value; }
        }
        private string roleName;

        public string RoleName
        {
            get { return roleName; }
            set { roleName = value; }
        }
          

    }
}
