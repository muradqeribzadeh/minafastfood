﻿namespace MinAFastFoodClient.Classes
{
    public class OrderItems
    {
        private decimal priceWithOption;

        public decimal PriceWithOption
        {
            get { return priceWithOption; }
            set { priceWithOption = value; }
        }

        private decimal oneprice;

        public decimal Oneprice
        {
            get { return oneprice; }
            set { oneprice = value; }
        }

        private int itemNumber;

        public int ItemNumber
        {
            get { return itemNumber; }
            set { itemNumber = value; }
        }


        private decimal quantity;

        public decimal Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        private string itemName;

        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }

        private decimal itemPrice;

        public decimal ItemPrice
        {
            get { return itemPrice; }
            set { itemPrice = value; }
        }

        private string itemId;

        public string ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }

        private string optionsDescription;

        public string OptionsDescription
        {
            get { return optionsDescription; }
            set { optionsDescription = value; }
        }

        private string discountId;

        public string DiscountId
        {
            get { return discountId; }
            set { discountId = value; }
        }

        private decimal discountAmount;

        public decimal DiscountAmount
        {
            get { return discountAmount; }
            set { discountAmount = value; }
        }

        public decimal OptionsPrice
        {
            get { return optionsPrice; }
            set { optionsPrice = value; }
        }

        private decimal optionsPrice;
    }
}