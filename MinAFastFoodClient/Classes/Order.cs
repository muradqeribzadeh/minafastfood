﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MinAFastFoodClient.Classes
{
   public  class Order
    {
        private string oid;

        public string Oid
        {
            get { return oid; }
            set { oid = value; }
        }

        private string ticket;

        public string Ticket
        {
            get { return ticket; }
            set { ticket = value; }
        }
        private decimal umumiMebleg;

        public decimal UmumiMebleg
        {
            get { return umumiMebleg; }
            set { umumiMebleg = value; }
        }
        private decimal yekun;

        public decimal Yekun
        {
            get { return yekun; }
            set { yekun = value; }
        }
        private string orderDate;

        public string OrderDate
        {
            get { return orderDate; }
            set { orderDate = value; }
        }
        private decimal discount;

        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }
        private int orderStatus;

        public int OrderStatus
        {
            get { return orderStatus; }
            set { orderStatus = value; }
        }

        
    }
}
