﻿namespace MinAFastFoodClient
{
    partial class EmployeeTimeCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmployeeTimeCard));
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnClockOut = new DevExpress.XtraEditors.SimpleButton();
            this.btnClockIn = new DevExpress.XtraEditors.SimpleButton();
            this.lblInOrOut = new DevExpress.XtraEditors.LabelControl();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnClose.Appearance.Font")));
            this.btnClose.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnClose.Appearance.GradientMode")));
            this.btnClose.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Appearance.Image")));
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.Image = global::MinAFastFoodClient.Properties.Resources.Symbols_Error_icon__1_;
            this.btnClose.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnClose.Name = "btnClose";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnClockOut
            // 
            resources.ApplyResources(this.btnClockOut, "btnClockOut");
            this.btnClockOut.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnClockOut.Appearance.Font")));
            this.btnClockOut.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnClockOut.Appearance.GradientMode")));
            this.btnClockOut.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnClockOut.Appearance.Image")));
            this.btnClockOut.Appearance.Options.UseFont = true;
            this.btnClockOut.Image = global::MinAFastFoodClient.Properties.Resources.clock_out_icon__1_;
            this.btnClockOut.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnClockOut.Name = "btnClockOut";
            this.btnClockOut.Click += new System.EventHandler(this.btnClockOut_Click);
            // 
            // btnClockIn
            // 
            resources.ApplyResources(this.btnClockIn, "btnClockIn");
            this.btnClockIn.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnClockIn.Appearance.Font")));
            this.btnClockIn.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnClockIn.Appearance.GradientMode")));
            this.btnClockIn.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnClockIn.Appearance.Image")));
            this.btnClockIn.Appearance.Options.UseFont = true;
            this.btnClockIn.Image = global::MinAFastFoodClient.Properties.Resources.clock_in_icon__1_;
            this.btnClockIn.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnClockIn.Name = "btnClockIn";
            this.btnClockIn.Click += new System.EventHandler(this.btnClockIn_Click);
            // 
            // lblInOrOut
            // 
            resources.ApplyResources(this.lblInOrOut, "lblInOrOut");
            this.lblInOrOut.Appearance.DisabledImage = ((System.Drawing.Image)(resources.GetObject("lblInOrOut.Appearance.DisabledImage")));
            this.lblInOrOut.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblInOrOut.Appearance.Font")));
            this.lblInOrOut.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblInOrOut.Appearance.ForeColor")));
            this.lblInOrOut.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("lblInOrOut.Appearance.GradientMode")));
            this.lblInOrOut.Appearance.HoverImage = ((System.Drawing.Image)(resources.GetObject("lblInOrOut.Appearance.HoverImage")));
            this.lblInOrOut.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("lblInOrOut.Appearance.Image")));
            this.lblInOrOut.Appearance.PressedImage = ((System.Drawing.Image)(resources.GetObject("lblInOrOut.Appearance.PressedImage")));
            this.lblInOrOut.Name = "lblInOrOut";
            // 
            // EmployeeTimeCard
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblInOrOut);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnClockOut);
            this.Controls.Add(this.btnClockIn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmployeeTimeCard";
            this.Load += new System.EventHandler(this.EmployeeTimeCard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnClockIn;
        private DevExpress.XtraEditors.SimpleButton btnClockOut;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.LabelControl lblInOrOut;
    }
}