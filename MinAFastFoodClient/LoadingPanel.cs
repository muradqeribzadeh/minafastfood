﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraWaitForm;
using System.Threading;
using System.Globalization;

namespace MinAFastFoodClient
{
    public partial class LoadingPanel : WaitForm
    {
        public LoadingPanel()
        {
            InitializeComponent();
            if (Language.LanguageNumber == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            this.progressPanel1.AutoHeight = true;
        }

        #region Overrides

        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            this.progressPanel1.Caption = caption;
        }
        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            this.progressPanel1.Description = description;
        }
        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        public enum WaitFormCommand
        {
        }

        
    }
}