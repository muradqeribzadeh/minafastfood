﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Globalization;
using System.Resources;

namespace MinAFastFoodClient
{
    public partial class OrderTables : DevExpress.XtraEditors.XtraForm
    {
        private int language;

        public OrderTables(int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.language = language;
        }

        private void OrderTables_Load(object sender, EventArgs e)
        {
            fillTableGroups();
        }
        protected DataSet PDataset(string select_statement)
        {
            SqlConnection conn = new SqlConnection(Utility.DBHelper.ConnectionString);
            SqlCommand cmd = new SqlCommand(select_statement, conn);
            conn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            ad.Fill(ds);

            conn.Close();
            return ds;
        }

        int XIndent = 0;
        int YIndent = 0;
        int count = 0;
        private void fillTableGroups()
        {


            DataSet menuDataSet = PDataset("Select Oid,GroupName from [TableGroups] where visible=1");
            DataTable menuTable = menuDataSet.Tables[0];

            foreach (DataRow r in menuTable.Rows)
            {
                SimpleButton groupButton = new SimpleButton();
                groupButton.Name = r[0].ToString();
                groupButton.Width = 98;
                groupButton.ImageLocation = ImageLocation.TopCenter;
                groupButton.Height = 97;
                groupButton.Text = r[1].ToString();
                groupButton.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                groupButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                groupButton.Left = XIndent;
                groupButton.Top = YIndent;


                tableGroupControl.Controls.Add(groupButton);
                XIndent += groupButton.Width + 12;

                groupButton.Click += new EventHandler(groupButton_Click);
                count++;

                if (count % 6 == 0)
                {
                    XIndent = 0;
                    YIndent += groupButton.Height + 10;
                }
            }

        }

        private void groupButton_Click(object sender, EventArgs e)
        {
            SimpleButton btn = sender as SimpleButton;
            XIndent = 0;
            YIndent = 0;
            count = 0;

            fillTables(btn.Name.Trim());


        }

        private void fillTables(string groupId)
        {
            DataSet itemDataSet = PDataset("SELECT  Oid,TableName,TablePicture,MaxGuests,(Select Count(Oid) from [Orders] where [Orders].[TableId]=[Tables].[Oid] and (OrderStatus=0 or OrderStatus=1) ) FROM [Tables] where TableGroups='" + groupId + "' and visible=1 order by TableName");
            DataTable itemTable = itemDataSet.Tables[0];
            tableControl.Controls.Clear();
            foreach (DataRow r in itemTable.Rows)
            {
                SimpleButton tableButton = new SimpleButton();
                tableButton.Name = r[0].ToString();
                tableButton.Width = 98;
                tableButton.ImageLocation = ImageLocation.TopCenter;
                tableButton.Height = 110;
                tableButton.Text = r[1].ToString() + "\n max =" + r[3].ToString();
                tableButton.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                tableButton.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                tableButton.Left = XIndent;
                tableButton.Top = YIndent;
                byte[] barrImg = (byte[])r[2];
                MemoryStream mstream = new MemoryStream(barrImg);
                //  Image img =ScaleImage(Image.FromStream(mstream),32,32);
                Bitmap b = new Bitmap(Image.FromStream(mstream));
                b = ResizeBitmap(b, 45, 45);
                tableButton.Image = (Image)b;
                tableButton.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;

                 if(Int32.Parse(r[4].ToString())>0 )
                     tableButton.Appearance.BackColor = Color.Red;
                 else
                     tableButton.Appearance.BackColor = Color.LimeGreen;
                XIndent += tableButton.Width + 12;
                tableButton.Tag = r[3].ToString();
                tableButton.Click += new EventHandler(tableButton_Click);
                tableControl.Controls.Add(tableButton);
                count++;

                if (count % 6 == 0)
                {
                    XIndent = 0;
                    YIndent += tableButton.Height + 10;
                }

            }


        }
        private void tableButton_Click(object sender, EventArgs e)
        {
            SimpleButton btn = sender as SimpleButton;
            XIndent = 0;
            YIndent = 0;
            count = 0;
            ResourceManager LocRM = new ResourceManager("MinAFastFoodClient.FastFoodLanguage", typeof(OrderTables).Assembly);
            MinAFastFoodClient.Menu.tableNameLableControl.Text =String.Concat( this.language == 0 ? LocRM.GetString("AzTable") : LocRM.GetString("EngTable"), btn.Text.Substring(0, btn.Text.IndexOf("\n")));
            MinAFastFoodClient.Menu.tableId = btn.Name.ToString();
            Close();
        }
        public Bitmap ResizeBitmap(Bitmap b, int nWidth, int nHeight)
        {
            Bitmap result = new Bitmap(nWidth, nHeight);
            using (Graphics g = Graphics.FromImage((Image)result))
                g.DrawImage(b, 0, 0, nWidth, nHeight);
            return result;
        }
    }
}