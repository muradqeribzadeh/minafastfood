﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MinAFastFoodClient.Classes;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;

namespace MinAFastFoodClient
{
    public partial class EmployeeTimeCard : DevExpress.XtraEditors.XtraForm
    {
        private User user;
        private string oid;
        private DateTime inTime;
        private int language;
        public EmployeeTimeCard(User user,int language)
        {
            if (language == 0)
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("Az");
            else
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            InitializeComponent();
            this.user = new User();
            this.user = user;
            this.language = language;
        }
        protected DataSet PDataset(string select_statement)
        {
            SqlConnection conn = new SqlConnection(Utility.DBHelper.ConnectionString);
            SqlCommand cmd = new SqlCommand(select_statement, conn);
            conn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            ad.Fill(ds);

            conn.Close();
            return ds;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void EmployeeTimeCard_Load(object sender, EventArgs e)
        {
            DataSet dsEmpTimeCard = PDataset("Select [Oid],[ClockInTime] from  [EmployeeTimeCard] where  [Employee]=N'" + this.user.UserEmployee + "' and [IsPresent]=1");
            DataTable empTable = dsEmpTimeCard.Tables[0];

            if (empTable.Rows.Count == 0)
            {
                btnClockIn.Enabled = true;
                btnClockOut.Enabled = false;
                lblInOrOut.Visible=false;
            }
            else {


                btnClockIn.Enabled = false;
                lblInOrOut.Text = DateTime.Parse(empTable.Rows[0][1].ToString()).ToString("dd.MM.yyyy HH:mm");
                btnClockOut.Enabled = true;
                oid = empTable.Rows[0][0].ToString();
                inTime = DateTime.Parse(empTable.Rows[0][1].ToString());
            
            
            }

        }

        private void btnClockIn_Click(object sender, EventArgs e)
        {
            DateTime date=DateTime.Now;
           try
             {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Insert into [EmployeeTimeCard]([Employee],[ClockInTime],[IsPresent]) Values (N'"+this.user.UserEmployee+"','"+date.ToString("yyyy-MM-dd HH:mm:ss.fff")+"',1)", con))
                    {
                         
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                

                    ClockForm cl = new ClockForm(false, date,this.language);
                    cl.ShowDialog();
                    Close();
                }
            }

            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }

         //  
              
        }

        private void btnClockOut_Click(object sender, EventArgs e)
        {
            DateTime date = DateTime.Now;
            TimeSpan span = date.Subtract(inTime);
          

            try
            {
                using (SqlConnection con = new SqlConnection(Utility.DBHelper.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Update    [EmployeeTimeCard] set  [IsPresent]=0,ClockOutTime='" + date.ToString("yyyy-MM-dd HH:mm:ss.fff") + "',[TotalTime]=" +   Math.Truncate(span.TotalMinutes)+ " where Oid=N'" + this.oid + "'", con))
                    {

                        command.ExecuteNonQuery();
                    }
                    con.Close();
                    // Close();

                    ClockForm cl = new ClockForm(true, date,this.language);
                    cl.ShowDialog();
                    Close();
                }
            }

            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }


        }
    }
}