﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MinAFastFoodClient.Classes;
using System.Resources;
using DevExpress.XtraSplashScreen;
using System.Threading;

namespace MinAFastFoodClient
{
    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {
        //public static User user;
        ResourceManager LocRM    = new ResourceManager("MinAFastFoodClient.FastFoodLanguage", typeof(LoginForm).Assembly);
        public LoginForm()
        {
            InitializeComponent();
        }

        protected DataSet PDataset(string select_statement)
        {
            SqlConnection conn = new SqlConnection(Utility.DBHelper.ConnectionString);
            SqlCommand cmd = new SqlCommand(select_statement, conn);
            conn.Open();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            ad.Fill(ds);

            conn.Close();
            return ds;
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            DataSet ds = PDataset("Select Oid,UserName  FROM [EmpUser] where [GCRecord] is null ");
            CBoxUsers.Properties.DataSource = ds.Tables[0];
            CBoxUsers.Properties.DisplayMember = "UserName";
            CBoxUsers.Properties.ValueMember = "Oid";

            CBoxUsers.Properties.ShowHeader = false;
            CBoxUsers.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserName"));
            CBoxUsers.Focus();
            rdAzerbaijan.Checked = true;
        }

        private User isUser(string password)
        {
            DataSet ds = PDataset("Select  StoredPassword,Oid,UserName,UserEmployee,(select Name from [SecuritySystemRole] where Oid= (Select Top 1 EmpRoles from  [EmpUserEmpUser_EmpRoleEmpRoles] where  [EmpUser]=[EmpUser].[Oid]) ) as RoleName from  [EmpUser] where Oid='" + CBoxUsers.EditValue.ToString() + "'");
            password = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            if (new DevExpress.Persistent.Base.PasswordCryptographer().AreEqual(password, txtPassword.Text))
            {

                DataSet userPermissionsDs = PDataset("Select  [AllowModifyOrder],[AllowVoidOrder],[CanTakeCash] from [RoleClientPermissions] where RoleName='" + ds.Tables[0].Rows[0][4].ToString() + "'");
                User u = new User();

                u.Password = password;
                u.UserId = ds.Tables[0].Rows[0][1].ToString();
                u.UserName = ds.Tables[0].Rows[0][2].ToString();
                u.RoleName = ds.Tables[0].Rows[0][4].ToString();
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0][3].ToString()))
                {
                    u.UserEmployee = ds.Tables[0].Rows[0][3].ToString();
                }
                else
                {
                    u.UserEmployee = "0";
                }
                u.AllowModifyOrder = Boolean.Parse(userPermissionsDs.Tables[0].Rows[0][0].ToString());
                u.AllowVoidOrder = Boolean.Parse(userPermissionsDs.Tables[0].Rows[0][1].ToString());
                u.CanTakeCash = Boolean.Parse(userPermissionsDs.Tables[0].Rows[0][2].ToString());
                return u;
            }
            else
            {
                return null;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            txtPassword_Click(null, null);
        }
        private bool shorOrderType() {

            DataSet ds = PDataset("Select Top 1 [showOrderType]  from  [Info]  ");
       //     password = ds.Tables[0].Rows[0].ItemArray[0].ToString();
            return Boolean.Parse(ds.Tables[0].Rows[0].ItemArray[0].ToString());
        }
        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            { txtPassword_Click(null, null); }
        }

        private void txtPassword_Click(object sender, EventArgs e)
        {
            NumbersKeyboard num = new NumbersKeyboard(1);
            num.ShowDialog();
            if (!string.IsNullOrWhiteSpace(CBoxUsers.EditValue.ToString()))
            {
                if (!NumbersKeyboard.password.Equals("a"))
                {
                    txtPassword.Text = NumbersKeyboard.password;
                    User isU = isUser(txtPassword.Text);

                    if (isU == null)
                    {
                        MessageBox.Show(rdAzerbaijan.Checked ? LocRM.GetString("AzUserWrong") : LocRM.GetString("EngUserWrong"), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        this.Hide();
                        MainForm main;
                        if (rdAzerbaijan.Checked)
                        {
                            main = new MainForm(isU, 0);
                            Language.LanguageNumber = 0;
                        }
                        else
                        {
                            main = new MainForm(isU, 1);
                            Language.LanguageNumber = 1;
                        }

                        MinAFastFoodClient.Menu.shorOrderType = shorOrderType();

                        SplashScreenManager.ShowForm(this, typeof(LoadingPanel), true, true, false);

                        try
                        {
                            Thread.Sleep(2000); // LoadData();
                        }
                        finally
                        {
                            //Close Wait Form
                            SplashScreenManager.CloseForm(false);
                            main.ShowDialog();
                            this.Close();
                        }
                    }
                }
            }
            else
            {
                XtraMessageBox.Show(rdAzerbaijan.Checked ? LocRM.GetString("AzShouldSelectUserName") : LocRM.GetString("EngShouldSelectUserName"));
            }
        }
    }


    public static class Language
    {
        public static int LanguageNumber;
    }
}