﻿namespace MinAFastFoodClient
{
    partial class DiscountChoise
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiscountChoise));
            this.btnDone = new DevExpress.XtraEditors.SimpleButton();
            this.btnDiscOrder = new DevExpress.XtraEditors.SimpleButton();
            this.btnDiscItem = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btnDone
            // 
            resources.ApplyResources(this.btnDone, "btnDone");
            this.btnDone.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDone.Appearance.Font")));
            this.btnDone.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnDone.Appearance.GradientMode")));
            this.btnDone.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnDone.Appearance.Image")));
            this.btnDone.Appearance.Options.UseFont = true;
            this.btnDone.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnDone.Name = "btnDone";
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnDiscOrder
            // 
            resources.ApplyResources(this.btnDiscOrder, "btnDiscOrder");
            this.btnDiscOrder.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDiscOrder.Appearance.Font")));
            this.btnDiscOrder.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnDiscOrder.Appearance.GradientMode")));
            this.btnDiscOrder.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnDiscOrder.Appearance.Image")));
            this.btnDiscOrder.Appearance.Options.UseFont = true;
            this.btnDiscOrder.Name = "btnDiscOrder";
            this.btnDiscOrder.Click += new System.EventHandler(this.btnDiscOrder_Click);
            // 
            // btnDiscItem
            // 
            resources.ApplyResources(this.btnDiscItem, "btnDiscItem");
            this.btnDiscItem.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDiscItem.Appearance.Font")));
            this.btnDiscItem.Appearance.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("btnDiscItem.Appearance.GradientMode")));
            this.btnDiscItem.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btnDiscItem.Appearance.Image")));
            this.btnDiscItem.Appearance.Options.UseFont = true;
            this.btnDiscItem.Name = "btnDiscItem";
            this.btnDiscItem.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // DiscountChoise
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDiscItem);
            this.Controls.Add(this.btnDiscOrder);
            this.Controls.Add(this.btnDone);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DiscountChoise";
            this.Load += new System.EventHandler(this.DiscountChoise_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnDone;
        private DevExpress.XtraEditors.SimpleButton btnDiscOrder;
        private DevExpress.XtraEditors.SimpleButton btnDiscItem;
    }
}