﻿namespace MinAFastFoodClient
{
    partial class OrderTables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderTables));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableGroupControl = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.tableControl);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // tableControl
            // 
            resources.ApplyResources(this.tableControl, "tableControl");
            this.tableControl.Name = "tableControl";
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.tableGroupControl);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // tableGroupControl
            // 
            resources.ApplyResources(this.tableGroupControl, "tableGroupControl");
            this.tableGroupControl.AllowTouchScroll = true;
            this.tableGroupControl.Name = "tableGroupControl";
            this.tableGroupControl.ScrollBarSmallChange = 30;
            // 
            // OrderTables
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderTables";
            this.Load += new System.EventHandler(this.OrderTables_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.XtraScrollableControl tableControl;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.XtraScrollableControl tableGroupControl;

    }
}