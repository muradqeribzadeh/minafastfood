using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Security.Strategy;

namespace MinAFastFoodAdmin.Module.BusinessObjects.Security
{
    [ImageName("BO_Role")]
    public class EmpRole : SecuritySystemRoleBase 
    {
        public EmpRole(Session session)
            : base(session)
        {
        }
        [Association("EmpUser-EmpRole")]
        public XPCollection<EmpUser> EmpUser
        {
            get
            {
                return GetCollection<EmpUser>("EmpUser");
            }
        }
    }

}
