using System;
using System.ComponentModel;

using DevExpress.Xpo;
using DevExpress.Data.Filtering;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Security;
using DevExpress.Persistent.Base.Security;
using System.Collections.Generic;
using DevExpress.ExpressApp.Utils;

namespace MinAFastFoodAdmin.Module.BusinessObjects.Security
{
    [NavigationItem]
    [DefaultProperty("UserName")]
    [Custom("Caption", "User")]
    [ImageName("BO_User"), System.ComponentModel.DisplayName("EmpUser")]
    [DefaultClassOptions]
    public class EmpUser : BaseObject, ISecurityUser,
        IAuthenticationStandardUser, IAuthenticationActiveDirectoryUser, IOperationPermissionProvider,
        ISecurityUserWithRoles
    {

        public EmpUser(Session session)
            : base(session) { }

           
        private Employee _userEmployee;
        //  [RuleRequiredField("Employee required", "Save", "The user name must not be empty")]
        //        [RuleUniqueValue("Employee is unique", "Save", "The login with the entered UserName was already registered within the system")]
        // [Association("EIUser-Employee")]
   
        public Employee UserEmployee
        {
            get { return _userEmployee; }

            set
            {
                if (_userEmployee == null)
                    SetPropertyValue("UserEmployee", ref _userEmployee, value);
                else if (!ReferenceEquals(_userEmployee, value))
                {
                    throw new ArgumentException("You are not allowed to reassign current user to somewhere else");
                    //  XtraMessageBox.Show("You are not allowed to reassign current user to somewhere else!",
                    //                      "Not allowed action.");
                }
            }
        }

        private string userName = String.Empty;
        [RuleRequiredField("EmployeeUserNameRequired", DefaultContexts.Save)]
        [RuleUniqueValue("EmployeeUserNameIsUnique", DefaultContexts.Save,
            "The login with the entered user name was already registered within the system.")]
        public string UserName
        {
            get { return userName; }
            set { SetPropertyValue("UserName", ref userName, value); }
        }
        private bool isActive = true;
        public bool IsActive
        {
            get { return isActive; }
            set { SetPropertyValue("IsActive", ref isActive, value); }
        }


        #region IAuthenticationStandardUser Members
        private bool changePasswordOnFirstLogon;
        public bool ChangePasswordOnFirstLogon
        {
            get { return changePasswordOnFirstLogon; }
            set
            {
                SetPropertyValue("ChangePasswordOnFirstLogon", ref changePasswordOnFirstLogon, value);
            }
        }
        private string storedPassword;
        [Browsable(false), Size(SizeAttribute.Unlimited), Persistent, SecurityBrowsable]
        protected string StoredPassword
        {
            get { return storedPassword; }
            set { storedPassword = value; }
        }
        public bool ComparePassword(string password)
        {
            return SecurityUserBase.ComparePassword(this.storedPassword, password);
        }
        public void SetPassword(string password)
        {
            this.storedPassword = new PasswordCryptographer().GenerateSaltedPassword(password);
            OnChanged("StoredPassword");
        }
        #endregion


        #region ISecurityUserWithRoles Members
        IList<ISecurityRole> ISecurityUserWithRoles.Roles
        {
            get
            {
                IList<ISecurityRole> result = new List<ISecurityRole>();
                foreach (EmpRole role in EmpRoles)
                {
                    result.Add(role);
                }
                return result;
            }
        }
        #endregion

        [Association("EmpUser-EmpRole")]
        [RuleRequiredField("EmployeeRoleIsRequired", DefaultContexts.Save,
            TargetCriteria = "IsActive",
            CustomMessageTemplate = "An active employee must have at least one role assigned")]
        public XPCollection<EmpRole> EmpRoles
        {
            get
            {
                return GetCollection<EmpRole>("EmpRoles");
            }
        }

        #region IOperationPermissionProvider Members
        IEnumerable<IOperationPermission> IOperationPermissionProvider.GetPermissions()
        {
            return new IOperationPermission[0];
        }
        IEnumerable<IOperationPermissionProvider> IOperationPermissionProvider.GetChildren()
        {
            return new EnumerableConverter<IOperationPermissionProvider, EmpRole>(EmpRoles);
        }
        #endregion
    }

}
