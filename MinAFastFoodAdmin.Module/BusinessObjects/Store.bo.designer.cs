//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
namespace MinAFastFoodAdmin.Module.BusinessObjects
{
  [DefaultClassOptions]
  public partial class Store : DevExpress.Persistent.BaseImpl.BaseObject
  {
    private System.String _storeName;
    public Store(DevExpress.Xpo.Session session)
      : base(session)
    {
    }
    public System.String StoreName
    {
      get
      {
        return _storeName;
      }
      set
      {
        SetPropertyValue("StoreName", ref _storeName, value);
      }
    }
  }
}
