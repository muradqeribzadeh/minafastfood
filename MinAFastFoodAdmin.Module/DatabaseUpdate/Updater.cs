﻿
using System;

using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using MinAFastFoodAdmin.Module.BusinessObjects.Security;
using MinAFastFoodAdmin.Module.BusinessObjects;

namespace MinAFastFoodAdmin.Module.DatabaseUpdate
{
    public class Updater : ModuleUpdater
    {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) : base(objectSpace, currentDBVersion) { }
        public override void UpdateDatabaseAfterUpdateSchema()
        {
            base.UpdateDatabaseAfterUpdateSchema();
              CreateRequiredUsers();
        }
        public void CreateRequiredUsers()
        {
            EmpRole adminRole = ObjectSpace.FindObject<EmpRole>(
       new BinaryOperator("Name", SecurityStrategy.AdministratorRoleName));
            if (adminRole == null)
            {
                adminRole = ObjectSpace.CreateObject<EmpRole>();
                adminRole.Name = SecurityStrategy.AdministratorRoleName;
                adminRole.IsAdministrative = true;
            }
            //Manager role
            EmpRole cashierRole = ObjectSpace.FindObject<EmpRole>(
        new BinaryOperator("Name", "Manager"));
            if (cashierRole == null)
            {
                cashierRole = ObjectSpace.CreateObject<EmpRole>();
                cashierRole.Name = "Manager";
              //  SecuritySystemTypePermissionObject userTypePermission =
                  //  ObjectSpace.CreateObject<SecuritySystemTypePermissionObject>();
              //  userTypePermission.TargetType = typeof(SecuritySystemUser);
               
            }
            //Operator role 
            EmpRole managerRole = ObjectSpace.FindObject<EmpRole>(
        new BinaryOperator("Name", "Operator"));
            if (managerRole == null)
            {
                managerRole = ObjectSpace.CreateObject<EmpRole>();
                managerRole.Name = "Operator";



              //  SecuritySystemTypePermissionObject userTypePermission =
                //    ObjectSpace.CreateObject<SecuritySystemTypePermissionObject>();
//                userTypePermission.TargetType = typeof(SecuritySystemUser);

              

            }

             Employee emp = ObjectSpace.FindObject<Employee>(
                            new BinaryOperator("FirstName", "Admin"));
             if (emp == null) {

                 emp =ObjectSpace.CreateObject<Employee>();

                 emp.FirstName = "Admin";
                 emp.LastName = "Admin";
             
             }
           
            // If a user named 'John' doesn't exist in the database, create this user 
            EmpUser user2 = ObjectSpace.FindObject<EmpUser>(
                 new BinaryOperator("UserName", "Admin"));
            if (user2 == null)
            {
                user2 = ObjectSpace.CreateObject<EmpUser>();
                user2.UserName = "Admin";
                // Set a password if the standard authentication type is used 
                user2.SetPassword("123");
                user2.UserEmployee = emp;
            }
        
            // Add the "Users" Role to the user2 
            user2.EmpRoles.Add(adminRole);

            Info info = ObjectSpace.FindObject<Info>(new BinaryOperator("Default", "Test"));
            if (info == null)
            {
                info = ObjectSpace.CreateObject<Info>();
                info.CompanyName = "Test";
                info.Address = "Test";
                info.City = "Test";
                info.Default = "Test";
                info.showOrderType = true;
            
            }



            RoleClientPermissions rcpAdmin = ObjectSpace.FindObject<RoleClientPermissions>(new BinaryOperator("RoleName", "Administrator"));


            if (rcpAdmin == null)
            {
                rcpAdmin = ObjectSpace.CreateObject<RoleClientPermissions>();
                rcpAdmin.RoleName = "Administrator";
                rcpAdmin.AllowModifyOrder = true;
                rcpAdmin.AllowVoidOrder = true;
                rcpAdmin.CanTakeCash = true;
            }
            RoleClientPermissions rcpManager = ObjectSpace.FindObject<RoleClientPermissions>(new BinaryOperator("RoleName", "Manager"));
          

            if (rcpManager == null)
            {
                rcpManager = ObjectSpace.CreateObject<RoleClientPermissions>();
                rcpManager.RoleName = "Manager";
                rcpManager.AllowModifyOrder = true;
                rcpManager.AllowVoidOrder = true;
                rcpManager.CanTakeCash = true;
            }

            RoleClientPermissions rcpOperator = ObjectSpace.FindObject<RoleClientPermissions>(new BinaryOperator("RoleName", "Operator"));
            //Adding RoleClientPermission

            if (rcpOperator == null)
            {
                rcpOperator = ObjectSpace.CreateObject<RoleClientPermissions>();
                rcpOperator.RoleName = "Operator";
                rcpOperator.AllowModifyOrder = false;
                rcpOperator.AllowVoidOrder = false;
                rcpOperator.CanTakeCash = false;
            }
        }
    }
}
