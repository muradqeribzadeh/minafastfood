using System;
using System.Globalization;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;

namespace ElektronIcraci.Module.Controllers
{
    public partial class ChangeLanguageController : WindowController
    {
        public ChangeLanguageController()
        {
            InitializeComponent();
            RegisterActions(components);
        }

        private string defaultCulture;
        private string defaultFormattingCulture;

        private void ChangeLanguageController_Activated(object sender, EventArgs e)
        {
            GetDefaultCulture();
            ChooseLanguage.Items.Add(new ChoiceActionItem("Azerbaijan (az)", "az-Latn-AZ"));

            ChooseLanguage.Items.Add(new ChoiceActionItem("English", "en"));

            ChooseFormattingCulture.Items.Add(new ChoiceActionItem("Azerbaijan (az)", "az-Latn-AZ"));

         ChooseFormattingCulture.Items.Add(new ChoiceActionItem("English", "en"));
        }

        private void GetDefaultCulture()
        {
            defaultCulture = CultureInfo.InvariantCulture.TwoLetterISOLanguageName;
            defaultFormattingCulture = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
        }

        private void ChooseLanguage_Execute(
           object sender, SingleChoiceActionExecuteEventArgs e)
        {
            Application.SetLanguage(e.SelectedChoiceActionItem.Data as string);
        }

        private void ChooseFormattingCulture_Execute(
        object sender, SingleChoiceActionExecuteEventArgs e)
        {
            Application.SetFormattingCulture(e.SelectedChoiceActionItem.Data as string);
        }
    }
}